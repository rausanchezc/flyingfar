/* AdminList Controller */

'use strict';

controllers.controller('AdminListController', ['$scope','UserService','DialogService', 
                                                function($scope, UserService, DialogService){

    /******************************************* Attributes **************************************/

    $scope.adminList = new Array();
    
    $scope.currentAdmin = null;

    /******************************************** Methods ****************************************/
    
    
    /**
     * @param {type} admin
     * @param {type} multiSelect
     * @returns {undefined}
     */
    $scope.selectAdmin = function(admin, multiSelect)
    {
        if($scope.currentAdmin)
        {
            if(!multiSelect){
                angular.forEach($scope.getSelectedAdminList($scope.adminList), function(admin){
                    admin.selected = false;
                });
            }
        }
        admin.selected = !admin.selected;
        
        if($scope.getSelectedAdminList($scope.adminList).length === 1){
            $scope.currentAdmin = $scope.getSelectedAdminList($scope.adminList)[0];
        }
        else{
            $scope.currentAdmin = null;
        }
    };
    
    /**
     * 
     * @param {type} adminList
     * @returns {Array}
     */
    $scope.getSelectedAdminList = function(adminList)
    {
        var selectedAdminList = new Array();
        angular.forEach(adminList, function(admin){
            if(admin.selected)
            {
                selectedAdminList.push(admin);
            }
        });
        return selectedAdminList;
    };
    
    
    /**
     * 
     * @returns {undefined}
     */
    $scope.getAdminList = function()
    {
        UserService.getAdminCompanyList().success(function(data)
        {
            if(data !== null && !angular.isUndefined(data))
            {
                    $scope.adminList = data.adminCompanyList;
                    $scope.prepareAdminList();
//                    $scope.selectAdmin($scope.adminList[0],false);
            }
        });
    };
    
    
    /**
     * 
     * @returns {Boolean}
     */
    $scope.anySelected = function()
    {
        return ($scope.getSelectedAdminList($scope.adminList).length > 0);
    };
    
    
    /**
     * 
     * @returns {undefined}
     */
    $scope.openCreateAdminCompanyDialog = function()
    {
        DialogService.openDialog({
            templateUrl:'fragments/views/admin/companyadmin/CreateAdminCompanyDialog.html',
            controller:'CreateAdminCompanyController',
            resolve:{
                currentAdmin: function(){
                    return null;
                }
            },
            accept:function(){
                $scope.getAdminList();
            }
        });
        
    };
    
    
    /**
     * 
     * @returns {undefined}
     */
    $scope.openModifyAdminCompanyDialog = function()
    {
        DialogService.openDialog({
            templateUrl:'fragments/views/admin/companyadmin/CreateAdminCompanyDialog.html',
            controller:'CreateAdminCompanyController',
            resolve:{
                currentAdmin: function(){
                    return angular.copy($scope.currentAdmin);
                }
            },
            accept: function(){
                $scope.getAdminList();
            }
        });
        
    };
    
    /**
     * 
     * @returns {undefined}
     */
    $scope.deleteAdminCompany = function()
    {
        DialogService.openConfirmationDialog({
            title:'Advertencia',
            message:'Va a proceder a borrar el administrador seleccionado,<br/> ¿Está seguro de que desea continuar?',
            accept: function(){
                UserService.delete($scope.currentAdmin.id).success(function(data,status){
                    if(status === 204)
                    {
                        DialogService.openConfirmationDialog({
                            title:'Éxito',
                            message:'La operación se ha realizado con éxito',
                            smallDialog:true,
                            accept: function(){
                                $scope.getAdminList();
                            }
                        });
                    }
                }).error(DialogService.openHttpErrorDialog);
            }
        });
    };
    
    
    /**
     * 
     * @returns {undefined}
     */
    $scope.prepareAdminList = function()
    {
        angular.forEach($scope.adminList, function(admin){
            admin.creationDate =            Util.convertStringToDate(admin.creationDate);
            admin.lastModificationDate =    Util.convertStringToDate(admin.lastModificationDate);
        });  
    };
    
    /*************************************** Initializer methods *********************************/

    $scope.init = function()
    {
        $scope.getAdminList();
    };
    
    $scope.init();
    
}]);