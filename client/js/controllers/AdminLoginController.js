'use strict';

controllers.controller('AdminLoginController',['$scope', '$location', 'SessionService', '$rootScope','UserService',
                                function($scope, $location, SessionService,$rootScope,UserService){
 
    /******************************************* Attributes **************************************/

    $scope.message = '';

    /******************************************* Methods **************************************/
   
    $scope.login = function(user)
    {
        if($scope.isValid(user))
        {
            $scope.message = "Combrobando...";
            UserService.existsUsername(user.username).success(function(data){
                if(data.exists)
                {
                    UserService.getRoleByUsername(user.username).success(function(data)
                    {
                        if(data.role.name !== 'ROLE_CLIENT')
                        {
                            SessionService.login(user.username, user.password).success(function(data){
                                $scope.message = 'Bienvenido';
                                if(data.roleList[0] === 'ROLE_SYSTEM_ADMIN')
                                {
                                    $location.path('/admin/sys/admins');

                                }
                                else
                                {
                                    var airlineCode = data.user.airlineCode;
                                    $location.path('/admin/'+airlineCode+'/flights');
                                }
                                $rootScope.isLoginAdmin = false;
                            }).error(function(data,status){
                                    switch (status)
                                    {
                                        case 401:
                                            user.password = '';
                                            $scope.message = 'El nombre de usuario o contraseña incorrectos.';
                                            break;
                                        default:
                                            $scope.message = 'Se ha producido un error en la conexión';
                                    }
                            });
                        }
                        else
                        {
                            $scope.message = 'Nombre de usuario o contraseña incorrectos.'
                        }
                    });
            }
             else
            {
                $scope.message = 'Nombre de usuario o contraseña incorrectos.'
            }
            });
        }
        else
        {
            $scope.message = "Complete los campos correctamente."
        }
    };

//    $scope.logout = function() 
//    {
//        SessionService.logout().success(function(data) {
//            $location.path('/admin/login');
//        });
//    };

    $scope.isValid = function(user)
    {
        return user && user.username && user.username.length > 2
            && user.password && user.password.length > 2;
    };
        
    $scope.submit = function(event)
    {
        if(event.keyCode === 13 && !$scope.adminLoginForm.$invalid) {
            $scope.login($scope.admin);
        }
    };
    
    /*************************************** Initializer methods *********************************/

    $scope.init = function()
    {
        $rootScope.isLoginAdmin = true;
        var session = SessionService.getSession();
        if(session.user !== null)
        {
            SessionService.logout();
        }
    };

    $scope.init();
}]);