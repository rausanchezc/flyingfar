/* AirlineList Controller */

'use strict';

controllers.controller('AirlineListController',['$scope','AirlineService','DialogService',
                                            function($scope,AirlineService,DialogService){
        
    /******************************************* Attributes **************************************/


    /******************************************** Methods ****************************************/

    $scope.selectAirline = function(airline, multiSelect)
    {
        if($scope.currentAirline)
        {
            if(!multiSelect){
                angular.forEach($scope.getSelectedAirlineList($scope.airlineList), function(airline){
                    airline.selected = false;
                });
            }
        }
        airline.selected = !airline.selected;
        
        if($scope.getSelectedAirlineList($scope.airlineList).length === 1){
            $scope.currentAirline= $scope.getSelectedAirlineList($scope.airlineList)[0];
        }
        else{
            $scope.currentAirline= null;
        }
    };

    $scope.getSelectedAirlineList = function(airlineList)
    {
        var selectedAirlineList = new Array();
        angular.forEach(airlineList, function(airline){
            if(airline.selected)
            {
                selectedAirlineList.push(airline);
            }
        });
        return selectedAirlineList;
    };

    $scope.anySelected = function()
    {
        return ($scope.getSelectedAirlineList($scope.airlineList).length > 0);
    };
    
    $scope.getAirlineList = function()
    {
        AirlineService.getAll().success(function(data)
        {
            if(data !== null && !angular.isUndefined(data))
            {
                $scope.airlineList = data.airlineList;
                $scope.prepareAirlineList();
//                $scope.selectAirline($scope.airlineList[0],false);
            }
        });
    };
    
    $scope.openCreateAirlineDialog = function()
    {
        DialogService.openDialog({
            templateUrl:'fragments/views/admin/airline/CreateAirlineDialog.html',
            controller:'CreateAirlineController',
            resolve:{
                currentAirline: function(){
                    return null;
                }
            },
            accept:function(){
                $scope.getAirlineList();
            }
        });
        
    };
    
    $scope.openModifyAirlineDialog = function()
    {
        DialogService.openDialog({
            templateUrl:'fragments/views/admin/airline/CreateAirlineDialog.html',
            controller:'CreateAirlineController',
            resolve:{
                currentAirline: function(){
                    return angular.copy($scope.currentAirline);
                }
            },
            accept: function(){
                $scope.getAirlineList();
            }
        });
        
    };
    
    
    $scope.deleteAirline = function()
    {
        DialogService.openConfirmationDialog({
            title:'Advertencia',
            message:'Va a proceder a borrar la aerolínea seleccionada,<br/> ¿Está seguro de que desea continuar?',
            accept: function(){
                AirlineService.delete($scope.currentAirline.id).success(function(data,status){
                    if(status === 204)
                    {
                        DialogService.openConfirmationDialog({
                            title:'Éxito',
                            message:'La operación se ha realizado con éxito',
                            smallDialog:true,
                            accept: function(){
                                $scope.getAirlineList();
                            }
                        });
                    }
                });
            }
        });
    };

    $scope.prepareAirlineList = function()
    {
        angular.forEach($scope.airlineList, function(airline){
            airline.creationDate =            Util.convertStringToDate(airline.creationDate);
            airline.lastModificationDate =    Util.convertStringToDate(airline.lastModificationDate);
        });  
    };

    /*************************************** Initializer methods *********************************/

    $scope.init = function()
    {
        $scope.getAirlineList();
    };
    
    $scope.init();
}]);