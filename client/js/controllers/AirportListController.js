/* AirlineList Controller */

'use strict';

controllers.controller('AirportListController',['$scope','AirportService','DialogService',
                                            function($scope,AirportService,DialogService){
        
    /******************************************* Attributes **************************************/

    /******************************************** Methods ****************************************/

    $scope.selectAirport = function(airport, multiSelect)
    {
        if($scope.currentAirport)
        {
            if(!multiSelect){
                angular.forEach($scope.getSelectedAirportList($scope.airportList), function(airport){
                    airport.selected = false;
                });
            }
        }
        airport.selected = !airport.selected;
        
        if($scope.getSelectedAirportList($scope.airportList).length === 1){
            $scope.currentAirport = $scope.getSelectedAirportList($scope.airportList)[0];
        }
        else{
            $scope.currentAirport= null;
        }
    };

    $scope.getSelectedAirportList = function(airportList)
    {
        var selectedAirportList = new Array();
        angular.forEach(airportList, function(airport){
            if(airport.selected)
            {
                selectedAirportList.push(airport);
            }
        });
        return selectedAirportList;
    };

    $scope.anySelected = function()
    {
        return ($scope.getSelectedAirportList($scope.airportList).length > 0);
    };
    
    $scope.getAirportList = function()
    {
        AirportService.getAll().success(function(data)
        {
            if(data !== null && !angular.isUndefined(data))
            {
                $scope.airportList = data.airportList;
                $scope.prepareAirportList();
//                $scope.selectAirport($scope.airportList[0],false);
            }
        });
    };
    
    $scope.openCreateAirportDialog = function()
    {
        DialogService.openDialog({
            templateUrl:'fragments/views/admin/airport/CreateAirportDialog.html',
            controller:'CreateAirportController',
            resolve:{
                currentAirport: function(){
                    return null;
                }
            },
            accept:function(){
                $scope.getAirportList();
            }
        });
        
    };
    
    $scope.openModifyAirportDialog = function()
    {
        DialogService.openDialog({
            templateUrl:'fragments/views/admin/airport/CreateAirportDialog.html',
            controller:'CreateAirportController',
            resolve:{
                currentAirport: function(){
                    return angular.copy($scope.currentAirport);
                }
            },
            accept: function(){
                $scope.getAirportList();
            }
        });
        
    };
    
    
    $scope.deleteAirport = function()
    {
        DialogService.openConfirmationDialog({
            title:'Advertencia',
            message:'Va a proceder a borrar el aeropuerto seleccionado,<br/> ¿Está seguro de que desea continuar?',
            accept: function(){
                AirportService.delete($scope.currentAirport.id).success(function(data,status){
                    if(status === 204)
                    {
                        DialogService.openConfirmationDialog({
                            title:'Éxito',
                            message:'La operación se ha realizado con éxito',
                            smallDialog:true,
                            accept: function(){
                                $scope.getAirportList();
                            }
                        });
                    }
                });
            }
        });
    };

    $scope.prepareAirportList = function()
    {
        angular.forEach($scope.airportList, function(airport){
            airport.creationDate =            Util.convertStringToDate(airport.creationDate);
            airport.lastModificationDate =    Util.convertStringToDate(airport.lastModificationDate);
        });  
    };

    /*************************************** Initializer methods *********************************/

    $scope.init = function()
    {
        $scope.getAirportList();
    };
    
    $scope.init();
}]);