/* Confirmation dialog controller */
'use strict';

controllers.controller('ConfirmationDialogController', ['$scope', '$modalInstance', '$sce', 'dialog', 
                                                 function($scope, $modalInstance, $sce, dialog) {

    /******************************************* Attributes **************************************/
    $scope.dialog = {
        title: $sce.trustAsHtml(dialog.title),
        message: $sce.trustAsHtml(dialog.message)
    };
    $scope.focusSubmit = dialog.focusSubmit;
    $scope.focusCancel = !dialog.focusSubmit;
    $scope.hideCancelButton = dialog.hideCancelButton;
    $scope.hideFooter = dialog.hideFooter;

    /******************************************** Methods ****************************************/
    $scope.submit = function() {
        $modalInstance.close();
    };

    $scope.cancel = function() {
        $modalInstance.dismiss('cancel');
    };
}]);
