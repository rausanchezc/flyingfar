/* Dashboard Controller  */

'use strict';

controllers.controller('DashboardController', ['$scope','CityService','FlightService','DialogService','$rootScope',
                                        function($scope,CityService, FlightService, DialogService,$rootScope){
    
    
     /******************************************* Attributes **************************************/

     $scope.searchFlight = {
         origin:        null,
         destination:   null,
         departure:     null,
         seats:         null,
         roundTrip:     false
     };
     
     $scope.searchPetition = false;
        
    /******************************************** Methods ****************************************/
    
    
    /* *********** Form Search ************** */
    
    $scope.formSearchIsValid = function(searchFlight)
    {
        var originAndDestination = (searchFlight.origin !== null 
                                    && searchFlight.destination !== null
                                    && searchFlight.origin !== searchFlight.destination);
        
        var dates = (Util.dates.compare(new Date(searchFlight.departure), new Date()) > 0);

        
        return originAndDestination && dates;
    };
    
    
    $scope.isValidDate = function(date)
    {
      return (Util.dates.compare(new Date(date), new Date()) > 0);
    };
    
    $scope.submitSearch = function()
    {
        $scope.searchPetition = true;
        if($scope.searchFlight.departure instanceof Date){
            
            $scope.searchFlight.departure = Util.dateTOString($scope.searchFlight.departure);
        }
        
        $scope.requestOriginCity = $scope.findCityById($scope.searchFlight.origin).name;
        $scope.requestDestinationCity = $scope.findCityById($scope.searchFlight.destination).name;
        
        FlightService.getByRestrictions($scope.searchFlight).success(function(data){
            $scope.flightResultList = data.flightList;
            
            angular.forEach($scope.flightResultList, function(flight){
                flight.departure = Util.convertStringToDate(flight.departure);
            });
            
        }).error(DialogService.openHttpErrorDialog);
        
    };
    
    $scope.isValidFare= function(flight, fare)
    {
        var maxSeats = 0;
        if(fare === 'business')
        {
            maxSeats = flight.plane.maxBusinessSeat;
        }else if(fare === 'tourist')
        {
            maxSeats = flight.plane.maxTouristSeat;
        }else if(fare === 'offer')
        {
            maxSeats = flight.plane.maxOfferSeat;
        }
        return (maxSeats > flight.countReservedOrPaid[fare]);
    };
    
    /* *********** City Methods ************ */
    
    /***
     * 
     * @returns {undefined}
     */
    $scope.getAllCities = function()
    {
        CityService.getAll().success(function(data){
            $scope.cityList = data.cityList;
        });
    };
    
    /***
     * 
     * @param {type} id
     * @returns {@var;city}
     */
    $scope.findCityById = function(id)
    {
      var foundCity;
      angular.forEach($scope.cityList, function(city){
          if(city.id === id)
              foundCity = city;
      });
      return foundCity; 
    };
    
    /***
     * @param {type} cityId
     * @param {type} type
     * @returns {undefined}
     */
    $scope.changeCity = function(cityId, type)
    {
        if(type === 'origin')
        {
            $scope.currentPath.originName = $scope.findCityById(cityId).name;
        }
        else if('destination')
        {
            $scope.currentPath.destinationName = $scope.findCityById(cityId).name;
        }        
    };
    
    /*************************************** Initializer methods *********************************/

    $scope.init = function()
    { 
        $rootScope.isLoginAdmin = false;
        $scope.getAllCities();
    };
    
    $scope.init();
}]);