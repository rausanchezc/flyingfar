/* FlightList Controller */

'use strict';

controllers.controller('FlightListController',['$scope','DialogService','FlightService','AdminCompanyService','SessionService','AirlineService',
                                            function($scope,DialogService,FlightService, AdminCompanyService,SessionService,AirlineService){

    /******************************************* Attributes **************************************/

    /******************************************** Methods ****************************************/

    $scope.selectFlight = function(flight, multiSelect)
    {
        if($scope.currentFlight)
        {
            if(!multiSelect){
                angular.forEach($scope.getSelectedFlightList($scope.flightList), function(flight){
                    flight.selected = false;
                });
            }
        }
        flight.selected = !flight.selected;
        
        if($scope.getSelectedFlightList($scope.flightList).length === 1){
            $scope.currentFlight = $scope.getSelectedFlightList($scope.flightList)[0];
        }
        else{
            $scope.currentFlight= null;
        }
    };
    
    $scope.getSelectedFlightList = function(flightList)
    {
        var selectedFlightList = new Array();
        angular.forEach(flightList, function(flight){
            if(flight.selected)
            {
                selectedFlightList.push(flight);
            }
        });
        return selectedFlightList;
    };
    
    $scope.anySelected = function()
    {
        return ($scope.getSelectedFlightList($scope.flightList).length > 0);
    };
    
    $scope.getFlightList = function()
    {
        FlightService.getAllByAirlineId($scope.ownAirline.id).success(function(data)
        {
            if(data !== null && !angular.isUndefined(data))
            {
                $scope.flightList = data.flightList;
                $scope.prepareFlightList();
//                $scope.selectFlight($scope.flightList[0],false);
            }
        });
    };   
    
    $scope.openCreateFlightDialog = function()
    {
        DialogService.openDialog({
            templateUrl:'fragments/views/admin/flight/CreateFlightDialog.html',
            controller:'CreateFlightController',
            resolve:{
                currentFlight: function(){
                    return null;
                },
                ownAirline: function(){
                    return $scope.ownAirline;
                },
                currentFlightCode: function(){
                    return $scope.currentFlightCode;
                }
            },
            accept:function(data){
                $scope.currentFlightCode = data;
                $scope.getFlightList();
            }
        });
        
    };
    
    $scope.openModifyFlightDialog = function()
    {
        DialogService.openDialog({
            templateUrl:'fragments/views/admin/flight/CreateFlightDialog.html',
            controller:'CreateFlightController',
            resolve:{
                currentFlight: function(){
                    return angular.copy($scope.currentFlight);
                },
                ownAirline: function(){
                    return $scope.ownAirline;
                },
                currentFlightCode: function(){
                    return $scope.currentFlightCode;
                }
            },
            accept: function(){
                $scope.getFlightList();
                $scope.currentFlight = null;
            }
        });
        
    };
    
    
    $scope.deleteFlight = function()
    {
        DialogService.openConfirmationDialog({
            title:'Advertencia',
            message:'Va a proceder a borrar el vuelo seleccionado,<br/> ¿Está seguro de que desea continuar?',
            accept: function(){
                FlightService.delete($scope.currentFlight.id).success(function(data,status){
                    if(status === 204)
                    {
                        DialogService.openConfirmationDialog({
                            title:'Éxito',
                            message:'La operación se ha realizado con éxito',
                            smallDialog:true,
                            accept: function(){
                                $scope.getFlightList();
                            }
                        });
                        
                        $scope.currentFlight = null;
                    }
                });
            }
        });
    };

    $scope.prepareFlightList = function()
    {
        angular.forEach($scope.flightList, function(flight){
            flight.creationDate =            Util.convertStringToDate(flight.creationDate);
            flight.lastModificationDate =    Util.convertStringToDate(flight.lastModificationDate);
            flight.arrival = Util.convertStringToDate(flight.arrival);
            flight.departure = Util.convertStringToDate(flight.departure);
            
            angular.forEach(flight.pathList, function(path){
                path.slot = Util.convertStringToDate(path.slot);
            });
        });  
    };
    
    /*************************************** Initializer methods *********************************/
    
    $scope.init = function()
    {
        var session = SessionService.getSession();
        AirlineService.getByUserId(session.user.id).success(function(data){
            $scope.ownAirline = data;
            $scope.currentFlightCode = data.lastFlightCode;
            $scope.getFlightList();
        });
        

    };
    
    $scope.init();

}]);
