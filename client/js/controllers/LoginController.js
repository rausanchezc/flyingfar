'use strict';

controllers.controller('LoginController',['$scope', '$location', 'SessionService', 
                                function($scope, $location, SessionService){
 
    /******************************************* Attributes **************************************/

    $scope.message = '';

    /******************************************* Methods **************************************/
    $scope.login = function(user){

        if($scope.isValid(user))
        {
            $scope.message = "Combrobando...";
            
            SessionService.login(user.username, user.password).success(function(data){
                $scope.message = 'Bienvenido';
                $location.path('/admin/admins');
            }).error(function(data,status){
                    switch (status)
                    {
                        case 401:
                            user.password = '';
                            $scope.message = 'El nombre de usuario o contraseña incorrectos.';
                            break;
                        default:
                            $scope.message = 'Se ha producido un error en la conexión';
                    }
            });
        }
        else
        {
            $scope.message = "Complete los campos correctamente."
        }
    };

    $scope.isValid = function(user)
    {
        return user && user.username && user.username.length > 2
            && user.password && user.password.length > 2;
    };
        
    $scope.submit = function(event)
    {
        if(event.keyCode === 13 && !$scope.loginForm.$invalid) {
            $scope.login($scope.user);
        }
    };
    
    /*************************************** Initializer methods *********************************/

    $scope.init = function()
    {
        
    };

    $scope.init();
}]);
