/* PlaneList Controller */

'use strict';

controllers.controller('PlaneListController',['$scope','PlaneService','DialogService', 'SessionService','AirlineService',
                                            function($scope,PlaneService,DialogService,SessionService,AirlineService){

    /******************************************* Attributes **************************************/

    /******************************************** Methods ****************************************/

    $scope.selectPlane = function(plane, multiSelect)
    {
        if($scope.currentPlane)
        {
            if(!multiSelect){
                angular.forEach($scope.getSelectedPlaneList($scope.planeList), function(plane){
                    plane.selected = false;
                });
            }
        }
        plane.selected = !plane.selected;
        
        if($scope.getSelectedPlaneList($scope.planeList).length === 1){
            $scope.currentPlane = $scope.getSelectedPlaneList($scope.planeList)[0];
        }
        else{
            $scope.currentPlane= null;
        }
    };
    
    $scope.getSelectedPlaneList = function(planeList)
    {
        var selectedPlaneList = new Array();
        angular.forEach(planeList, function(plane){
            if(plane.selected)
            {
                selectedPlaneList.push(plane);
            }
        });
        return selectedPlaneList;
    };
    
    $scope.anySelected = function()
    {
        return ($scope.getSelectedPlaneList($scope.planeList).length > 0);
    };
    
    $scope.getPlaneList = function()
    {
        PlaneService.getAllByAirlineId($scope.ownAirline.id).success(function(data)
        {
            if(data !== null && !angular.isUndefined(data))
            {
                $scope.planeList = data.planeList;
                $scope.preparePlaneList();
//                $scope.selectPlane($scope.planeList[0],false);
            }
        });
    };   
    
    $scope.openCreatePlaneDialog = function()
    {
        DialogService.openDialog({
            templateUrl:'fragments/views/admin/plane/CreatePlaneDialog.html',
            controller:'CreatePlaneController',
            resolve:{
                currentPlane: function(){
                    return null;
                },
                airlineId:function(){
                    return $scope.ownAirline.id;
                }
            },
            accept:function(){
                $scope.getPlaneList();
            }
        });
        
    };
    
    $scope.openModifyPlaneDialog = function()
    {
        DialogService.openDialog({
            templateUrl:'fragments/views/admin/plane/CreatePlaneDialog.html',
            controller:'CreatePlaneController',
            resolve:{
                currentPlane: function(){
                    return angular.copy($scope.currentPlane);
                },
                airlineId:function(){
                    return $scope.ownAirline.id;
                }
            },
            accept: function(){
                $scope.getPlaneList();
            }
        });
        
    };
    
    
    $scope.deletePlane = function()
    {
        DialogService.openConfirmationDialog({
            title:'Advertencia',
            message:'Va a proceder a borrar el avión seleccionado,<br/> ¿Está seguro de que desea continuar?',
            accept: function(){
                PlaneService.delete($scope.currentPlane.id).success(function(data,status){
                    if(status === 204)
                    {
                        DialogService.openConfirmationDialog({
                            title:'Éxito',
                            message:'La operación se ha realizado con éxito',
                            smallDialog:true,
                            accept: function(){
                                $scope.getPlaneList();
                                $scope.currentPlane = null;
                            }
                        });
                    }
                });
            }
        });
    };

    $scope.preparePlaneList = function()
    {
        angular.forEach($scope.planeList, function(plane){
            plane.creationDate =            Util.convertStringToDate(plane.creationDate);
            plane.lastModificationDate =    Util.convertStringToDate(plane.lastModificationDate);
    
                if(plane.manufactureDate !== null)
                    plane.manufactureDate =     Util.convertStringToDate(plane.manufactureDate);
        });  
    };
    
    /*************************************** Initializer methods *********************************/
    
    $scope.init = function()
    {
        var session = SessionService.getSession();
        AirlineService.getByUserId(session.user.id).success(function(data){
            $scope.ownAirline = data;
            $scope.getPlaneList();
        });

    };
    
    $scope.init();

}]);
