/* Reports Controller */

'use strict';

controllers.controller('ReportsController', ['$scope','ReportService','SessionService',
                                     function($scope,ReportService,SessionService){

    /******************************************* Attributes **************************************/

    /******************************************** Methods ****************************************/
    
    $scope.prepareFlightList = function(flightList)
    {
        angular.forEach(flightList, function(flight){
            flight.numTouristSeat = 0;
            flight.numOfferSeat = 0;
            angular.forEach(flight.seatReservedList, function(seat){
                if(seat.type === "TOURIST"){
                    flight.numTouristSeat++;
                }else if(seat.type === "OFFER"){
                    flight.numOfferSeat++;
                }
            });
            
        });
        
        return flightList;
    };
    
    /*************************************** Initializer methods *********************************/

    $scope.init = function()
    {
        $scope.session = SessionService.getSession();
        ReportService.getFlightsWithSeatOccupation($scope.session.user.airlineCode).success(function(data){
            $scope.flightList = $scope.prepareFlightList(data.flightList);
        });
        
    };
    
    $scope.init();
}]);