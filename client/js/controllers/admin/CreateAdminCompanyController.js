/* CrateAdminCompanyController */

'use strict';

controllers.controller('CreateAdminCompanyController',
                        ['$scope','$modalInstance','AirlineService','AdminCompanyService','DialogService',
                         'currentAdmin','UserService',
                        function($scope,$modalInstance, AirlineService, AdminCompanyService, DialogService,
                          currentAdmin,UserService){

    /******************************************* Attributes **************************************/
    
    $scope.dialogTitle;
    
    $scope.adminCompany = {};
    
    $scope.usernameExists = false;
    $scope.emailExist = false;
    
    /******************************************** Methods ****************************************/

    /**
     * 
     * @returns {undefined}
     */
    $scope.getAllAirlines = function(){
        AirlineService.getAll().success(function(data)
        {
            $scope.airlineList = data.airlineList;
        });
    };
    
    /**
     * 
     * @returns {undefined}
     */
    $scope.changeAirline= function()
    {
        if($scope.findAirlineById($scope.airline)){
            $scope.airlineName = $scope.findAirlineById($scope.airline).name;
        }       
    };
    
    /**
     * 
     * @param {type} id
     * @returns {@var;airline}
     */
    $scope.findAirlineById = function(id){
      var foundAirline;
      angular.forEach($scope.airlineList, function(airline){
          if(airline.id === id)
              foundAirline = airline;
      });
      return foundAirline;
    };
    
    
    /**
     * 
     * @param {type} newPassword
     * @returns {unresolved}
     */
    $scope.checkNewPassword = function(newPassword)
    {
        var valid = (!angular.isUndefined(newPassword) && !angular.isUndefined($scope.adminCompany.password)
                && newPassword.length > 2 && $scope.adminCompany.password.length > 2 
                && newPassword === $scope.adminCompany.password);
        return valid;
    };
    
    /**
     * 
     * @param {type} admin
     * @returns {unresolved}
     */
    $scope.prepareNewAdmin = function(admin)
    {
        delete admin.selected;
        delete admin.creationDate;
        delete admin.lastModificationDate;
        delete admin.airlineName;
        delete admin.airlineCode;
        
        return admin;
    };
    
    
    $scope.existsUsername = function(username)
    {
        if(!angular.isUndefined(username) && username != null && username.length > 0)
        {
            UserService.existsUsername(username).success(function(data){
                $scope.usernameExists = data.exists;
            });
        }
    };
    
    $scope.existsEmail = function(email)
    {
        UserService.existsEmail(email).success(function(data){
            $scope.emailExists = data.exists;
        });
    };
    

    /**
     * 
     * @returns {undefined}
     */
    $scope.submit = function()
    {
        if($scope.creationMode)
        {
            AdminCompanyService.create($scope.adminCompany).success(function(data){
                $modalInstance.close();
                DialogService.openConfirmationDialog({
                    title:'Éxito',
                    message:'La operación se ha realizado con éxito.',
                    smallDialog:true
                });
            }).error(DialogService.openHttpErrorDialog);
        }
        else /* Creation Mode === false */
        {
            AdminCompanyService.update($scope.adminCompany).success(function(data){
                $modalInstance.close();
                DialogService.openConfirmationDialog({
                    title:'Éxito',
                    message:'La operación se ha realizado con éxito.',
                    smallDialog:true
                });
            }).error(DialogService.openHttpErrorDialog);
        }
    };
    
    /**
     * 
     * @returns {undefined}
     */
    $scope.cancel = function()
    {
        $modalInstance.dismiss('cancel');
    };

    /*************************************** Initializer methods *********************************/

    $scope.init = function()
    {
        $scope.getAllAirlines();
        
        if(currentAdmin !== null)
        {
            $scope.dialogTitle = 'Editar Administrador de compañía';
            $scope.dialogAcceptButtonText = 'Guardar';
            $scope.creationMode = false;
            $scope.adminCompany = $scope.prepareNewAdmin(currentAdmin); 
        }
        else
        {
            $scope.dialogTitle = 'Nuevo Administrador de compañía';
            $scope.dialogAcceptButtonText = 'Crear';
            $scope.creationMode = true;
        }
    };
    
    $scope.init();
    
}]);