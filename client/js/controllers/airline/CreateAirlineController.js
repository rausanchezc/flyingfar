/* CreateAirline Controller */

'use strict';

controllers.controller('CreateAirlineController',
                        ['$scope','$modalInstance','AirlineService','DialogService','currentAirline',
                        function($scope,$modalInstance,AirlineService,DialogService,currentAirline){
    
    /******************************************* Attributes **************************************/

    $scope.dialogTitle;
    
    $scope.airline = {};

    /******************************************** Methods ****************************************/

    $scope.submit = function()
    {
        if($scope.creationMode)
        {
            AirlineService.create($scope.airline).success(function(data){
                    $modalInstance.close();
                    DialogService.openConfirmationDialog({
                        title:'Éxito',
                        message:'La operación se ha realizado con éxito.',
                        smallDialog:true
                    });
            });
        }
        else // Creation Mode === false
        {
            AirlineService.update($scope.airline).success(function(data){
                $modalInstance.close();
                DialogService.openConfirmationDialog({
                    title:'Éxito',
                    message:'La operación se ha realizado con éxito.',
                    smallDialog:true
                });
            });
            
        }
    };
    
    $scope.cancel = function()
    {
        $modalInstance.dismiss('cancel');
    };
    
    $scope.prepareNewAirline = function(airline)
    {
        delete airline.selected;
        delete airline.creationDate;
        delete airline.lastModificationDate;

        return airline;
    };
    
    /*************************************** Initializer methods *********************************/
    
    $scope.init = function()
    {
        if(currentAirline !== null)
        {
            $scope.dialogTitle = 'Editar Aerolínea';
            $scope.dialogAcceptButtonText = 'Guardar';
            $scope.creationMode = false;
            $scope.airline = $scope.prepareNewAirline(currentAirline);
        }
        else
        {
            $scope.dialogTitle = 'Nueva Aerolínea';
            $scope.dialogAcceptButtonText = 'Crear';
            $scope.creationMode = true;
        }
        
    };

    $scope.init();
    
}]);