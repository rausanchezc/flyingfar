/* CreateAirline Controller */

'use strict';

controllers.controller('CreateAirportController',
                        ['$scope','$modalInstance','CityService','DialogService','currentAirport','AirportService',
                        function($scope,$modalInstance,CityService,DialogService,currentAirport,AirportService){
    
    /******************************************* Attributes **************************************/

    $scope.dialogTitle;
    
    $scope.airport = {};

    /******************************************** Methods ****************************************/

    $scope.submit = function()
    {
        if($scope.creationMode)
        {
            AirportService.create($scope.airport).success(function(data){
                    $modalInstance.close();
                    DialogService.openConfirmationDialog({
                        title:'Éxito',
                        message:'La operación se ha realizado con éxito.',
                        smallDialog:true
                    });
            });
        }
        else // Creation Mode === false
        {
            AirportService.update($scope.airport).success(function(data){
                $modalInstance.close();
                DialogService.openConfirmationDialog({
                    title:'Éxito',
                    message:'La operación se ha realizado con éxito.',
                    smallDialog:true
                });
            });
            
        }
    };
    
    $scope.getAllCities = function()
    {
        CityService.getAll().success(function(data){
            $scope.cityList = data.cityList;
        });
    };
    
    $scope.changeCity = function()
    {
        if($scope.findCityById($scope.city)){
            $scope.cityName = $scope.findCityById($scope.city).name;
        }
    };
    
    $scope.findCityById = function(id)
    {
      var foundCity;
      angular.forEach($scope.cityList, function(city){
          if(city.id === id)
              foundCity = city;
      });
      return foundCity; 
    };
    
    $scope.cancel = function()
    {
        $modalInstance.dismiss('cancel');
    };
    
    $scope.prepareNewAirport = function(airport)
    {
        delete airport.selected;
        delete airport.creationDate;
        delete airport.lastModificationDate;
        delete airport.cityName;
        
        return airport;
    };
    
    /*************************************** Initializer methods *********************************/
    
    $scope.init = function()
    {
        $scope.getAllCities();
        
        if(currentAirport !== null)
        {
            $scope.dialogTitle = 'Editar Aeropuerto';
            $scope.dialogAcceptButtonText = 'Guardar';
            $scope.creationMode = false;
            $scope.airport = $scope.prepareNewAirport(currentAirport);
        }
        else
        {
            $scope.dialogTitle = 'Nuevo Aeropuerto';
            $scope.dialogAcceptButtonText = 'Crear';
            $scope.creationMode = true;
        }
        
    };

    $scope.init();
    
}]);