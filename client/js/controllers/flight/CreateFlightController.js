/* CreateFlight Controller */

'use strict';

controllers.controller('CreateFlightController',
                            ['$scope','DialogService','FlightService','$modalInstance','currentFlight','CityService','currentFlightCode',
                                'PlaneService','ownAirline',
                            function($scope,DialogService,FlightService,$modalInstance,currentFlight,CityService,currentFlightCode,
                                    PlaneService,ownAirline){
                                
    /******************************************* Attributes **************************************/

    $scope.dialogTitle;
    
    $scope.flight = {};
    $scope.currentPath  = null;
    $scope.pathIndex = 0;
    $scope.currentFare= {
        business : null,
        tourist:null,
        offer:null
    };
    
    $scope.pathList = new Array();
    
    /* Flags */
    $scope.flagNewPath  = false;
    $scope.flagEditPath = false;
    $scope.isBusinessFare = false;
    $scope.isTouristFare = false;
    $scope.isOfferFare = false;
    $scope.isLastMinuteFaree = false;
    /******************************************** Methods ****************************************/

    /***
     * @returns {undefined}
     */
    $scope.submit = function()
    {
        $scope.composeSubmitFlight();
        if($scope.creationMode)
        {
            FlightService.create($scope.flight).success(function(data){
                    $modalInstance.close(currentFlightCode + 1);
                    DialogService.openConfirmationDialog({
                        title:'Éxito',
                        message:'La operación se ha realizado con éxito.',
                        smallDialog:true
                    });
            });
        }
        else // Creation Mode === false
        {
            FlightService.update($scope.flight).success(function(data){
                $modalInstance.close();
                DialogService.openConfirmationDialog({
                    title:'Éxito',
                    message:'La operación se ha realizado con éxito.',
                    smallDialog:true
                });
            });
            
        }
    };
    
    /**
     * @returns {undefined}
     */
    $scope.cancel = function()
    {
        $modalInstance.dismiss('cancel');
    };
    
    /* ********************** Flight Methods ******************** */
    
    /**
     * @returns {_L8.$scope.prepareNewFlight.flight}
     */
    $scope.prepareNewFlight = function()
    {
        var flight = {
            code:           ownAirline.code + '' + (currentFlightCode + 1),
            airlineName:    ownAirline.code,
            airlineId:      ownAirline.id,
            origin:         null,
            destination:    null,
            departure:      null,
            arrival:        null,
            plane:          null,
            roundTrip:      false,
            fareList:       new Array(),
            pathList:       new Array()
        };
        
        $scope.titlePathButton = 'Añadir';
        
        return flight;
    };
    
    
    /**
     * 
     * @param {type} flight
     * @returns {undefined}
     */
    $scope.prepareEditFlight = function(flight)
    {
        delete flight.originName;
        delete flight.destinationName;
        
        flight.airlineName =  ownAirline.code;
        flight.airlineId = ownAirline.id;
        
        var fareList = flight.fareList;
        angular.forEach(fareList, function(fare){
            if(fare.type === 'BUSINESS'){ 
                $scope.isBusinessFare = true;
                $scope.currentFare.businessId = fare.id;
                $scope.currentFare.business = fare.value;
            }
            if(fare.type === 'TOURIST'){ 
                $scope.isTouristFare = true;
                $scope.currentFare.touristId = fare.id;
                $scope.currentFare.tourist = fare.value;
            }
            if(fare.type === 'OFFER'){ 
                $scope.isOfferFare = true;
                $scope.currentFare.offerId = fare.id;
                $scope.currentFare.offer = fare.value;
            }
            if(fare.type === 'LAST_MINUTE'){ 
                $scope.isLastMinuteFare = true;
                $scope.currentFare.lastMinuteId = fare.id;
                $scope.currentFare.lastMinute = fare.value;
            }
            
        });
        
        angular.forEach(flight.pathList, function(path){
            path.tempId = ($scope.pathIndex + 1);
            $scope.pathIndex ++;
        });

        $scope.pathList = angular.copy(flight.pathList);

        return flight;
    };
    
    /**
     * 
     * @param {type} flight
     * @returns {Boolean}
     */                            
    $scope.flightIsValid = function(flight)
    {
        var basicInfo = (flight.code !==  null && flight.airlineName !== null && flight.plane !== null);
        var fromTo = (flight.origin !== null && flight.destination !== null);
        var dates = (flight.departure !== null);
        
        return (basicInfo && fromTo && dates);
    };
    
    $scope.composeSubmitFlight = function()
    {
        delete $scope.flight.airlineName;
        delete $scope.flight.airline;
        delete $scope.flight.selected;
        delete $scope.flight.creationDate;
        delete $scope.flight.lastModificationDate;
        delete $scope.flight.planeName;
        delete $scope.flight.planeInfo;
        
        $scope.flight.departure = Util.dateTOString($scope.flight.departure);
        $scope.flight.arrival = Util.dateTOString($scope.flight.arrival);
        
        
        /* Fares */
        if($scope.flight.fareList.length === 0)
        {
            if($scope.currentFare.business !== null)
            {
                $scope.flight.fareList.push({
                    type: 'BUSINESS',
                    value: parseFloat($scope.currentFare.business)
                });
            }
            
            if($scope.currentFare.tourist !== null)
            {
                $scope.flight.fareList.push({
                    type: 'TOURIST',
                    value: parseFloat($scope.currentFare.tourist)
                });
            }
            
            if($scope.currentFare.offer !== null)
            {
                $scope.flight.fareList.push({
                    type: 'OFFER',
                    value: parseFloat($scope.currentFare.offer)
                });
            }
            
            if($scope.currentFare.lastMinute !== null)
            {
                $scope.flight.fareList.push({
                    type: 'LAST_MINUTE',
                    value: parseFloat($scope.currentFare.lastMinute)
                });
            }
        }
        else
        {
            // business
            if(!angular.isUndefined($scope.currentFare.businessId) && $scope.currentFare.businessId !== null 
                    && $scope.currentFare.business !== '')
            {
                var path = Util.contains($scope.flight.fareList, {id: $scope.currentFare.businessId}, 'id');
                path.value = parseFloat($scope.currentFare.business);
            }
            else if((angular.isUndefined($scope.currentFare.businessId) || $scope.currentFare.businessId === null) 
                    && $scope.currentFare.business !== null )
            {
                $scope.flight.fareList.push({
                    id: $scope.currentFare.businessId,
                    type:'BUSINESS', 
                    value: parseFloat($scope.currentFare.business)
                });
            }
            else // $scope.currentFare.business === null
            {
                Util.remove($scope.flight.fareList, {id: $scope.currentFare.businessId},'id');
            }

            //tourist
            if(!angular.isUndefined($scope.currentFare.touristId) && $scope.currentFare.touristId !== null 
                    && $scope.currentFare.tourist !== '')
            {
                var path = Util.contains($scope.flight.fareList, {id: $scope.currentFare.touristId}, 'id');
                path.value = parseFloat($scope.currentFare.tourist);
            }
            else if((angular.isUndefined($scope.currentFare.touristId) || $scope.currentFare.touristId === null) 
                    && $scope.currentFare.tourist !== null)
            {
                $scope.flight.fareList.push({
                    id: $scope.currentFare.touristId,
                    type:'TOURIST', 
                    value: parseFloat($scope.currentFare.tourist)
                });
            }
            else // $scope.currentFare.tourist === null
            {
                Util.remove($scope.flight.fareList, {id: $scope.currentFare.touristId},'id');
            }

            //offer
            if(!angular.isUndefined($scope.currentFare.offerId) && 
                    $scope.currentFare.offerId !== null && $scope.currentFare.offer !== '')
            {
                var path = Util.contains($scope.flight.fareList, {id: $scope.currentFare.offerId}, 'id');
                path.value = parseFloat($scope.currentFare.offer);
            }
            else if((angular.isUndefined($scope.currentFare.offerId) || $scope.currentFare.offerId === null)
                && $scope.currentFare.offer !== null)
            {
                $scope.flight.fareList.push({
                    id: $scope.currentFare.offerId,
                    type:'OFFER', 
                    value: parseFloat($scope.currentFare.offer)
                });
            }
            else // $scope.currentFare.offer === null
            {
                Util.remove($scope.flight.fareList, {id: $scope.currentFare.offerId},'id');
            }
            // last minute
            if(!angular.isUndefined($scope.currentFare.lastMinuteId) && 
                    $scope.currentFare.lastMinuteId !== null && $scope.currentFare.lastMinute !== '')
            {
                var path = Util.contains($scope.flight.fareList, {id: $scope.currentFare.lastMinuteId}, 'id');
                path.value = parseFloat($scope.currentFare.lastMinute);
            }
            else if((angular.isUndefined($scope.currentFare.lastMinuteId) || $scope.currentFare.lastMinuteId === null)
                && $scope.currentFare.lastMinute !== null)
            {
                $scope.flight.fareList.push({
                    id: $scope.currentFare.lastMinuteId,
                    type:'LAST_MINUTE', 
                    value: parseFloat($scope.currentFare.lastMinute)
                });
            }
            else // $scope.currentFare.lastMinute === null
            {
                Util.remove($scope.flight.fareList, {id: $scope.currentFare.lastMinuteId},'id');
            }
        }
        
        /* Paths */
        angular.forEach($scope.pathList,function(path, index){
            delete path.destinationName;
            delete path.originName;
            delete path.selected;
            delete path.tempId;
            
            path.position = (index + 1);
            path.slot = Util.dateTOString(path.slot);
            path.duration = parseInt(path.duration);
        });

        // Compose flight object
        $scope.flight.pathList = angular.copy($scope.pathList);
        
    };
    
    /* ********************** Fare Methods ********************* */
    
    /**
     * 
     * @param {type} fare
     * @returns {undefined}
     */
    $scope.isFare = function(fare)
    {

    };
    
    /**
     * 
     * @param {type} fare
     * @returns {Boolean}
     */
    $scope.fareIsValid = function(fare)
    {
        var business =  (fare.business !== null && fare.business !== '');
        var tourist =   (fare.tourist !== null && fare.tourist !== '');
        var offer =     (fare.offer !== null && fare.offer !== '');
        var lastMinute =(fare.lastMinute !== null && fare.lastMinute !== '');
     
        return business || tourist || offer || lastMinute;
    };
    
    /* ********************** Paths Methods ******************** */
    
    /***
     * @param {type} path
     * @returns {undefined}
     */
    $scope.selectPath = function(path)
    {
        if($scope.currentPath)
        {
            angular.forEach($scope.getSelectedPathList($scope.pathList), function(path){
                path.selected = false;
            });
        }
        path.selected = !path.selected;
        
        if($scope.getSelectedPathList($scope.pathList).length === 1)
        {
            $scope.currentPath = $scope.getSelectedPathList($scope.pathList)[0];
        }
        else{
            $scope.currentPath = null;
        }
    };
    
    
    /***
     * @param {type} pathList
     * @returns {Array}
     */
    $scope.getSelectedPathList = function(pathList)
    {
        var selectedPathList = new Array();
        angular.forEach(pathList, function(path){
            if(path.selected)
            {
                selectedPathList.push(path);
            }
        });
        
        return selectedPathList;

    };

    /***
     * @returns {Boolean}
     */
    $scope.anyPathSelected = function()
    {
      return ($scope.getSelectedPathList($scope.pathList).length > 0);  
    };
    
    /**
     * @param {type} type
     * @returns {undefined}
     */
    $scope.changeFlag = function(type)
    {
        if(type === 'new')
        {
            $scope.flagNewPath = !$scope.flagNewPath;
            if($scope.flagNewPath)
            {
                $scope.currentPath = {
                    origin: null,
                    originName: '',
                    destination: null,
                    destinationName:'',
                    slot: null,
                    duration:null
                };
                $scope.titlePathButton = 'Añadir';
            }
            
        }
        else if(type === 'edit')
        {
            $scope.flagEditPath = ! $scope.flagEditPath;
            $scope.copyCurrentSlot = $scope.currentPath.slot;
            if($scope.flagEditPath){ $scope.titlePathButton = 'Guardar';}
        }
        $scope.notAppliedChanges = false;
    };
    
    
    /**
     *
     * @returns {undefined}
     */
    $scope.dismissNewPath = function()
    {
        
//        if($scope.flagNewPath){$scope.currentPath = null;}
        
        $scope.flagNewPath = false;
        $scope.flagEditPath = false;
    };
  
    /***
     * @param {type} path
     * @returns {undefined}
     */
    $scope.addOrEditPath = function(path)
    {
        var valid = false;
        $scope.notAppliedChanges = false;
        
        if($scope.flagEditPath) // EDIT PATH
        {
            if($scope.validateDatesPathList($scope.pathList,$scope.flight,path,'edit'))
            {
                angular.forEach($scope.pathList, function(ph, index){
                    if(ph.id === path.tempId)
                    {
                        $scope.pathList[index] = path;
                    }
                });
                valid = true;
                $scope. notAppliedChanges = false;
            }
            else{
                $scope.currentPath.slot = $scope.copyCurrentSlot;
                $scope.notAppliedChanges = true;
            }
        }
        else // NEW PATH
        {
            if($scope.validateDatesPathList($scope.pathList, $scope.flight, path, 'new'))
            {
                $scope.currentPath = null;
                path.tempId = $scope.pathIndex + 1;
                $scope.pathList.push(path);
                $scope.pathIndex ++;
                valid = true;
                $scope.notAppliedChanges = false;
            }
            else
            {
                $scope.notAppliedChanges = true;
            }
        }
        
        /* Reset Flags */
        $scope.flagEditPath = false;
        $scope.flagNewPath = false;
        
        if(valid){ $scope.flight.arrival = $scope.calculateArrivalDate();}
        
    };
    
    /**
     * 
     * @returns {Date}
     */
    $scope.calculateArrivalDate = function()
    {
        var lastPath = $scope.pathList[$scope.pathList.length - 1];
        return  new Date(Date.parse(lastPath.slot) + (lastPath.duration * MINUTE_TO_MILLIS));
    };

    /**
     * @param {type} path
     * @returns {Boolean}
     */
    $scope.pathIsValid = function(path)
    {
        var valid = false;
        
        if(!angular.isUndefined(path) && path !== null)
        {
            valid = (path.origin !== null && path.destination !== null && 
               path.slot !== null && path.duration !== null);
        }

        return valid;
    };
    
    
    /**
     * @returns {undefined}
     */
    $scope.deletePath = function()
    {
        DialogService.openConfirmationDialog({
            title:'Advertencia',
            message:'Va a proceder a borrar el trayecto seleccionado,<br/> ¿Está seguro de que desea continuar?',
            accept: function(){
                
                var path = Util.remove($scope.pathList, {tempId:$scope.currentPath.tempId},'tempId');
                $scope.currentPath = null;
            }
        });
    };
    
    
    /**
     * 
     * @param {type} pathList
     * @param {type} flight
     * @returns {Boolean}
     */
    $scope.validatePathList = function(pathList, flight)
    {
        var result = true;
        if(pathList.length === 1)
        {
            var path = pathList[0];
            if(path.origin !== flight.origin ||
                    path.destination !== flight.destination ||
                    (Util.dates.compare(flight.departure, path.slot) >= 0))
            { 
                result = false;
            }
        }
        else if(pathList.length > 1)
        {
            var index = 0;
            var path;
            var prevPath;
            
            while(index < pathList.length && result)
            {
                path = pathList[index];
                
                if(index === 0) // For first path
                {
                    if((path.origin !== flight.origin) ||
                            ( Util.dates.compare(flight.departure, path.slot) >= 0))
                    {
                        result = false;
                    }
                }
                else if( index === (pathList.length - 1)) // For last path
                {
                    prevPath = pathList[index - 1];
                    
                    if((path.origin !== prevPath.destination) || 
                            (path.destination !== flight.destination)
                            || (Util.dates.compare(prevPath.slot, path.slot) >= 0))
                    {
                        result = false;
                    }
                }
                else
                {
                    prevPath = pathList[index - 1];
                    
                    if((path.origin !== prevPath.destination) ||
                            (Util.dates.compare(prevPath.slot, path.slot) >= 0))
                    {
                        result = false;
                    }
                }
                
                index ++;
            }
        }
        else{ result = false;}
        
        return result;
    };
    
    
    /**
     * @param {type} pathList
     * @param {type} flight
     * @param {type} currentPath
     * @param {type} operation
     * @returns {Boolean}
     */
    $scope.validateDatesPathList = function(pathList, flight, currentPath, operation)
    {
        var valid = true;

        /* Operation: Add Path */
        if(operation === 'new')
        {
            if(pathList.length === 0) // First insert
            {
                valid =  (Util.dates.compare(currentPath.slot, flight.departure) >= 0) ? true : false;
            }
            else
            {
                var lastPath = pathList[pathList.length - 1];
                
                valid = Date.parse(lastPath.slot) + (lastPath.duration * MINUTE_TO_MILLIS) <= Date.parse(currentPath.slot) ? true : false;
            }
        }
        /* Operation: Edit Path */
        else if('edit')
        {
            var prevPath, postPath;
            var current = Date.parse(currentPath.slot);
            var currentPlusDuration = current + (currentPath.duration * MINUTE_TO_MILLIS);
            
            // Find in pathList
            angular.forEach(pathList, function(path, index){
                if(path.tempId === currentPath.tempId)
                {
                    if(pathList[index - 1] !== null)
                    {
                        prevPath = pathList[index -1];
                    }
                    if(pathList[index + 1] !== null)
                    {
                        postPath = pathList[index + 1];
                    }
                }
            });
            
            if(!angular.isUndefined(prevPath) && prevPath !== null && 
                    !angular.isUndefined(postPath) && postPath !== null)
            {
                var prevPathPlusDuration = Date.parse(prevPath.slot) + (prevPath.duration * MINUTE_TO_MILLIS);
                var postPathSlot = Date.parse(postPath.slot);
                
                valid = ((current >= prevPathPlusDuration) && (currentPlusDuration <= postPathSlot));
            }
            else if(angular.isUndefined(prevPath) || prevPath === null 
                    || angular.isUndefined(postPath )|| postPath === null) // First or Last in pathList
            {
                if(pathList.length === 1)
                {
                    valid = (Util.dates.compare(flight.departure, currentPath.slot) <= 0) ? true : false;
                }
                else if(pathList.length > 1)
                {
                    if(angular.isUndefined(prevPath) || prevPath === null)
                    {
                        // compare with flight
                        valid = (Util.dates.compare(flight.departure, currentPath.slot) <= 0) ? true : false;
                    }
                    if(angular.isUndefined(postPath) || postPath === null)
                    {
                        // compare with prevPathius
                        var prevPathPlusDuration = Date.parse(prevPath.slot) + (prevPath.duration * MINUTE_TO_MILLIS);

                        valid = (current >= prevPathPlusDuration) ? true: false;
                    } 
                }
            }

        }
        
        return valid;
    };
    
    /* ******************* City Methods *************************/
    
    /***
     * 
     * @returns {undefined}
     */
    $scope.getAllCities = function()
    {
        CityService.getAll().success(function(data){
            $scope.cityList = data.cityList;
        });
    };
    
    /***
     * 
     * @param {type} id
     * @returns {@var;city}
     */
    $scope.findCityById = function(id)
    {
      var foundCity;
      angular.forEach($scope.cityList, function(city){
          if(city.id === id)
              foundCity = city;
      });
      return foundCity; 
    };
    
    /***
     * @param {type} cityId
     * @param {type} type
     * @returns {undefined}
     */
    $scope.changeCity = function(cityId, type)
    {
        if(type === 'origin')
        {
            $scope.currentPath.originName = $scope.findCityById(cityId).name;
        }
        else if('destination')
        {
            $scope.currentPath.destinationName = $scope.findCityById(cityId).name;
        }
    };
    
    /* **************** Plane Methods ********************* */
    
    /***
     * 
     * @param {type} airlineId
     * @returns {undefined}
     */
    $scope.getAllAirlinePlanes = function(airlineId)
    {
        PlaneService.getAllByAirlineId(airlineId).success(function(data){
            $scope.planeList = data.planeList;
        });
    };
    
    /***
     * 
     * @param {type} planeId
     * @returns {undefined}
     */
    $scope.changePlane = function(planeId)
    {
        var plane = $scope.findPlaneById(planeId);
        $scope.flight.planeName = plane.model;
        $scope.flight.planeInfo = {
                                    total: plane.totalSeats,
                                    business: plane.businessSeats,
                                    tourist:plane.touristSeats,
                                    offer: plane.offerSeats
                                };
    };
    
    
    /***
     * 
     * @param {type} planeId
     * @returns {@var;plane}
     */
    $scope.findPlaneById = function(planeId)
    {
        var planeFound;
        
        angular.forEach($scope.planeList, function(plane){
            if(plane.id === planeId)
            {
                planeFound = plane;
            }
        });
        return planeFound;
    };
    
    /*************************************** Initializer methods *********************************/
    
    $scope.init = function()
    {
        
        if(currentFlight !== null)
        {
            $scope.dialogTitle = 'Editar Vuelo';
            $scope.dialogAcceptButtonText = 'Guardar';
            $scope.creationMode = false;
            $scope.flight = $scope.prepareEditFlight(currentFlight);
        }
        else
        {
            $scope.dialogTitle = 'Nuevo Vuelo';
            $scope.dialogAcceptButtonText = 'Crear';
            $scope.creationMode = true;
            $scope.flight = $scope.prepareNewFlight();
        }
        
        $scope.getAllCities();
        $scope.getAllAirlinePlanes(ownAirline.id);
    };
    

    $scope.init();
}]);