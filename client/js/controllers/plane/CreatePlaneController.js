/* CreatePlane Controller */

'use strict';

controllers.controller('CreatePlaneController',
                                ['$scope','$modalInstance','DialogService','currentPlane','PlaneService','airlineId',
                        function($scope,$modalInstance,DialogService,currentPlane,PlaneService,airlineId){
        
    /******************************************* Attributes **************************************/
    
    $scope.dialogTitle;
    $scope.plane = {};

    /******************************************** Methods ****************************************/
    
    $scope.submit = function()
    {
        if($scope.creationMode)
        {
            PlaneService.create($scope.plane).success(function(data){
                    $modalInstance.close();
                    DialogService.openConfirmationDialog({
                        title:'Éxito',
                        message:'La operación se ha realizado con éxito.',
                        smallDialog:true
                    });
            });
        }
        else // Creation Mode === false
        {
            PlaneService.update($scope.plane).success(function(data){
                $modalInstance.close();
                DialogService.openConfirmationDialog({
                    title:'Éxito',
                    message:'La operación se ha realizado con éxito.',
                    smallDialog:true
                });
            });
            
        }
    };
    
//    $scope.getAllCities = function()
//    {
//        CityService.getAll().success(function(data){
//            $scope.cityList = data.cityList;
//        });
//    };
//    
//    $scope.changeCity = function()
//    {
//        if($scope.findCityById($scope.city)){
//            $scope.cityName = $scope.findCityById($scope.city).name;
//        }
//    };
//    
//    $scope.findCityById = function(id)
//    {
//      var foundCity;
//      angular.forEach($scope.cityList, function(city){
//          if(city.id === id)
//              foundCity = city;
//      });
//      return foundCity; 
//    };
    
    $scope.cancel = function()
    {
        $modalInstance.dismiss('cancel');
    };
    
    $scope.prepareNewPlane = function(plane)
    {
        delete plane.selected;
        delete plane.creationDate;
        delete plane.lastModificationDate;
        delete plane.airlineCode;
        
        plane.airlineId = airlineId;

        return plane;
    };
    
    /*************************************** Initializer methods *********************************/
    $scope.init = function()
    {
        if(currentPlane !== null)
        {
            $scope.dialogTitle = 'Editar Avión';
            $scope.dialogAcceptButtonText = 'Guardar';
            $scope.creationMode = false;
            $scope.plane = $scope.prepareNewPlane(currentPlane);
                
        }
        else
        {
            $scope.dialogTitle = 'Nuevo Avión';
            $scope.dialogAcceptButtonText = 'Crear';
            $scope.creationMode = true;
            $scope.plane.airlineId = airlineId;
        }    
    };
    
    $scope.init();
    
}]);