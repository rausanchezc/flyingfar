/* CreateClient Controller */

'use strict';

controllers.controller('CreateClientController',
                        ['$scope','$modalInstance','DialogService','ClientService','currentClient','UserService',
                        function($scope,$modalInstance,DialogService,ClientService,currentClient,UserService){
    
    /******************************************* Attributes **************************************/

    $scope.dialogTitle;
    
    $scope.client = {};
    
    $scope.usernameExists = false;
    $scope.emailExist = false;

    /******************************************** Methods ****************************************/

    $scope.submit = function()
    {
        if($scope.creationMode)
        {
            ClientService.create($scope.client).success(function(data){
                    $modalInstance.close();
                    DialogService.openConfirmationDialog({
                        title:'Éxito',
                        message:'La operación se ha realizado con éxito.',
                        smallDialog:true
                    });
            }).error(DialogService.openHttpErrorDialog);
        }
        else // Creation Mode === false
        {
            ClientService.update($scope.client).success(function(data){
                $modalInstance.close();
                DialogService.openConfirmationDialog({
                    title:'Éxito',
                    message:'La operación se ha realizado con éxito.',
                    smallDialog:true
                });
            }).error(DialogService.openHttpErrorDialog);
            
        }
    };
    
    $scope.cancel = function()
    {
        $modalInstance.dismiss('cancel');
    };
    
    $scope.prepareNewClient = function(client)
    {

    };
    
        $scope.existsUsername = function(username)
    {
        if(!angular.isUndefined(username) && username !== null && username.length > 0)
        {
            UserService.existsUsername(username).success(function(data){
                $scope.usernameExists = data.exists;
            });
        }
    };
    
    $scope.existsEmail = function(email)
    {
        UserService.existsEmail(email).success(function(data){
            $scope.emailExists = data.exists;
        });
    };
    
    $scope.checkPassword = function(password)
    {
        var valid = (!angular.isUndefined(password) && !angular.isUndefined($scope.client.password)
                && password.length > 2 && $scope.client.password.length > 2 
                && password === $scope.client.password);
        return valid;
    };
    
    /*************************************** Initializer methods *********************************/
    
    $scope.init = function()
    {
        if(currentClient !== null)
        {
            $scope.dialogTitle = 'Editar Perfil';
            $scope.dialogAcceptButtonText = 'Guardar';
            $scope.creationMode = false;
            $scope.client = $scope.prepareNewClient(currentClient);
        }
        else
        {
            $scope.dialogTitle = 'Nuevo Perfil';
            $scope.dialogAcceptButtonText = 'Crear';
            $scope.creationMode = true;
        }
        
    };

    $scope.init();
    
}]);