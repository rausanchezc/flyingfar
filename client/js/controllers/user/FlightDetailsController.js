/* Flight Details Controller */

'use strict';


controllers.controller('FlightDetailsController', 
    ['$scope','$routeParams','FlightService','DialogService','SessionService','$location','$timeout',
    function($scope, $routeParams,FlightService,DialogService,SessionService,$location,$timeout){

    /******************************************* Attributes **************************************/

    $scope.numSeatSelected = 0;
    
    $scope.session = null;
    
    /******************************************** Methods ****************************************/
      
    /* ****** Seats Functions ********* */

    /**
     * @param {type} seat
     * @returns {undefined}
     */
    $scope.selectSeat = function(seat)
    {
        if(seat.reserved !== null && angular.isUndefined(seat.reserved) && !seat.reserved)
        {
            seat.selected = !seat.selected;
            
            if(seat.selected){
                $scope.numSeatSelected ++; 
            }else{
                $scope.numSeatSelected --;
            }
        }
    };

    /**
     * @returns {undefined}
     */ 
    $scope.deselectAllSeat = function()
    {
        angular.forEach($scope.businessSeatList, function(seat){
            seat.selected = false;
        });
        
        angular.forEach($scope.touristSeatList, function(seat){
           seat.selected = false; 
        });

        angular.forEach($scope.offerSeatList, function(seat){
           seat.selected = false; 
        });
    };
    
    /* ****** Flight Functions ********* */
    
    $scope.openPayFlightReserve = function()
    {
        var flightBoardingCard = {
            code: $scope.flight.code,
            origin: $scope.flight.originName,
            destination: $scope.flight.destinationName,
            user: $scope.session.user,
            airline: $scope.flight.airline,
            pathList: $scope.flight.pathList
        };
        
        $scope.saveFlightReserve(true);
        
        DialogService.openDialog({
            templateUrl: 'fragments/views/user/FlightReservePayDialog.html',
            controller: 'FlightReservePayController',
            resolve: {
                flight:function(){ return flightBoardingCard;}
            },
            accept: function() {

            }
        });  
    };
    
    $scope.openSaveFlightReserve = function()
    {
        $scope.saveFlightReserve();
        DialogService.openConfirmationDialog({
            title:'Éxito',
            message:'La reserva se ha realizado con éxito.',
            accept: function(){                
                $location.path( '/client/' + $scope.session.user.username + '/dashboard');
            }
        });
    };
    
    $scope.saveFlightReserve = function(paid,pending,canceled)
    {
        var flight = $scope.prepareFlighReserveForSave(paid,pending,canceled);        
        FlightService.reserve(flight).success(function(data){});        
    };
    
    $scope.prepareFlighReserveForSave = function(paid,pending,canceled)
    {
        //Seats
        var idListReserveSeat = new Array();
        
        angular.forEach($scope.businessSeatList, function(seat){
            if(seat.selected){idListReserveSeat.push(seat.id);}
        });
        
        angular.forEach($scope.touristSeatList, function(seat){
           if(seat.selected){idListReserveSeat.push(seat.id);}
        });

        angular.forEach($scope.offerSeatList, function(seat){
            if(seat.selected){idListReserveSeat.push(seat.id);}
        });
        
        var flight = {
            userId: $scope.session.user.id,
            flightId: $scope.flight.id,
            seatList: idListReserveSeat,
            paid: paid,
            pending: pending,
            canceled: canceled
        }; 
        
        return flight;
    };
    
    /**
     * @param {type} flight
     * @returns {unresolved}
     */
    $scope.prepareFlightReserve = function(flight)
    {
        flight.departure =  Util.convertStringToDate(flight.departure);
        flight.arrival =    Util.convertStringToDate(flight.arrival);
        
        /* Paths */
        angular.forEach(flight.pathList, function(path)
        {
            path.slot = Util.convertStringToDate(path.slot);
        });
        
        /* Fares */
        angular.forEach(flight.fareList, function(fare)
        {
            if(fare.type === 'BUSINESS'){
                $scope.businessFare = fare;
            }else if(fare.type === 'TOURIST'){
                $scope.touristFare = fare;
            }else if(fare.type === 'OFFER'){
                $scope.offerFare = fare;
            }
        });
        
        /* Seats */
        $scope.businessSeatList = new Array();
        $scope.touristSeatList = new Array();
        $scope.offerSeatList = new Array();
        
        angular.forEach(flight.seatList, function(seat){
            
            seat.selected = false;
            
            if(seat.type === 'BUSINESS')
            {
                $scope.businessSeatList.push(seat);
            }
            else if(seat.type === 'TOURIST')
            {
                $scope.touristSeatList.push(seat);
            }
            else if(seat.type === 'OFFER')
            {
                $scope.offerSeatList.push(seat);
            }
        });
        return flight;
    };
    
    $scope.countReservesByUser = function()
    {
        FlightService.countReservesByUserId($scope.session.user.id).success(function(data){
            $scope.countReserves = data.count;
        });
    };
    
    $scope.exitAndSaveChanges = function()
    {
        if($scope.numSeatSelected > 0)
        {
            DialogService.openConfirmationDialog({
                title: 'Salir',
                message: '¿Desea mantener la reserva actual en "Vuelos en Trámite" para continuar posteriormente?',
                accept: function(){
                    $scope.saveFlightReserve(false,true);
                    $timeout(function(){
                        $location.path( '/client/' + $scope.session.user.username + '/dashboard');
                    }, 1000);
                },
                cancel: function(){
                    $location.path('/dashboard');
                }
            });            
        }else
        {
            $location.path('/dashboard');
        }
    };

    /*************************************** Initializer methods *********************************/

    $scope.init = function()
    {
        $scope.session = SessionService.getSession();
        FlightService.getByIdWithDetails($routeParams.flightId).success(function(data){
            $scope.flight = $scope.prepareFlightReserve(data);
        });
        
        $scope.countReservesByUser();
        
    };

    $scope.init();
}]);