/* FlightResevePay Controller */

'use strict';

controllers.controller('FlightReservePayController', 
    ['$scope','$modalInstance','DialogService','flight', '$location','SessionService',
        function($scope,$modalInstance,DialogService, flight, $location,SessionService){
  
    /******************************************* Attributes **************************************/

    $scope.session = null;

    /******************************************** Methods ****************************************/
    
    $scope.printBoardingCar = function()
    {
        $modalInstance.close();
        DialogService.openConfirmationDialog({
            title:'Tarjeta de embarque',
            message:'La tarjeta de embarque se ha imprimido con éxito',
            accept: function(){
                $location.path('/client/' + $scope.session.user.username + '/dashboard');
            },
            cancel: function()
            {
                $location.path('/client/' + $scope.session.user.username + '/dashboard');
            }
        });
    };
    
    $scope.submit = function()
    {
        $scope.printBoardingCar();
    };

    $scope.cancel = function()
    {
        $location.path('/client/' + $scope.session.user.username + '/dashboard');
        $modalInstance.dismiss('cancel');
    };
    /*************************************** Initializer methods *********************************/

    $scope.init = function()
    {
        $scope.flight = flight;
        $scope.session = SessionService.getSession();
    };

    $scope.init();
    
}]);