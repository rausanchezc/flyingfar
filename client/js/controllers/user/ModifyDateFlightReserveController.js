/* MofidySeatFlightReserve Controller */

'use strict';

controllers.controller('ModifyDateFlightReserveController',
                        ['$scope','DialogService','$routeParams','SessionService','FlightService','$modalInstance','flightInfo',
                        function($scope,DialogService,$routeParams, SessionService, FlightService, $modalInstance, flightInfo){
        
    /******************************************* Attributes **************************************/

    $scope.dialogTitle = 'Cambiar Fecha/Hora';
    $scope.session = null;
    $scope.businessSeatList = new Array();
    $scope.touristSeatList = new Array();
    $scope.offerSeatList = new Array();
    
    $scope.flight = null;
    $scope.newFlight = null;
    $scope.numSeatSelected = 0;
    
    /******************************************** Methods ****************************************/

    $scope.submit = function()
    {
         DialogService.openConfirmationDialog({
            title:'Asientos',
            message:'Se dispone a modificar su reserva,  ¿Desea continuar con la operación?',
            accept: function()
            { 
                // arg1: paid , arg2:pending
                var flight = $scope.prepareFlightForSave(false,false);
                
                FlightService.modifyFlightReserve(flight).success(function(data){
                    $modalInstance.close();
                });
            }
        });
    };
      
    $scope.cancel = function()
    {
        $modalInstance.dismiss('cancel');
    };
    
    
    $scope.saveFlight = function(paid,pending,canceled)
    {
        var flight = $scope.prepareFlighReserveForSave(paid,pending,canceled);        
        FlightService.reserve(flight).success(function(data){});        
    };
    
    $scope.prepareFlightForSave = function(paid,pending,canceled)
    {
        //Seats
        var idListReserveSeat = new Array();
        
        angular.forEach($scope.businessSeatList, function(seat){
            if(seat.selected){idListReserveSeat.push(seat.id);}
        });
        
        angular.forEach($scope.touristSeatList, function(seat){
           if(seat.selected){idListReserveSeat.push(seat.id);}
        });

        angular.forEach($scope.offerSeatList, function(seat){
            if(seat.selected){idListReserveSeat.push(seat.id);}
        });
        
        var flight = {
            userId: $scope.session.user.id,
            flightId: $scope.newFlight.id,
            seatList: idListReserveSeat,
            paid: paid,
            pending: pending,
            canceled: canceled,
            oldReserveId: $scope.flight.reserveId
        }; 
        
        return flight;
    };
    
    /**
     * 
     * @returns {undefined}
     */
    $scope.prepareFlightSeats = function()
    {
        FlightService.getByIdWithDetails($scope.newFlight.id).success(function(data)
        {
            angular.forEach(data.seatList, function(seat){
                
                if(seat.type === "BUSINESS")
                {
                    $scope.businessSeatList.push(seat);
                }
                
                if(seat.type === "TOURIST")
                {
                    $scope.touristSeatList.push(seat);
                }
                
                if(seat.type === "OFFER")
                {
                    $scope.offerSeatList.push(seat);
                }
            });
            
            //Fares
            angular.forEach($scope.newFlight.fareList, function(fare){
                if(fare.type === 'BUSINESS'){
                    $scope.businessFare = fare;
                }else if(fare.type === 'TOURIST')
                {
                    $scope.touristFare = fare;
                }else if(fare.type === 'OFFER')
                {
                    $scope.offerFare = fare;
                }
            });
        });
    };
    
    /**
     * @param {type} seat
     * @returns {undefined}
     */
    $scope.selectSeat = function(seat)
    {
        if(seat.reserved !== null && angular.isUndefined(seat.reserved) && !seat.reserved)
        {
            seat.selected = !seat.selected;
            
            if(seat.selected){
                $scope.numSeatSelected ++; 
            }else{
                $scope.numSeatSelected --;
            }
        }
    };
    
    
    $scope.searchFlights = function()
    {
        var flight = {
            origin:         $scope.flight.origin,
            destination:    $scope.flight.destination,
            seats:          $scope.flight.numSeats,
            roundTrip:      $scope.flight.roundTrip,
            departure:      Util.dateTOString($scope.flight.departure)
        };
        
        FlightService.getByRestrictions(flight).success(function(data){
            $scope.flightResultList = data.flightList;
            
            angular.forEach($scope.flightResultList, function(flight){
                flight.departure = Util.convertStringToDate(flight.departure);
            });
        });
    };
    
    
    $scope.isValidFare= function(flight, fare)
    {
        var maxSeats = 0;
        if(fare === 'business')
        {
            maxSeats = flight.plane.maxBusinessSeat;
        }else if(fare === 'tourist')
        {
            maxSeats = flight.plane.maxTouristSeat;
        }else if(fare === 'offer')
        {
            maxSeats = flight.plane.maxOfferSeat;
        }
        return (maxSeats > flight.countReservedOrPaid[fare]);
    };
    
    
    $scope.selectFlight = function(flight)
    {
        if($scope.newFlight !== null && $scope.newFlight.selected !== undefined)
        {
            $scope.newFlight.selected = false;
        }
        
        $scope.newFlight = flight;
        flight.selected = true;
        
        $scope.cleanFlight();
        $scope.prepareFlightSeats();
    };
    
    $scope.cleanFlight = function()
    {
        $scope.businessSeatList = new Array();
        $scope.touristSeatList = new Array();
        $scope.offerSeatList = new Array();
  
    };
    
    /*************************************** Initializer methods *********************************/

    $scope.init = function()
    {
        $scope.session = SessionService.getSession();
        $scope.flight = flightInfo;
        $scope.searchFlights();
        
//        $scope.prepareFlightSeats();
    };
    
    $scope.init();
}]);