/* MofidySeatFlightReserve Controller */

'use strict';

controllers.controller('ModifySeatFlightReserveController',
                        ['$scope','DialogService','$routeParams','SessionService','FlightService','$modalInstance','flightInfo',
                        function($scope,DialogService,$routeParams, SessionService, FlightService, $modalInstance,flightInfo){
        
    /******************************************* Attributes **************************************/

    $scope.dialogTitle = 'Cambiar asientos';
    
    $scope.businessSeatList = new Array();
    $scope.touristSeatList = new Array();
    $scope.offerSeatList = new Array();
    
    $scope.numSeatSelected = 0;
    
    /******************************************** Methods ****************************************/

    $scope.submit = function()
    {
        var reserve = $scope.prepareFlightSeatForUpdate();
        
         DialogService.openConfirmationDialog({
            title:'Asientos',
            message:'Se dispone a modificar los asientos de su reserva,  ¿Desea continuar con la operación?',
            accept: function()
            { 
                FlightService.modifySeatReserveList(reserve).success(function(data,status){

                    if(status === 204)
                    {
                        $modalInstance.close();
                    }
                });
            }
        });

        
    };
      
    $scope.cancel = function()
    {
        $modalInstance.dismiss('cancel');
    };
    
    
    $scope.prepareFlightSeatForUpdate = function()
    {
        //Seats
        var idListReserveSeat = new Array();
        
        angular.forEach($scope.businessSeatList, function(seat){
            if(seat.selected){idListReserveSeat.push(seat.id);}
        });
        
        angular.forEach($scope.touristSeatList, function(seat){
           if(seat.selected){idListReserveSeat.push(seat.id);}
        });

        angular.forEach($scope.offerSeatList, function(seat){
            if(seat.selected){idListReserveSeat.push(seat.id);}
        });
        
        var reserveFlight = {
            id: flightInfo.reserveId,
            seatList: idListReserveSeat
        };
        return reserveFlight;
    };
    
    /**
     * 
     * @returns {undefined}
     */
    $scope.prepareFlightSeats = function()
    {
        FlightService.getByIdWithDetails(flightInfo.flightId).success(function(data)
        {
            angular.forEach(data.seatList, function(seat){
                
                angular.forEach(flightInfo.myReservedSeatList, function(mySeat)
                {
                    if(seat.id === mySeat.id)
                    {
                        delete seat.reserved;
                        seat.editable = true;
                    }
                });
                
                if(seat.type === "BUSINESS")
                {
                    $scope.businessSeatList.push(seat);
                }
                
                if(seat.type === "TOURIST")
                {
                    $scope.touristSeatList.push(seat);
                }
                
                if(seat.type === "OFFER")
                {
                    $scope.offerSeatList.push(seat);
                }
            });
            
            //Fares
            angular.forEach(data.fareList, function(fare){
                if(fare.type === 'BUSINESS'){
                    $scope.businessFare = fare;
                }else if(fare.type === 'TOURIST')
                {
                    $scope.touristFare = fare;
                }else if(fare.type === 'OFFER')
                {
                    $scope.offerFare = fare;
                }
            });
        });
    };
    
    /**
     * @param {type} seat
     * @returns {undefined}
     */
    $scope.selectSeat = function(seat)
    {
        if(seat.reserved !== null && angular.isUndefined(seat.reserved) && !seat.reserved)
        {
            seat.selected = !seat.selected;
            
            if(seat.selected){
                $scope.numSeatSelected ++; 
            }else{
                $scope.numSeatSelected --;
            }
        }
    };
    
    
    /*************************************** Initializer methods *********************************/

    $scope.init = function()
    {
        $scope.prepareFlightSeats();
    };
    
    $scope.init();
}]);