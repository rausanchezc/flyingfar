/* MyFlightList Controller */

'use strict';

controllers.controller('MyFlightListController',['$scope','DialogService','$routeParams','SessionService','FlightService',
                                            function($scope,DialogService,$routeParams, SessionService, FlightService){
        
    /******************************************* Attributes **************************************/

    $scope.session = null;
    $scope.pendingFlightList = new Array();
    $scope.reservedFlightList = new Array();
    $scope.paidFlightList = new Array();

    /******************************************** Methods ****************************************/


    $scope.prepareMyFlightList = function()
    {
        
        FlightService.getFlightListByUserId($scope.session.user.id).success(function(data){
            
            angular.forEach(data.pendingFlights, function(flight)
            {
                flight.departure = Util.convertStringToDate(flight.departure);
                flight.arrival = Util.convertStringToDate(flight.arrival);
                flight.reserveDate = Util.convertStringToDate(flight.reserveDate);
                
                angular.forEach(flight.seatReservedList, function(seat){
                    if(seat.type === "TOURIST"){
                        flight.hasTouristSeat = true;
                    }else if(seat.type === "BUSINESS"){
                        flight.hasBusinessSeat = true;
                    }else if(seat.type === "OFFER"){
                        flight.hasOfferSeat = true;
                    }
                });
            });
            
            angular.forEach(data.reservedFlights, function(flight)
            {
                flight.departure = Util.convertStringToDate(flight.departure);
                flight.arrival = Util.convertStringToDate(flight.arrival);
                flight.reserveDate = Util.convertStringToDate(flight.reserveDate);
                
                angular.forEach(flight.seatReservedList, function(seat){
                    if(seat.type === "TOURIST"){
                        flight.hasTouristSeat = true;
                    }else if(seat.type === "BUSINESS"){
                        flight.hasBusinessSeat = true;
                    }else if(seat.type === "OFFER"){
                        flight.hasOfferSeat = true;
                    }
                });

            });
            
            angular.forEach(data.paidFlights, function(flight)
            {
                flight.departure = Util.convertStringToDate(flight.departure);
                flight.arrival = Util.convertStringToDate(flight.arrival);
                flight.reserveDate = Util.convertStringToDate(flight.reserveDate);
                
                angular.forEach(flight.seatReservedList, function(seat){
                    if(seat.type === "TOURIST"){
                        flight.hasTouristSeat = true;
                    }else if(seat.type === "BUSINESS"){
                        flight.hasBusinessSeat = true;
                    }else if(seat.type === "OFFER"){
                        flight.hasOfferSeat = true;
                    }
                });

            });
            
            $scope.pendingFlightList = data.pendingFlights;
            $scope.reservedFlightList = data.reservedFlights;
            $scope.paidFlightList = data.paidFlights;
        });
    };
    
    
    $scope.canModifyFlight = function(flight)
    { 
        return !flight.hasOfferSeat && (new Date().getTime() + (1000 * 60 * 60 *24 ) < flight.departure.getTime());
    };
    
    $scope.printBoardingCard = function(flight)
    {
        FlightService.getByIdWithDetails(flight.flightId).success(function(data){

            var flightBoardingCard = {
                code: data.code,
                origin: data.originName,
                destination: data.destinationName,
                user: $scope.session.user,
                airline: data.airline,
                pathList:data.pathList
            };
            
            DialogService.openDialog({
                templateUrl: 'fragments/views/user/FlightReservePayDialog.html',
                controller: 'FlightReservePayController',
                resolve: {
                    flight:function(){ return flightBoardingCard;}
                },
                accept: function() {

                }
            })
            
        });
        
    };
    
    /**
     * @param {type} flight
     * @returns {undefined}
     */
    $scope.openCancelFlightDialog = function(flight)
    {
        DialogService.openConfirmationDialog({
            title:'Reserva',
            message:'Se dispone a eliminar esta reserva, esta operación es irreversible ¿Esta seguro de continuar?',
            accept: function(){                
                $scope.cancelFlight(flight);
            }
        });
    };
    
    /**
     * @param {type} flight
     * @returns {undefined}
     */
    $scope.cancelFlight = function(flight)
    {
        FlightService.deleteReserveFlight(flight.reserveId).success(function(data){
            $scope.prepareMyFlightList();
        });
    };
    
    /**
     * @param {type} flight
     * @returns {undefined}
     */
    $scope.openPayFlightDialog = function(flight)
    {
         DialogService.openConfirmationDialog({
            title:'Reserva',
            message:'Se dispone a pagar esta reserva,  ¿Desea continuar con el pago?',
            accept: function(){                
               $scope.payFlight(flight);
            }
        });
    };
    
    
    /**
     * @param {type} flight
     * @returns {undefined}
     */
    $scope.payFlight = function(flight)
    {
        FlightService.payReserveFlight(flight.reserveId).success(function(data){
            $scope.prepareMyFlightList();
        });
    };
    
    
    $scope.openModifySeatsFlightDialog = function(flight)
    {
        DialogService.openDialog({
            templateUrl:'fragments/views/user/ModifySeatFlightReserveDialog.html',
            controller:'ModifySeatFlightReserveController',
            resolve:{
                flightInfo:function()
                {
                    return {
                        flightId: flight.flightId , 
                        reserveId: flight.reserveId,
                        myReservedSeatList : flight.seatReservedList};
                }
            },
            accept:function(){
                $scope.prepareMyFlightList();
            }
        });
       
    };
    
    $scope.openModifyDateFlightDialog = function(flight)
    {
        DialogService.openDialog({
            templateUrl:'fragments/views/user/ModifyDateFlightReserveDialog.html',
            controller:'ModifyDateFlightReserveController',
            resolve:{
                flightInfo: function()
                {
                    return flight;
                }
            },
            accept:function(){

            }
        }); 
    }
    
    /*************************************** Initializer methods *********************************/

    $scope.init = function()
    {
        $scope.session = SessionService.getSession();
        $scope.prepareMyFlightList();
    };
    
    $scope.init();
}]);