'use strict';

var allModules = [
    'ngRoute',
    'flyingfarServices',
    'flyingfarDirectives',
    'flyingfarControllers',
    'ui.bootstrap',
    'ui.bootstrap.datetimepicker'
];

var app = angular.module('flyingfar', allModules);

app.config(['$routeProvider','$logProvider','$httpProvider', 
    function($routeProvider, $logProvider,$httpProvider){
        
        $logProvider.debugEnabled(true);
        
//        $httpProvider.defaults.useXDomain = true;
        $httpProvider.defaults.withCredentials = true;
//        delete $httpProvider.defaults.headers.common["X-Requested-With"];
//        $httpProvider.defaults.headers.common["Accept"] = "application/json";
//        $httpProvider.defaults.headers.common["Content-Type"] = "application/json";

        $routeProvider.when('/dashboard', {
            templateUrl:'fragments/views/Dashboard.html',
            controller:'DashboardController'
        });
        

        $routeProvider.when('/login', {
            templateUrl:'fragments/views/Login.html',
            controller:'LoginController'
        });
        
        $routeProvider.when('/admin/login',{
            templateUrl:'fragments/views/admin/AdminLogin.html',
            controller:'AdminLoginController'
        });
    /* *********** System Administration Routes ************** */
    
        $routeProvider.when('/admin/sys/admins', {
            templateUrl:'fragments/views/admin/AdminList.html',
            controller:'AdminListController'
        });
        $routeProvider.when('/admin/sys/airlines',{
            templateUrl:'fragments/views/admin/AirlineList.html',
            controller:'AirlineListController'
        });
        $routeProvider.when('/admin/sys/airports',{
            templateUrl:'fragments/views/admin/AirportList.html',
            controller:'AirportListController'
        });
        
    /* *********** Airline Administration Routes *****************/
        
        $routeProvider.when('/admin/airline/planes',{
            templateUrl:'fragments/views/admin/PlaneList.html',
            controller:'PlaneListController'
        });
        $routeProvider.when('/admin/airline/flights',{
            templateUrl:'fragments/views/admin/FlightList.html',
            controller:'FlightListController'
        });
        $routeProvider.when('/admin/airline/reports',{
            templateUrl:'fragments/views/admin/Reports.html',
            controller:'ReportsController'
        });
        
        
        /* *********** Client Routes *****************/

        $routeProvider.when('/client/:username/dashboard',{
            templateUrl:'fragments/views/user/MyFlightList.html',
            controller:'MyFlightListController'
        });
        $routeProvider.when('/flight/:flightId/reserve',{
            templateUrl:'fragments/views/user/FlightDetails.html',
            controller:'FlightDetailsController'
        });
            
        $routeProvider.otherwise({
            redirectTo:'/dashboard'
        });
}]);

app.run(['$rootScope', '$location','SessionService', function($rootScope, $location, SessionService){
        
        var routes = [
            {url:'/dashboard',                     access:['ROLE_ANONYMOUS','ROLE_CLIENT']},
            {url:'/admin/sys/admins',              access:['ROLE_SYSTEM_ADMIN']},
            {url:'/admin/sys/airlines',            access:['ROLE_SYSTEM_ADMIN']},
            {url:'/admin/sys/airports',            access:['ROLE_SYSTEM_ADMIN']},
            {url:'/admin/airline/flights',         access:['ROLE_AIRLINE_ADMIN']},
            {url:'/admin/airline/planes',          access:['ROLE_AIRLINE_ADMIN']},
            {url:'/admin/airline/reports',         access:['ROLE_AIRLINE_ADMIN']}
        ];
        
    $rootScope.$on('$locationChangeStart', function (event, nextLocation, currentLocation) {
        var index = 0;

        while(index < routes.length) {
            var route = routes[index];
            if(new RegExp(route.url + '$', 'g').test(nextLocation)) {
                var userRoles = SessionService.getSession().roleList || ['ROLE_ANONYMOUS'];
                var authorized = false;

                for (var index = 0; index < userRoles.length; index++) {
                     if(route.access.indexOf(userRoles[index]) !== -1) {
                         authorized = true;
                         break;
                     }
                }
                if(!authorized) {
                    if(new RegExp('/login$', 'g').test(nextLocation)) {
                        $location.path('/dashboard');
                    } else {
                        var adminRole = null;
                        angular.forEach(userRoles, function(rol){
                            if(rol === 'ROLE_SYSTEM_ADMIN' || rol === 'ROLE_AIRLINE_ADMIN')
                            { adminRole = rol;}
                        });
                        if(adminRole)
                        {
                            if(adminRole === 'ROLE_SYSTEM_ADMIN')
                            {
                                $location.path('/admin/sys/admins');
                            }else{
                                $location.path('/admin/airline/flights'); 
                            }
                        }
                        else{
                            $location.path('/dashboard');
                        }
                    }
                }
                break;
            }
            index++;
        }
    });
    
    $rootScope.isLoginAdmin = false;
}]);