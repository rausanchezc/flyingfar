
'use strict';

var controllers = angular.module('flyingfarControllers',[]);

controllers.controller('GlobalController',[ '$scope','SessionService','DialogService','$location','$rootScope','UserService',
    function($scope,SessionService, DialogService, $location, $rootScope,UserService){
    
    /********************************* Attributes ************************************/
        
    $scope.session = SessionService.getSession();
    $scope.message = '';
    $rootScope.isLoginAdmin = false;
    /******************************************** Methods ****************************************/
    
    /**
     * @param {type} user
     * @returns {undefined}
     */
    $scope.login = function(user) {
        $scope.message = '';

        if($scope.isValid(user)) {
            $scope.message = 'Comprobando…';

            UserService.getRoleByUsername(user.username).success(function(data){
                if(data.role.name === "ROLE_CLIENT")
                {
                    SessionService.login(user.username, user.password).success(function(data) {
                        $location.path('/dashboard');
                        user.username = '';
                        user.password = '';
                        $scope.message = '';
                    }).error(function(data, status) {
                        switch (status) {
                            case 401:
                                user.password = '';
                                $scope.message = 'Nombre de usuario o contraseña incorrectos.';
                                break;
                            default:
                                $scope.message = 'Se ha producido un error en la conexión.';
                        }
                    });
                }
                else
                {
                    $scope.message = 'Nombre de usuario o contraseña incorrectos.';
                }
            });
        }
    };
    
    /**
     * @returns {undefined}
     */
    $scope.submitLogin = function()
    {
        if(event.keyCode === 13) {
            $scope.login($scope.user);
        }
    };
    
    /**
     * @returns {undefined}
     */
    $scope.logout = function() 
    {
        SessionService.logout().success(function(data) {
            $location.path('/dashboard');
        });
    };

    /* ******************* Utils Functions ************************* */    
    /**
     * @param {type} user
     * @returns {Boolean}
     */
    $scope.isValid = function(user) {
        return user && user.username && user.username.length > 2 && user.password && user.password.length > 2;
    };
    
    
    $scope.gotoAdminLogin = function()
    {
        $location.path('/admin/login');
        $rootScope.isLoginAdmin = true;
    };
    
    $scope.openSignupClient = function()
    {
        DialogService.openDialog({
            templateUrl:'fragments/views/user/CreateClientDialog.html',
            controller:'CreateClientController',
            resolve:{
                currentClient: function(){
                    return null;
                }
            },
            accept:function(){
            }
        });
    };
    

    $rootScope.adminNavegationList = {
        sys: [{
            title:'Administradores', 
            url:'#/admin/sys/admins',
            enabled:true,
            active:true
        }, {
            title:'Aerolíneas',
            url:'#/admin/sys/airlines',
            enabled:true,
            active:false
        }, {
            title:'Aeropuertos',
            url:'#/admin/sys/airports',
            enabled:true,
            active:false
        }],
        company:[{
            title:'Vuelos', 
            url:'#/admin/airline/flights',
            enabled:true,
            active:true
        },{
            title:'Aviones',
            url:'#/admin/airline/planes',
            enabled:true,
            active:false
        },{
            title:'Informes', 
            url:'#/admin/airline/reports',
            enabled:true,
            active:false
        }]
    };
    
    $scope.clickItemMenu = function(section, position)
    {
        angular.forEach(section, function(item){
            item.active = false;
        });
     
        section[position].active = true;
    };
    
}]);
