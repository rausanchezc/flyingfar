'use strict';

var flyingfarDirectives = angular.module('flyingfarDirectives', []);

flyingfarDirectives.directive('stopEvent', function () {
        return {
            restrict: 'A',
            link: function (scope, element, attr) {
                element.bind(attr.stopEvent, function (e) {
                    e.stopPropagation();
                });
            }
        };
});

flyingfarDirectives.directive('historyBack', ['$window', function($window) {
    return {
        restrict: 'A',
        link: function(scope, element, attributes) {
            element.on('click', function() {
                $window.history.back();
            });
        }
    };
}]);
