/* CompanyAdminService */

'use strict';


flyingfarServices.factory('AdminCompanyService',['$http', function($http){
    return{
        ROLE_AIRLINE_ADMIN:'ROLE_AIRLINE_ADMIN',
        create: function(admin)
        {
            
            var admin = {
                username:       admin.username      ? admin.username :       undefined,
                password:       admin.password      ? admin.password :       undefined,
                name:           admin.name          ? admin.name     :       undefined,
                firstSurname:   admin.firstSurname  ? admin.firstSurname:    undefined,
                secondSurname:  admin.secondSurname ? admin.secondSurname:   undefined,
                address:        admin.address       ? admin.address  :       undefined,
                email:          admin.email         ? admin.email    :       undefined,
                creditCard:     admin.creditCard    ? admin.creditCard :     undefined,
                airlineId:      admin.airlineId     ? admin.airlineId  :     undefined,
                role:           this.ROLE_AIRLINE_ADMIN
            };
            
            return $http({
                url: SERVER_URL + '/user',
                method:'POST',
                data: admin
            });
        },
        update: function(admin)
        {
            admin.role = this.ROLE_AIRLINE_ADMIN;
            return $http({
                url: SERVER_URL + '/user/' + admin.id,
                method:'PUT',
                data: admin
            });
        },
        getById: function(adminId)
        {
            return $http({
                url: SERVER_URL + '/user/' + adminId,
                method:'GET'
            });
        },
        getByUsername: function(username)
        {
            return $http.get(SERVER_URL +'/user?username='+ username);
        },
        delete: function(adminId)
        {
            return $http({
                url: SERVER_URL + '/user/' + adminId,
                method: 'DELETE'
            });
        }
    };
}]);