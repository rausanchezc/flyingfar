/* AirlineService */

'use strict';

flyingfarServices.factory('AirlineService', ['$http', function($http){
        return{
            URL: SERVER_URL + '/airline',
            create:function(airline)
            {
                var airline = {
                    code:       airline.code ? airline.code : undefined,
                    fullName:   airline.fullName ? airline.fullName : undefined
                };
                
                return $http({
                    url: this.URL,
                    method:'POST',
                    data: airline
                });
            },
            update: function(airline)
            {
                return $http({
                    url: this.URL + '/' + airline.id,
                    method: 'PUT',
                    data: airline
                });
            },
            delete: function(airlineId)
            {
                return $http({
                    url: this.URL +'/'+ airlineId,
                    method:'DELETE'
                });
            },
            getAll: function()
            {
                return $http.get(this.URL + '/getAll');
            },
            getByUserId: function(userId)
            {
                return $http.get(this.URL + '/getByUserId/' + userId);
            }
        };
}]);