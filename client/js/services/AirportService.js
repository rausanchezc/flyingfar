/* Airport Service */

'use strict';

flyingfarServices.factory('AirportService', ['$http', function($http){
        return{
            URL: SERVER_URL + '/airport',
            create:function(airport)
            {
                var airport = {
                    code:       airport.code ? airport.code : undefined,
                    name:       airport.name ? airport.name : undefined,
                    cityId:     airport.cityId ? airport.cityId : undefined
                };
                
                return $http({
                    url: this.URL,
                    method:'POST',
                    data: airport
                });
            },
            update: function(airport)
            {
                return $http({
                    url: this.URL + '/' + airport.id,
                    method: 'PUT',
                    data: airport
                });
            },
            delete: function(airportId)
            {
                return $http({
                    url: this.URL +'/'+ airportId,
                    method:'DELETE'
                });
            },
            getAll: function()
            {
                return $http.get(this.URL + '/getAll');
            }
        };
}]);