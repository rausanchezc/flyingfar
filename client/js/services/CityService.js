/* CityService */

'use strict';

flyingfarServices.factory('CityService', ['$http', function($http){
        return{
            URL: SERVER_URL + '/city',
            getAll: function()
            {
                return $http.get(this.URL + '/getAll');
            }
        };
}]);