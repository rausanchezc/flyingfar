/* Client Service */

'use strict';

flyingfarServices.factory('ClientService', ['$http', function($http){
        return{
          CLIENT_ROLE:'ROLE_CLIENT',
          URL : SERVER_URL + '/user',
          create: function(client)
          {
              var client = {
                name:           client.name          ? client.name :           undefined,
                username:       client.username      ? client.username :       undefined,
                password:       client.password      ? client.password :       undefined,
                firstSurname:   client.firstSurname  ? client.firstSurname:    undefined,
                secondSurname:  client.secondSurname ? client.secondSurname:   undefined,
                address:        client.address       ? client.address  :       undefined,
                email:          client.email         ? client.email    :       undefined,
                role:           this.CLIENT_ROLE                 
              };
              
              return $http({
                  url : this.URL,
                  method:'POST',
                  data:client
              });
          },
          update : function(client)
          {
              
          }
        };
}]);