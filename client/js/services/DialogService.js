/* Dialog Service */

'use strict';

flyingfarServices.factory('DialogService',['$modal', function($modal){
        return{
            openDialog:function(params){
                $modal.open({
                    templateUrl:params.templateUrl,
                    controller:params.controller,
                    resolve:params.resolve,
                    windowClass: params.smallDialog ? 'small-dialog':'',
                    keyboard:false,
                    backdrop:'static'
                }).result.then(params.accept, params.cancel);
            },
            openConfirmationDialog:function(params){
                $modal.open({
                    templateUrl:'fragments/views/commons/ConfirmationDialog.html',
                    controller:'ConfirmationDialogController',
                    resolve:{
                        dialog:function(){
                           return{ 
                               title: params.title,
                               message:params.message,
                               focusSubmit:params.autoSize || false,
                               hideFooter:params.hideFooter || false
                            };  
                        }
                    },
                    windowsClass:params.smallDialog ? 'small-dialog':''
                }).result.then(params.accept, params.cancel);
            },
            openHttpErrorDialog: function(data, status) {
                var messages = {
                    '400': 'El formato de la petición es incorrecto',
                    '401': 'No estas autorizado',
                    '403': 'No estas autorizado',
                    '404': 'No se han encontrado resultados',
                    '405': 'Método no permitido',
                    '500': 'Se ha producido un error interno en el servidor',
                    '422': 'Revise los campos erroneos'
                };

                if(status === 422) {
                    var message = '<br/><br/><ul class="list-unstyled">';
                    angular.forEach(data.errors, function(error) {
                        message += '<li><strong>' + error.field + ':</strong> ' + error.message + '</li>';
                    });
                    message += '</ul>';
                }

                $modal.open({
                    templateUrl: 'fragments/views/commons/ConfirmationDialog.html',
                    controller: 'ConfirmationDialogController',
                    resolve: {
                        dialog: function(){
                            return {
                                title: 'Información',
                                message: '<p>Código de estado <strong>' + status + '</strong></p><p>' + messages[status] + '</p>' + (message ? '<p>' + message + '</p>' : ''),
                                focusSubmit: true,
                                hideCancelButton: true
                            };
                        }
                    },
                    windowClass: "small-dialog"
                });
            }
        };
}]);
