/* Flight Service */

'use strict';

flyingfarServices.factory('FlightService',['$http', function($http){
    return{
      URL: SERVER_URL + '/flight',
      create: function(flight)
      {
          return $http({
              url : this.URL,
              method:'POST',
              data: flight
          });
      },
      update:function(flight)
      {
          return $http({
              url: this.URL + '/' + flight.id,
              method: 'PUT',
              data: flight
          });
      },
      delete:function(flightId)
      {
          return $http({
              url: this.URL +'/'+ flightId,
              method: 'DELETE'
          });
      },
      getAll:function()
      {
          return $http.get(this.URL + '/getAll');
      },
      getAllByAirlineId: function(airlineId)
      {
          return $http({
              url: this.URL + '/getAllByAirlineId/' + airlineId,
              method: 'GET'
          });
      },
      getByIdWithDetails: function(flightId)
      {
          return $http.get(this.URL +'/'+flightId);
      },
      getByRestrictions: function(restrictions)
      {
          return $http({
              url: this.URL + '/search',
              method: 'GET',
              params: restrictions
          });
      },
      reserve: function(reserveFlight)
      {
          var data = {
            userId : reserveFlight.userId,
            flightId: reserveFlight.flightId,
            seatList: reserveFlight.seatList,
            paid: reserveFlight.paid,
            pending: reserveFlight.pending,
            canceled: reserveFlight.canceled
          };  
          
          return $http({
              url: this.URL + '/reserve',
              method:'POST',
              data: data
          });
      },
      countReservesByUserId: function(userId)
      {
          return $http.get(this.URL + '/countReserves/' + userId);
      },
      getFlightListByUserId : function(userId)
      {
          return $http.get(this.URL + '/getAllByUserId/' + userId);
      },
      deleteReserveFlight: function(reserveFlightId)
      {
          return $http({
              url: this.URL + '/reserve/' + reserveFlightId,
              method: 'DELETE'
          });
      },
      payReserveFlight: function(reserveFlightId)
      {
          return $http({
              url : this.URL + '/reserve/' + reserveFlightId,
              method: 'PUT'
          });
      },
      modifySeatReserveList: function(reserve){
          
          var data = {
            seatList: reserve.seatList
          };
          
          return $http({
              url: this.URL + '/reserve/modifySeats/' + reserve.id,
              method: 'PUT',
              params: data
          });
      },
      modifyFlightReserve: function(flight){
          
        var data = {
            userId :    flight.userId,
            flightId:   flight.flightId,
            seatList:   flight.seatList,
            paid:       flight.paid,
            pending:    flight.pending,
            canceled:   flight.canceled
          };  
          
          return $http({
              url: this.URL + '/reserve/modify/' + flight.oldReserveId,
              method: 'POST',
              data: data
          });
      }
    };
}]);