/* Local storage Service */

'use strict';

flyingfarServices.factory('LocalStorageService', function() {
    return {
        isSupported: function() {
            return 'sessionStorage' in window && window['sessionStorage'];
        },
        setItem: function(key, value) {
            sessionStorage.setItem(key, JSON.stringify(value));
        },
        getItem: function(key) {
            return JSON.parse(sessionStorage.getItem(key));
        },
        removeItem: function(key) {
            sessionStorage.removeItem(key);
        },
        clear: function() {
            sessionStorage.clear();
        }
    };
});
