/* Plane Service */

'use strict';

flyingfarServices.factory('PlaneService',['$http',function($http){
        return{
            URL: SERVER_URL + '/plane',
            create:function(plane)
            {
                var plane = {
                    manufacturer:       plane.manufacturer,
                    model:              plane.model ? plane.model : undefined,
                    manufactureDate:    plane.manufactureDate,
                    maxBusinessSeats:   plane.businessSeats ? plane.businessSeats : undefined,
                    maxTouristSeats:    plane.touristSeats ? plane.touristSeats : undefined,
                    maxOfferSeats:      plane.offerSeats ? plane.offerSeats : undefined,
                    airlineId:          plane.airlineId ? plane.airlineId : undefined
                };
                
                return $http({
                    url: this.URL,
                    method:'POST',
                    data: plane
                });
            },
            update: function(plane)
            {
                var plane = {
                    id:                 plane.id,
                    manufacturer:       plane.manufacturer,
                    model:              plane.model ? plane.model : undefined,
                    manufactureDate:    plane.manufactureDate,
                    maxBusinessSeats:   plane.businessSeats ? plane.businessSeats : undefined,
                    maxTouristSeats:    plane.touristSeats ? plane.touristSeats : undefined,
                    maxOfferSeats:      plane.offerSeats ? plane.offerSeats : undefined
//                    airline:            plane.airline ?
                };
                
                return $http({
                    url: this.URL + '/' + plane.id,
                    method: 'PUT',
                    data: plane
                });
            },
            delete: function(planeId)
            {
                return $http({
                    url: this.URL +'/'+ planeId,
                    method:'DELETE'
                });
            },
            getAll : function()
            {
                return $http.get(this.URL + '/getAll');
            },
            getAllByAirlineId: function(airlineId)
            {
                return $http({
                    url: this.URL + '/getAllByAirline/' + airlineId,
                    method: 'GET'
                });
            }
        };
}]);