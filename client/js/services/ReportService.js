/* Report Service */

'use strict';

flyingfarServices.factory('ReportService',['$http',function($http){
    return{
        URL: SERVER_URL + '/reports',
        getFlightsWithSeatOccupation: function(airlineCode)
        {
            return $http.get( this.URL + '/seat-occupancy/' + airlineCode);
        }
    };
}]);