/* Session service */

'use strict';

flyingfarServices.factory('SessionService', [
    '$location', 'UserService', 'LocalStorageService',
    function($location, UserService, LocalStorageService) {

    var session = {};

    var init = function() {
        var storedSession = LocalStorageService.getItem('session');

        if(storedSession) {
            UserService.refreshSession().success(function(data) {
                if(data) {
                    for(var property in data) {
                        session[property] = data[property];
                    }
                    LocalStorageService.setItem('session', session);
                }
            }).error(function(data, status) {
                switch (status) {
                    case 401:
                    case 403:
                        for(var property in session) {
                            delete session[property];
                        }
                        LocalStorageService.removeItem('session');
                        $location.path('/dashboard');
                        break;
                }
            });

            for(var property in storedSession) {
                session[property] = storedSession[property];
            }   
        }
    };

    init();

    return {
        login: function(username, password) {
            return UserService.login(username, password).success(function(data) {
                for(var property in data) {
                    session[property] = data[property];
                }
                LocalStorageService.setItem('session', session);
            });
        },
        logout: function() {
            return UserService.logout().success(function(data) {
                for(var property in session) {
                    delete session[property];
                }
                LocalStorageService.removeItem('session');
            });
        },
        getSession: function() {
            return session;
        }
    };
}]);
