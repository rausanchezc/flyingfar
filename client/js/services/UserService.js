/* User Service */

'use strict';

flyingfarServices.factory('UserService', ['$http','DialogService', function($http, DialogService){
        return{
            URL: SERVER_URL + '/user',
            login: function(username, password){
                return $http({
                    url: SERVER_URL + '/login',
                    method: 'POST',
                    params:{
                        'username':username,
                        'password':password
                    }
                });
            },
            logout: function()
            {
                return $http({
                    url:SERVER_URL + '/logout',
                    method:'POST'
                }).error(DialogService.openHttpErrorDialog);
            },
            refreshSession: function() {
                return $http({
                    url: SERVER_URL + '/refresh',
                    method: 'POST'
                });
            },
            getRoleByUsername: function(username)
            {
                return $http({
                    url: this.URL +'/'+ username +'/role',
                    method: 'GET'
                });
            },
            existsUsername:function(username)
            {
                var params = {
                    username:username
                };
                
                return $http({
                    url : this.URL + '/exists/username',
                    method: 'GET',
                    params:params
                });
            },
            existsEmail:function(email)
            {
                var params = {
                    email: email
                };
                
                return $http({
                    url : this.URL + '/exists/email',
                    method: 'GET',
                    params: params
                });
            },
            getAdminCompanyList: function()
            {
                return $http({
                    url: this.URL + '/getAdminCompanyList',
                    method: 'GET'
                }).error(DialogService.openHttpErrorDialog);
            },
            delete:function(userId)
            {
                return $http({
                    url: this.URL + '/'+ userId,
                    method: 'DELETE'
                });
            }
        };
}]);