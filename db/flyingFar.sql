SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

DROP SCHEMA IF EXISTS `flyingfar` ;
CREATE SCHEMA IF NOT EXISTS `flyingfar` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `flyingfar` ;

-- -----------------------------------------------------
-- Table `flyingfar`.`AIRLINE`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `flyingfar`.`AIRLINE` ;

CREATE TABLE IF NOT EXISTS `flyingfar`.`AIRLINE` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `code` VARCHAR(4) NOT NULL,
  `fullName` VARCHAR(60) NULL,
  `creationDate` DATETIME NOT NULL,
  `lastModificationDate` DATETIME NOT NULL,
  `lastFlightCode` INT(7) NOT NULL DEFAULT 0000000,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `flyingfar`.`COUNTRY`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `flyingfar`.`COUNTRY` ;

CREATE TABLE IF NOT EXISTS `flyingfar`.`COUNTRY` (
  `id` INT NOT NULL,
  `code` VARCHAR(10) NOT NULL,
  `name` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `flyingfar`.`CITY`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `flyingfar`.`CITY` ;

CREATE TABLE IF NOT EXISTS `flyingfar`.`CITY` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `code` VARCHAR(20) NULL,
  `name` VARCHAR(100) NULL,
  `country` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_CITY_COUNTRY1_idx` (`country` ASC),
  CONSTRAINT `fk_CITY_COUNTRY1`
    FOREIGN KEY (`country`)
    REFERENCES `flyingfar`.`COUNTRY` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `flyingfar`.`AIRPORT`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `flyingfar`.`AIRPORT` ;

CREATE TABLE IF NOT EXISTS `flyingfar`.`AIRPORT` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `code` VARCHAR(45) NOT NULL,
  `name` VARCHAR(120) NULL,
  `city` INT UNSIGNED NOT NULL,
  `creationDate` DATETIME NOT NULL,
  `lastModificationDate` DATETIME NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_AIRPORT_CITY1_idx` (`city` ASC),
  CONSTRAINT `fk_AIRPORT_CITY1`
    FOREIGN KEY (`city`)
    REFERENCES `flyingfar`.`CITY` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `flyingfar`.`PLANE`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `flyingfar`.`PLANE` ;

CREATE TABLE IF NOT EXISTS `flyingfar`.`PLANE` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `manufacturer` VARCHAR(80) NOT NULL,
  `model` VARCHAR(45) NOT NULL,
  `totalSeats` INT(4) NOT NULL,
  `manufactureDate` DATETIME NULL,
  `maxBusinessSeats` INT(4) NOT NULL,
  `maxTouristSeats` INT(4) NOT NULL,
  `maxOfferSeats` INT(4) NOT NULL,
  `airline` INT UNSIGNED NULL,
  `creationDate` DATETIME NOT NULL,
  `lastModificationDate` DATETIME NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_PLANE_AIRLINE1_idx` (`airline` ASC),
  CONSTRAINT `fk_PLANE_AIRLINE1`
    FOREIGN KEY (`airline`)
    REFERENCES `flyingfar`.`AIRLINE` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `flyingfar`.`ROLE`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `flyingfar`.`ROLE` ;

CREATE TABLE IF NOT EXISTS `flyingfar`.`ROLE` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(60) NOT NULL,
  `label` VARCHAR(60) NULL,
  `description` VARCHAR(100) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `flyingfar`.`USER`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `flyingfar`.`USER` ;

CREATE TABLE IF NOT EXISTS `flyingfar`.`USER` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `email` VARCHAR(100) NOT NULL,
  `username` VARCHAR(48) NOT NULL,
  `password` VARCHAR(256) NOT NULL,
  `role` INT UNSIGNED NOT NULL,
  `airline` INT UNSIGNED NULL,
  `name` VARCHAR(60) NULL,
  `firstSurname` VARCHAR(60) NULL,
  `secondSurname` VARCHAR(60) NULL,
  `address` VARCHAR(100) NULL,
  `creditCard` INT(20) NULL,
  `lastModificationDate` DATETIME NOT NULL,
  `creationDate` DATETIME NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `username_UNIQUE` (`username` ASC),
  UNIQUE INDEX `email_UNIQUE` (`email` ASC),
  INDEX `fk_USER_ROLE1_idx` (`role` ASC),
  INDEX `fk_USER_AIRLINE1_idx` (`airline` ASC),
  CONSTRAINT `fk_USER_ROLE1`
    FOREIGN KEY (`role`)
    REFERENCES `flyingfar`.`ROLE` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_USER_AIRLINE1`
    FOREIGN KEY (`airline`)
    REFERENCES `flyingfar`.`AIRLINE` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `flyingfar`.`FLIGHT`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `flyingfar`.`FLIGHT` ;

CREATE TABLE IF NOT EXISTS `flyingfar`.`FLIGHT` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `code` VARCHAR(45) NOT NULL,
  `departure` DATETIME NOT NULL,
  `arrival` DATETIME NOT NULL,
  `origin` INT UNSIGNED NOT NULL,
  `destination` INT UNSIGNED NOT NULL,
  `plane` INT UNSIGNED NOT NULL,
  `creationDate` DATETIME NOT NULL,
  `lastModificationDAte` DATETIME NOT NULL,
  `logicDeletion` TINYINT(1) NULL DEFAULT 0,
  `roundTrip` TINYINT(1) NOT NULL,
  `amount` DECIMAL(10,4) NULL DEFAULT 0,
  `airline` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_FLIGHT_CITY1_idx` (`origin` ASC),
  INDEX `fk_FLIGHT_CITY2_idx` (`destination` ASC),
  INDEX `fk_FLIGHT_PLANE1_idx` (`plane` ASC),
  INDEX `fk_FLIGHT_AIRLINE1_idx` (`airline` ASC),
  CONSTRAINT `fk_FLIGHT_CITY1`
    FOREIGN KEY (`origin`)
    REFERENCES `flyingfar`.`CITY` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_FLIGHT_CITY2`
    FOREIGN KEY (`destination`)
    REFERENCES `flyingfar`.`CITY` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_FLIGHT_PLANE1`
    FOREIGN KEY (`plane`)
    REFERENCES `flyingfar`.`PLANE` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_FLIGHT_AIRLINE1`
    FOREIGN KEY (`airline`)
    REFERENCES `flyingfar`.`AIRLINE` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `flyingfar`.`AIRLINE_HAS_FLIGHT`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `flyingfar`.`AIRLINE_HAS_FLIGHT` ;

CREATE TABLE IF NOT EXISTS `flyingfar`.`AIRLINE_HAS_FLIGHT` (
  `airline` INT UNSIGNED NOT NULL,
  `flight` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`airline`, `flight`),
  INDEX `fk_AIRLINE_has_FLIGHT_FLIGHT1_idx` (`flight` ASC),
  INDEX `fk_AIRLINE_has_FLIGHT_AIRLINE1_idx` (`airline` ASC),
  CONSTRAINT `fk_AIRLINE_has_FLIGHT_AIRLINE1`
    FOREIGN KEY (`airline`)
    REFERENCES `flyingfar`.`AIRLINE` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_AIRLINE_has_FLIGHT_FLIGHT1`
    FOREIGN KEY (`flight`)
    REFERENCES `flyingfar`.`FLIGHT` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `flyingfar`.`PATH`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `flyingfar`.`PATH` ;

CREATE TABLE IF NOT EXISTS `flyingfar`.`PATH` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `position` INT NOT NULL,
  `flight` INT UNSIGNED NOT NULL,
  `origin` INT UNSIGNED NOT NULL,
  `destination` INT UNSIGNED NOT NULL,
  `slot` DATETIME NOT NULL,
  `duration` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_PATH_FLIGHT1_idx` (`flight` ASC),
  INDEX `fk_PATH_CITY1_idx` (`origin` ASC),
  INDEX `fk_PATH_CITY2_idx` (`destination` ASC),
  CONSTRAINT `fk_PATH_FLIGHT1`
    FOREIGN KEY (`flight`)
    REFERENCES `flyingfar`.`FLIGHT` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_PATH_CITY1`
    FOREIGN KEY (`origin`)
    REFERENCES `flyingfar`.`CITY` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_PATH_CITY2`
    FOREIGN KEY (`destination`)
    REFERENCES `flyingfar`.`CITY` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `flyingfar`.`AIRLINE_OPERATE_AIRPORT`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `flyingfar`.`AIRLINE_OPERATE_AIRPORT` ;

CREATE TABLE IF NOT EXISTS `flyingfar`.`AIRLINE_OPERATE_AIRPORT` (
  `airline` INT UNSIGNED NOT NULL,
  `airport` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`airline`, `airport`),
  INDEX `fk_AIRLINE_has_AIRPORT_AIRPORT1_idx` (`airport` ASC),
  INDEX `fk_AIRLINE_has_AIRPORT_AIRLINE1_idx` (`airline` ASC),
  CONSTRAINT `fk_AIRLINE_has_AIRPORT_AIRLINE1`
    FOREIGN KEY (`airline`)
    REFERENCES `flyingfar`.`AIRLINE` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_AIRLINE_has_AIRPORT_AIRPORT1`
    FOREIGN KEY (`airport`)
    REFERENCES `flyingfar`.`AIRPORT` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `flyingfar`.`SEAT_TYPE`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `flyingfar`.`SEAT_TYPE` ;

CREATE TABLE IF NOT EXISTS `flyingfar`.`SEAT_TYPE` (
  `id` INT UNSIGNED NOT NULL DEFAULT 1,
  `category` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `flyingfar`.`SEAT`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `flyingfar`.`SEAT` ;

CREATE TABLE IF NOT EXISTS `flyingfar`.`SEAT` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `plane` INT UNSIGNED NOT NULL,
  `seatType` INT UNSIGNED NOT NULL,
  `seatNumber` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_SEAT_PLANE1_idx` (`plane` ASC),
  INDEX `fk_SEAT_SEAT_TYPE1_idx` (`seatType` ASC),
  CONSTRAINT `fk_SEAT_PLANE1`
    FOREIGN KEY (`plane`)
    REFERENCES `flyingfar`.`PLANE` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_SEAT_SEAT_TYPE1`
    FOREIGN KEY (`seatType`)
    REFERENCES `flyingfar`.`SEAT_TYPE` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `flyingfar`.`RESERVE_FLIGHT`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `flyingfar`.`RESERVE_FLIGHT` ;

CREATE TABLE IF NOT EXISTS `flyingfar`.`RESERVE_FLIGHT` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `user` INT UNSIGNED NOT NULL,
  `flight` INT UNSIGNED NOT NULL,
  `reserveDate` DATETIME NOT NULL,
  `numSeats` INT(3) NULL,
  `isPaid` TINYINT(1) NOT NULL,
  `canceled` TINYINT(1) NULL,
  `pending` TINYINT(1) NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_USER_has_FLIGHT_FLIGHT1_idx` (`flight` ASC),
  INDEX `fk_USER_has_FLIGHT_USER1_idx` (`user` ASC),
  CONSTRAINT `fk_USER_has_FLIGHT_USER1`
    FOREIGN KEY (`user`)
    REFERENCES `flyingfar`.`USER` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_USER_has_FLIGHT_FLIGHT1`
    FOREIGN KEY (`flight`)
    REFERENCES `flyingfar`.`FLIGHT` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `flyingfar`.`TICKET`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `flyingfar`.`TICKET` ;

CREATE TABLE IF NOT EXISTS `flyingfar`.`TICKET` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `flyingfar`.`FARE`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `flyingfar`.`FARE` ;

CREATE TABLE IF NOT EXISTS `flyingfar`.`FARE` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `flyingfar`.`SEAT_TYPE_INTO_FLIGHT`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `flyingfar`.`SEAT_TYPE_INTO_FLIGHT` ;

CREATE TABLE IF NOT EXISTS `flyingfar`.`SEAT_TYPE_INTO_FLIGHT` (
  `seatType` INT UNSIGNED NOT NULL,
  `flight` INT UNSIGNED NOT NULL,
  `fare` DOUBLE NOT NULL,
  PRIMARY KEY (`seatType`, `flight`),
  INDEX `fk_SEAT_TYPE_has_FLIGHT_FLIGHT1_idx` (`flight` ASC),
  INDEX `fk_SEAT_TYPE_has_FLIGHT_SEAT_TYPE1_idx` (`seatType` ASC),
  CONSTRAINT `fk_SEAT_TYPE_has_FLIGHT_SEAT_TYPE1`
    FOREIGN KEY (`seatType`)
    REFERENCES `flyingfar`.`SEAT_TYPE` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_SEAT_TYPE_has_FLIGHT_FLIGHT1`
    FOREIGN KEY (`flight`)
    REFERENCES `flyingfar`.`FLIGHT` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `flyingfar`.`SEAT_INTO_RESERVE_FLIGHT`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `flyingfar`.`SEAT_INTO_RESERVE_FLIGHT` ;

CREATE TABLE IF NOT EXISTS `flyingfar`.`SEAT_INTO_RESERVE_FLIGHT` (
  `seat` INT UNSIGNED NOT NULL,
  `reserveFlight` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`seat`, `reserveFlight`),
  INDEX `fk_RESERVE_FLIGHT_has_SEAT_RESERVE_FLIGHT1_idx` (`reserveFlight` ASC),
  CONSTRAINT `fk_RESERVE_FLIGHT_has_SEAT_RESERVE_FLIGHT1`
    FOREIGN KEY (`reserveFlight`)
    REFERENCES `flyingfar`.`RESERVE_FLIGHT` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_RESERVE_FLIGHT_has_SEAT_SEAT1`
    FOREIGN KEY (`seat`)
    REFERENCES `flyingfar`.`SEAT` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `flyingfar`.`AIRLINE`
-- -----------------------------------------------------
START TRANSACTION;
USE `flyingfar`;
INSERT INTO `flyingfar`.`AIRLINE` (`id`, `code`, `fullName`, `creationDate`, `lastModificationDate`, `lastFlightCode`) VALUES (1, 'AA', 'America Airlines', now(), now(), 0);
INSERT INTO `flyingfar`.`AIRLINE` (`id`, `code`, `fullName`, `creationDate`, `lastModificationDate`, `lastFlightCode`) VALUES (2, 'AC', 'Air Canadá', now(), now(), 0);
INSERT INTO `flyingfar`.`AIRLINE` (`id`, `code`, `fullName`, `creationDate`, `lastModificationDate`, `lastFlightCode`) VALUES (3, 'AF', 'Air France', now(), now(), 0);
INSERT INTO `flyingfar`.`AIRLINE` (`id`, `code`, `fullName`, `creationDate`, `lastModificationDate`, `lastFlightCode`) VALUES (4, 'AI', 'Air India', now(), now(), 0);
INSERT INTO `flyingfar`.`AIRLINE` (`id`, `code`, `fullName`, `creationDate`, `lastModificationDate`, `lastFlightCode`) VALUES (5, 'AM', 'Aeroméxico', now(), now(), 0);
INSERT INTO `flyingfar`.`AIRLINE` (`id`, `code`, `fullName`, `creationDate`, `lastModificationDate`, `lastFlightCode`) VALUES (6, 'AO', 'Australian Airlines', now(), now(), 0);
INSERT INTO `flyingfar`.`AIRLINE` (`id`, `code`, `fullName`, `creationDate`, `lastModificationDate`, `lastFlightCode`) VALUES (7, 'AR', 'Aerolíneas Argentinas', now(), now(), 0);
INSERT INTO `flyingfar`.`AIRLINE` (`id`, `code`, `fullName`, `creationDate`, `lastModificationDate`, `lastFlightCode`) VALUES (8, 'AZ', 'Alitalia', now(), now(), 0);
INSERT INTO `flyingfar`.`AIRLINE` (`id`, `code`, `fullName`, `creationDate`, `lastModificationDate`, `lastFlightCode`) VALUES (9, 'BA', 'British Airways', now(), now(), 0);
INSERT INTO `flyingfar`.`AIRLINE` (`id`, `code`, `fullName`, `creationDate`, `lastModificationDate`, `lastFlightCode`) VALUES (10, 'CA', 'Air China', now(), now(), 0);
INSERT INTO `flyingfar`.`AIRLINE` (`id`, `code`, `fullName`, `creationDate`, `lastModificationDate`, `lastFlightCode`) VALUES (11, 'CO', 'Continental Airlines', now(), now(), 0);
INSERT INTO `flyingfar`.`AIRLINE` (`id`, `code`, `fullName`, `creationDate`, `lastModificationDate`, `lastFlightCode`) VALUES (12, 'CU', 'Cubana', now(), now(), 0);
INSERT INTO `flyingfar`.`AIRLINE` (`id`, `code`, `fullName`, `creationDate`, `lastModificationDate`, `lastFlightCode`) VALUES (13, 'DJ', 'Virgin Blue', now(), now(), 0);
INSERT INTO `flyingfar`.`AIRLINE` (`id`, `code`, `fullName`, `creationDate`, `lastModificationDate`, `lastFlightCode`) VALUES (14, 'EG', 'Japan Asia Airways', now(), now(), 0);
INSERT INTO `flyingfar`.`AIRLINE` (`id`, `code`, `fullName`, `creationDate`, `lastModificationDate`, `lastFlightCode`) VALUES (15, 'FG', 'Ariana Afghan Airlines', now(), now(), 0);
INSERT INTO `flyingfar`.`AIRLINE` (`id`, `code`, `fullName`, `creationDate`, `lastModificationDate`, `lastFlightCode`) VALUES (16, 'H2', 'Sky Airline', now(), now(), 0);
INSERT INTO `flyingfar`.`AIRLINE` (`id`, `code`, `fullName`, `creationDate`, `lastModificationDate`, `lastFlightCode`) VALUES (17, 'IB', 'Iberia', now(), now(), 0);
INSERT INTO `flyingfar`.`AIRLINE` (`id`, `code`, `fullName`, `creationDate`, `lastModificationDate`, `lastFlightCode`) VALUES (18, 'IC', 'Indian Airlines', now(), now(), 0);
INSERT INTO `flyingfar`.`AIRLINE` (`id`, `code`, `fullName`, `creationDate`, `lastModificationDate`, `lastFlightCode`) VALUES (19, 'IR', 'Iran Air', now(), now(), 0);
INSERT INTO `flyingfar`.`AIRLINE` (`id`, `code`, `fullName`, `creationDate`, `lastModificationDate`, `lastFlightCode`) VALUES (20, 'JK', 'Spanair', now(), now(), 0);
INSERT INTO `flyingfar`.`AIRLINE` (`id`, `code`, `fullName`, `creationDate`, `lastModificationDate`, `lastFlightCode`) VALUES (21, 'JL', 'Japan Airlines', now(), now(), 0);
INSERT INTO `flyingfar`.`AIRLINE` (`id`, `code`, `fullName`, `creationDate`, `lastModificationDate`, `lastFlightCode`) VALUES (22, 'KE', 'Korean Airlines', now(), now(), 0);
INSERT INTO `flyingfar`.`AIRLINE` (`id`, `code`, `fullName`, `creationDate`, `lastModificationDate`, `lastFlightCode`) VALUES (23, 'LG', 'Luxair', now(), now(), 0);
INSERT INTO `flyingfar`.`AIRLINE` (`id`, `code`, `fullName`, `creationDate`, `lastModificationDate`, `lastFlightCode`) VALUES (24, 'LH', 'Lufthansa', now(), now(), 0);
INSERT INTO `flyingfar`.`AIRLINE` (`id`, `code`, `fullName`, `creationDate`, `lastModificationDate`, `lastFlightCode`) VALUES (25, 'MX', 'Mexicana de Aviación', now(), now(), 0);
INSERT INTO `flyingfar`.`AIRLINE` (`id`, `code`, `fullName`, `creationDate`, `lastModificationDate`, `lastFlightCode`) VALUES (26, 'NI', 'Porgalia', now(), now(), 0);
INSERT INTO `flyingfar`.`AIRLINE` (`id`, `code`, `fullName`, `creationDate`, `lastModificationDate`, `lastFlightCode`) VALUES (27, 'NQ', 'Air Japan', now(), now(), 0);
INSERT INTO `flyingfar`.`AIRLINE` (`id`, `code`, `fullName`, `creationDate`, `lastModificationDate`, `lastFlightCode`) VALUES (28, 'NZ', 'Air New Zeland', now(), now(), 0);
INSERT INTO `flyingfar`.`AIRLINE` (`id`, `code`, `fullName`, `creationDate`, `lastModificationDate`, `lastFlightCode`) VALUES (29, 'OM', 'MIAT', now(), now(), 0);
INSERT INTO `flyingfar`.`AIRLINE` (`id`, `code`, `fullName`, `creationDate`, `lastModificationDate`, `lastFlightCode`) VALUES (30, 'OO', 'SkyWest', now(), now(), 0);
INSERT INTO `flyingfar`.`AIRLINE` (`id`, `code`, `fullName`, `creationDate`, `lastModificationDate`, `lastFlightCode`) VALUES (31, 'OS', 'Austrian Airlines', now(), now(), 0);
INSERT INTO `flyingfar`.`AIRLINE` (`id`, `code`, `fullName`, `creationDate`, `lastModificationDate`, `lastFlightCode`) VALUES (32, 'OU', 'Croatia Airlines', now(), now(), 0);
INSERT INTO `flyingfar`.`AIRLINE` (`id`, `code`, `fullName`, `creationDate`, `lastModificationDate`, `lastFlightCode`) VALUES (33, 'PE', 'Air Europe SPA', now(), now(), 0);
INSERT INTO `flyingfar`.`AIRLINE` (`id`, `code`, `fullName`, `creationDate`, `lastModificationDate`, `lastFlightCode`) VALUES (34, 'QR', 'Qatar Airways', now(), now(), 0);
INSERT INTO `flyingfar`.`AIRLINE` (`id`, `code`, `fullName`, `creationDate`, `lastModificationDate`, `lastFlightCode`) VALUES (35, 'R4', 'Russia Airline', now(), now(), 0);
INSERT INTO `flyingfar`.`AIRLINE` (`id`, `code`, `fullName`, `creationDate`, `lastModificationDate`, `lastFlightCode`) VALUES (36, 'RG', 'Varig', now(), now(), 0);
INSERT INTO `flyingfar`.`AIRLINE` (`id`, `code`, `fullName`, `creationDate`, `lastModificationDate`, `lastFlightCode`) VALUES (37, 'RQ', 'Kam Air', now(), now(), 0);
INSERT INTO `flyingfar`.`AIRLINE` (`id`, `code`, `fullName`, `creationDate`, `lastModificationDate`, `lastFlightCode`) VALUES (38, 'S7', 'Siberia Airlines', now(), now(), 0);
INSERT INTO `flyingfar`.`AIRLINE` (`id`, `code`, `fullName`, `creationDate`, `lastModificationDate`, `lastFlightCode`) VALUES (39, 'SR', 'Swissair', now(), now(), 0);
INSERT INTO `flyingfar`.`AIRLINE` (`id`, `code`, `fullName`, `creationDate`, `lastModificationDate`, `lastFlightCode`) VALUES (40, 'TP', 'Tap Air Portugal', now(), now(), 0);
INSERT INTO `flyingfar`.`AIRLINE` (`id`, `code`, `fullName`, `creationDate`, `lastModificationDate`, `lastFlightCode`) VALUES (41, 'US', 'US Airways', now(), now(), 0);
INSERT INTO `flyingfar`.`AIRLINE` (`id`, `code`, `fullName`, `creationDate`, `lastModificationDate`, `lastFlightCode`) VALUES (42, 'YW', 'Air Nostrum', now(), now(), 0);

COMMIT;


-- -----------------------------------------------------
-- Data for table `flyingfar`.`COUNTRY`
-- -----------------------------------------------------
START TRANSACTION;
USE `flyingfar`;
INSERT INTO `flyingfar`.`COUNTRY` (`id`, `code`, `name`) VALUES (1, 'ES', 'España');
INSERT INTO `flyingfar`.`COUNTRY` (`id`, `code`, `name`) VALUES (2, 'FR', 'Francia');

COMMIT;


-- -----------------------------------------------------
-- Data for table `flyingfar`.`CITY`
-- -----------------------------------------------------
START TRANSACTION;
USE `flyingfar`;
INSERT INTO `flyingfar`.`CITY` (`id`, `code`, `name`, `country`) VALUES (1, 'FUE', 'Fuerteventura', 1);
INSERT INTO `flyingfar`.`CITY` (`id`, `code`, `name`, `country`) VALUES (2, 'ALC', 'Alicante', 1);
INSERT INTO `flyingfar`.`CITY` (`id`, `code`, `name`, `country`) VALUES (3, 'ALM', 'Almeria', 1);
INSERT INTO `flyingfar`.`CITY` (`id`, `code`, `name`, `country`) VALUES (4, 'OVD', 'Oviedo', 1);
INSERT INTO `flyingfar`.`CITY` (`id`, `code`, `name`, `country`) VALUES (5, 'BJZ', 'Badajoz', 1);
INSERT INTO `flyingfar`.`CITY` (`id`, `code`, `name`, `country`) VALUES (6, 'BCN', 'Barcelona', 1);
INSERT INTO `flyingfar`.`CITY` (`id`, `code`, `name`, `country`) VALUES (7, 'BIO', 'Bilbao', 1);
INSERT INTO `flyingfar`.`CITY` (`id`, `code`, `name`, `country`) VALUES (8, 'CEU', 'Ceuta', 1);
INSERT INTO `flyingfar`.`CITY` (`id`, `code`, `name`, `country`) VALUES (9, 'COR', 'Cordoba', 1);
INSERT INTO `flyingfar`.`CITY` (`id`, `code`, `name`, `country`) VALUES (10, 'GER', 'Gerona', 1);
INSERT INTO `flyingfar`.`CITY` (`id`, `code`, `name`, `country`) VALUES (11, 'GRA', 'Granada', 1);
INSERT INTO `flyingfar`.`CITY` (`id`, `code`, `name`, `country`) VALUES (12, 'IBZ', 'Ibiza', 1);
INSERT INTO `flyingfar`.`CITY` (`id`, `code`, `name`, `country`) VALUES (13, 'JER', 'Jerez De La Frontera', 1);
INSERT INTO `flyingfar`.`CITY` (`id`, `code`, `name`, `country`) VALUES (14, 'LCG', 'La Coruña', 1);
INSERT INTO `flyingfar`.`CITY` (`id`, `code`, `name`, `country`) VALUES (15, 'LAN', 'Lanzarote', 1);
INSERT INTO `flyingfar`.`CITY` (`id`, `code`, `name`, `country`) VALUES (16, 'LPA', 'Las Palmas', 1);
INSERT INTO `flyingfar`.`CITY` (`id`, `code`, `name`, `country`) VALUES (17, 'MAD', 'Madrid', 1);
INSERT INTO `flyingfar`.`CITY` (`id`, `code`, `name`, `country`) VALUES (18, 'MA', 'Malaga', 1);
INSERT INTO `flyingfar`.`CITY` (`id`, `code`, `name`, `country`) VALUES (19, 'MEL', 'Melilla', 1);
INSERT INTO `flyingfar`.`CITY` (`id`, `code`, `name`, `country`) VALUES (20, 'MEN', 'Menorca', 1);
INSERT INTO `flyingfar`.`CITY` (`id`, `code`, `name`, `country`) VALUES (21, 'OZP', 'Moron', 1);
INSERT INTO `flyingfar`.`CITY` (`id`, `code`, `name`, `country`) VALUES (22, 'MUR', 'Murcia', 1);
INSERT INTO `flyingfar`.`CITY` (`id`, `code`, `name`, `country`) VALUES (23, 'MALL', 'Mallorca', 1);
INSERT INTO `flyingfar`.`CITY` (`id`, `code`, `name`, `country`) VALUES (24, 'PAM', 'Pamplona', 1);
INSERT INTO `flyingfar`.`CITY` (`id`, `code`, `name`, `country`) VALUES (25, 'REU', 'Reus', 1);
INSERT INTO `flyingfar`.`CITY` (`id`, `code`, `name`, `country`) VALUES (26, 'SAL', 'Salamanca', 1);
INSERT INTO `flyingfar`.`CITY` (`id`, `code`, `name`, `country`) VALUES (27, 'SS', 'San Sebastian', 1);
INSERT INTO `flyingfar`.`CITY` (`id`, `code`, `name`, `country`) VALUES (28, 'SPC', 'Santa Cruz de La Palma', 1);
INSERT INTO `flyingfar`.`CITY` (`id`, `code`, `name`, `country`) VALUES (29, 'SAN', 'Santander', 1);
INSERT INTO `flyingfar`.`CITY` (`id`, `code`, `name`, `country`) VALUES (30, 'COM', 'Santiago de Compostela', 1);
INSERT INTO `flyingfar`.`CITY` (`id`, `code`, `name`, `country`) VALUES (31, 'LEU', 'Seo De Urgel ', 1);
INSERT INTO `flyingfar`.`CITY` (`id`, `code`, `name`, `country`) VALUES (32, 'SEV', 'Sevilla', 1);
INSERT INTO `flyingfar`.`CITY` (`id`, `code`, `name`, `country`) VALUES (33, 'TNF', 'Tenerife', 1);
INSERT INTO `flyingfar`.`CITY` (`id`, `code`, `name`, `country`) VALUES (35, 'VAL', 'Valencia', 1);
INSERT INTO `flyingfar`.`CITY` (`id`, `code`, `name`, `country`) VALUES (36, 'VLL', 'Valladolid', 1);
INSERT INTO `flyingfar`.`CITY` (`id`, `code`, `name`, `country`) VALUES (37, 'VDE', 'Valverde', 1);
INSERT INTO `flyingfar`.`CITY` (`id`, `code`, `name`, `country`) VALUES (38, 'VGO', 'Vigo', 1);
INSERT INTO `flyingfar`.`CITY` (`id`, `code`, `name`, `country`) VALUES (39, 'VIT', 'Vitoria', 1);
INSERT INTO `flyingfar`.`CITY` (`id`, `code`, `name`, `country`) VALUES (40, 'ZAR', 'Zaragoza', 1);
INSERT INTO `flyingfar`.`CITY` (`id`, `code`, `name`, `country`) VALUES (34, 'TFS', 'Tenerife', 1);

COMMIT;


-- -----------------------------------------------------
-- Data for table `flyingfar`.`AIRPORT`
-- -----------------------------------------------------
START TRANSACTION;
USE `flyingfar`;
INSERT INTO `flyingfar`.`AIRPORT` (`id`, `code`, `name`, `city`, `creationDate`, `lastModificationDate`) VALUES (1, 'FUE', 'Fuerteventura', 1, now(), now());
INSERT INTO `flyingfar`.`AIRPORT` (`id`, `code`, `name`, `city`, `creationDate`, `lastModificationDate`) VALUES (2, 'ALC', 'Alicante', 2, now(), now());
INSERT INTO `flyingfar`.`AIRPORT` (`id`, `code`, `name`, `city`, `creationDate`, `lastModificationDate`) VALUES (3, 'LEI', 'Almeria', 3, now(), now());
INSERT INTO `flyingfar`.`AIRPORT` (`id`, `code`, `name`, `city`, `creationDate`, `lastModificationDate`) VALUES (4, 'OVD', 'Oviedo', 4, now(), now());
INSERT INTO `flyingfar`.`AIRPORT` (`id`, `code`, `name`, `city`, `creationDate`, `lastModificationDate`) VALUES (5, 'BJZ', 'Talaveral La Real Airport', 5, now(), now());
INSERT INTO `flyingfar`.`AIRPORT` (`id`, `code`, `name`, `city`, `creationDate`, `lastModificationDate`) VALUES (6, 'BCN', 'Barcelona', 6, now(), now());
INSERT INTO `flyingfar`.`AIRPORT` (`id`, `code`, `name`, `city`, `creationDate`, `lastModificationDate`) VALUES (7, 'BIO', 'Bilbao', 7, now(), now());
INSERT INTO `flyingfar`.`AIRPORT` (`id`, `code`, `name`, `city`, `creationDate`, `lastModificationDate`) VALUES (8, 'JCU', 'Ceuta Heliport', 8, now(), now());
INSERT INTO `flyingfar`.`AIRPORT` (`id`, `code`, `name`, `city`, `creationDate`, `lastModificationDate`) VALUES (9, 'ODB', 'Cordoba Airport', 9, now(), now());
INSERT INTO `flyingfar`.`AIRPORT` (`id`, `code`, `name`, `city`, `creationDate`, `lastModificationDate`) VALUES (10, 'GRO', 'Gerona', 10, now(), now());
INSERT INTO `flyingfar`.`AIRPORT` (`id`, `code`, `name`, `city`, `creationDate`, `lastModificationDate`) VALUES (11, 'GRX', 'Granada Airport', 11, now(), now());
INSERT INTO `flyingfar`.`AIRPORT` (`id`, `code`, `name`, `city`, `creationDate`, `lastModificationDate`) VALUES (12, 'IBZ', 'Ibiza', 12, now(), now());
INSERT INTO `flyingfar`.`AIRPORT` (`id`, `code`, `name`, `city`, `creationDate`, `lastModificationDate`) VALUES (13, 'XRY', 'Jerez', 13, now(), now());
INSERT INTO `flyingfar`.`AIRPORT` (`id`, `code`, `name`, `city`, `creationDate`, `lastModificationDate`) VALUES (14, 'LCG', 'La Coruna Airport', 14, now(), now());
INSERT INTO `flyingfar`.`AIRPORT` (`id`, `code`, `name`, `city`, `creationDate`, `lastModificationDate`) VALUES (15, 'ACE', 'Lanzarote', 15, now(), now());
INSERT INTO `flyingfar`.`AIRPORT` (`id`, `code`, `name`, `city`, `creationDate`, `lastModificationDate`) VALUES (16, 'LPA', 'Las Palmas', 16, now(), now());
INSERT INTO `flyingfar`.`AIRPORT` (`id`, `code`, `name`, `city`, `creationDate`, `lastModificationDate`) VALUES (17, 'MAD', 'Madrid', 17, now(), now());
INSERT INTO `flyingfar`.`AIRPORT` (`id`, `code`, `name`, `city`, `creationDate`, `lastModificationDate`) VALUES (18, 'AGP', 'Malaga', 18, now(), now());
INSERT INTO `flyingfar`.`AIRPORT` (`id`, `code`, `name`, `city`, `creationDate`, `lastModificationDate`) VALUES (19, 'MLN', 'Melilla Airport', 19, now(), now());
INSERT INTO `flyingfar`.`AIRPORT` (`id`, `code`, `name`, `city`, `creationDate`, `lastModificationDate`) VALUES (20, 'MAH', 'Menorca', 20, now(), now());
INSERT INTO `flyingfar`.`AIRPORT` (`id`, `code`, `name`, `city`, `creationDate`, `lastModificationDate`) VALUES (21, 'OZP', 'Moron Airport', 21, now(), now());
INSERT INTO `flyingfar`.`AIRPORT` (`id`, `code`, `name`, `city`, `creationDate`, `lastModificationDate`) VALUES (22, 'MJV', 'Murcia San Javier', 22, now(), now());
INSERT INTO `flyingfar`.`AIRPORT` (`id`, `code`, `name`, `city`, `creationDate`, `lastModificationDate`) VALUES (23, 'PMI', 'Palma Mallorca', 23, now(), now());
INSERT INTO `flyingfar`.`AIRPORT` (`id`, `code`, `name`, `city`, `creationDate`, `lastModificationDate`) VALUES (24, 'PNA', 'Pamplona Airport', 24, now(), now());
INSERT INTO `flyingfar`.`AIRPORT` (`id`, `code`, `name`, `city`, `creationDate`, `lastModificationDate`) VALUES (25, 'REU', 'Reus', 25, now(), now());
INSERT INTO `flyingfar`.`AIRPORT` (`id`, `code`, `name`, `city`, `creationDate`, `lastModificationDate`) VALUES (26, 'SLM', 'Matacan Airport', 26, now(), now());
INSERT INTO `flyingfar`.`AIRPORT` (`id`, `code`, `name`, `city`, `creationDate`, `lastModificationDate`) VALUES (27, 'EAS', 'San Sebastian Airport', 27, now(), now());
INSERT INTO `flyingfar`.`AIRPORT` (`id`, `code`, `name`, `city`, `creationDate`, `lastModificationDate`) VALUES (28, 'SPC', 'Santa Cruz La Palma', 28, now(), now());
INSERT INTO `flyingfar`.`AIRPORT` (`id`, `code`, `name`, `city`, `creationDate`, `lastModificationDate`) VALUES (29, 'SDR', 'Santander Airport', 29, now(), now());
INSERT INTO `flyingfar`.`AIRPORT` (`id`, `code`, `name`, `city`, `creationDate`, `lastModificationDate`) VALUES (30, 'SCQ', 'Santiago de Compostela', 30, now(), now());
INSERT INTO `flyingfar`.`AIRPORT` (`id`, `code`, `name`, `city`, `creationDate`, `lastModificationDate`) VALUES (31, 'LEU', 'Aeroport De La Seu', 31, now(), now());
INSERT INTO `flyingfar`.`AIRPORT` (`id`, `code`, `name`, `city`, `creationDate`, `lastModificationDate`) VALUES (32, 'SVQ', 'Seville', 32, now(), now());
INSERT INTO `flyingfar`.`AIRPORT` (`id`, `code`, `name`, `city`, `creationDate`, `lastModificationDate`) VALUES (33, 'TFN', 'Los Rodeos Airport', 33, now(), now());
INSERT INTO `flyingfar`.`AIRPORT` (`id`, `code`, `name`, `city`, `creationDate`, `lastModificationDate`) VALUES (34, 'TFS', 'Tenerife', 34, now(), now());
INSERT INTO `flyingfar`.`AIRPORT` (`id`, `code`, `name`, `city`, `creationDate`, `lastModificationDate`) VALUES (35, 'VLC', 'Valencia', 35, now(), now());
INSERT INTO `flyingfar`.`AIRPORT` (`id`, `code`, `name`, `city`, `creationDate`, `lastModificationDate`) VALUES (36, 'VLL', 'Valladolid Airport', 36, now(), now());
INSERT INTO `flyingfar`.`AIRPORT` (`id`, `code`, `name`, `city`, `creationDate`, `lastModificationDate`) VALUES (37, 'VDE', 'Hierro Airport', 37, now(), now());
INSERT INTO `flyingfar`.`AIRPORT` (`id`, `code`, `name`, `city`, `creationDate`, `lastModificationDate`) VALUES (38, 'VGO', 'Vigo Airport', 38, now(), now());
INSERT INTO `flyingfar`.`AIRPORT` (`id`, `code`, `name`, `city`, `creationDate`, `lastModificationDate`) VALUES (39, 'VIT', 'Vitoria Airport', 39, now(), now());
INSERT INTO `flyingfar`.`AIRPORT` (`id`, `code`, `name`, `city`, `creationDate`, `lastModificationDate`) VALUES (40, 'ZAZ', 'Zaragoza Airport', 40, now(), now());

COMMIT;


-- -----------------------------------------------------
-- Data for table `flyingfar`.`PLANE`
-- -----------------------------------------------------
START TRANSACTION;
USE `flyingfar`;
INSERT INTO `flyingfar`.`PLANE` (`id`, `manufacturer`, `model`, `totalSeats`, `manufactureDate`, `maxBusinessSeats`, `maxTouristSeats`, `maxOfferSeats`, `airline`, `creationDate`, `lastModificationDate`) VALUES (1, 'Boeing', 'Airbus 340', 10, '2014-11-27 21:41:45', 5, 3, 2, 1, '2014-11-27 21:41:45', '2014-11-27 21:41:45');
INSERT INTO `flyingfar`.`PLANE` (`id`, `manufacturer`, `model`, `totalSeats`, `manufactureDate`, `maxBusinessSeats`, `maxTouristSeats`, `maxOfferSeats`, `airline`, `creationDate`, `lastModificationDate`) VALUES (2, 'Boeing', 'Airbus 100', 3, '2014-11-27 21:41:45', 1, 1, 1, 1, '2014-11-27 21:41:45', '2014-11-27 21:41:45');
INSERT INTO `flyingfar`.`PLANE` (`id`, `manufacturer`, `model`, `totalSeats`, `manufactureDate`, `maxBusinessSeats`, `maxTouristSeats`, `maxOfferSeats`, `airline`, `creationDate`, `lastModificationDate`) VALUES (4, 'Planz', 'DHZ', 90, '2002-04-08 00:00:00', 45, 35, 10, 2, '2014-11-27 22:51:32', '2014-11-27 22:51:32');

COMMIT;


-- -----------------------------------------------------
-- Data for table `flyingfar`.`ROLE`
-- -----------------------------------------------------
START TRANSACTION;
USE `flyingfar`;
INSERT INTO `flyingfar`.`ROLE` (`id`, `name`, `label`, `description`) VALUES (NULL, 'ROLE_SYSTEM_ADMIN', 'Admin', 'Manage application');
INSERT INTO `flyingfar`.`ROLE` (`id`, `name`, `label`, `description`) VALUES (NULL, 'ROLE_AIRLINE_ADMIN', 'Airline Admin', 'Generate flights');
INSERT INTO `flyingfar`.`ROLE` (`id`, `name`, `label`, `description`) VALUES (NULL, 'ROLE_CLIENT', 'Client', 'Reserve and buy flights');

COMMIT;


-- -----------------------------------------------------
-- Data for table `flyingfar`.`USER`
-- -----------------------------------------------------
START TRANSACTION;
USE `flyingfar`;
INSERT INTO `flyingfar`.`USER` (`id`, `email`, `username`, `password`, `role`, `airline`, `name`, `firstSurname`, `secondSurname`, `address`, `creditCard`, `lastModificationDate`, `creationDate`) VALUES (1, 'rausanchezc@gmail.com', 'raul.sanchez', '1616ad72a74aff17e621825e63af746a3602d81ca583275889df8201cf97317b9571b985c4a9d7e092c61e2cc4e79dd840581144a1d8e627d34373227344aace', 1, NULL, 'Raúl ', 'Sánchez', 'Castillo', 'C/ Francisco Silvela', 00000000, now(), now());
INSERT INTO `flyingfar`.`USER` (`id`, `email`, `username`, `password`, `role`, `airline`, `name`, `firstSurname`, `secondSurname`, `address`, `creditCard`, `lastModificationDate`, `creationDate`) VALUES (2, 'raul.sanchez@correo.es', 'raul_airline', '1616ad72a74aff17e621825e63af746a3602d81ca583275889df8201cf97317b9571b985c4a9d7e092c61e2cc4e79dd840581144a1d8e627d34373227344aace', 2, 1, 'Raúl ', 'Sánchez', 'Castillo', 'C/ Francisco Silvela', 00000000, now(), now());
INSERT INTO `flyingfar`.`USER` (`id`, `email`, `username`, `password`, `role`, `airline`, `name`, `firstSurname`, `secondSurname`, `address`, `creditCard`, `lastModificationDate`, `creationDate`) VALUES (3, 'marta.bazan@correo.es', 'marta_airline', '1616ad72a74aff17e621825e63af746a3602d81ca583275889df8201cf97317b9571b985c4a9d7e092c61e2cc4e79dd840581144a1d8e627d34373227344aace', 2, 2, 'Marta', 'Bazán', 'Quijada', 'C/ Rafael Alberti', 00000000, now(), now());
INSERT INTO `flyingfar`.`USER` (`id`, `email`, `username`, `password`, `role`, `airline`, `name`, `firstSurname`, `secondSurname`, `address`, `creditCard`, `lastModificationDate`, `creationDate`) VALUES (4, 'raul@correo.es', 'raul', '1616ad72a74aff17e621825e63af746a3602d81ca583275889df8201cf97317b9571b985c4a9d7e092c61e2cc4e79dd840581144a1d8e627d34373227344aace', 3, NULL, 'Raúl', 'Sánchez', 'Castillo', 'C/A', 00000000, now(), now());

COMMIT;


-- -----------------------------------------------------
-- Data for table `flyingfar`.`FLIGHT`
-- -----------------------------------------------------
START TRANSACTION;
USE `flyingfar`;
INSERT INTO `flyingfar`.`FLIGHT` (`id`, `code`, `departure`, `arrival`, `origin`, `destination`, `plane`, `creationDate`, `lastModificationDAte`, `logicDeletion`, `roundTrip`, `amount`, `airline`) VALUES (1, 'AA1', '2014-11-30 17:25:00', '2014-11-30 20:50:00', 5, 15, 1, '2014-11-27 22:15:34', '2014-11-27 22:28:47', NULL, 0, NULL, 1);
INSERT INTO `flyingfar`.`FLIGHT` (`id`, `code`, `departure`, `arrival`, `origin`, `destination`, `plane`, `creationDate`, `lastModificationDAte`, `logicDeletion`, `roundTrip`, `amount`, `airline`) VALUES (2, 'AA2', '2014-12-04 17:15:00', '2014-12-04 20:50:00', 7, 11, 2, '2014-11-27 22:19:03', '2014-11-27 22:19:03', NULL, 0, NULL, 1);
INSERT INTO `flyingfar`.`FLIGHT` (`id`, `code`, `departure`, `arrival`, `origin`, `destination`, `plane`, `creationDate`, `lastModificationDAte`, `logicDeletion`, `roundTrip`, `amount`, `airline`) VALUES (3, 'AA3', '2014-12-04 23:05:00', '2015-01-05 04:20:00', 7, 11, 2, '2014-11-27 22:25:37', '2014-11-27 22:25:37', NULL, 0, NULL, 1);
INSERT INTO `flyingfar`.`FLIGHT` (`id`, `code`, `departure`, `arrival`, `origin`, `destination`, `plane`, `creationDate`, `lastModificationDAte`, `logicDeletion`, `roundTrip`, `amount`, `airline`) VALUES (4, 'AA4', '2014-12-08 09:25:00', '2014-12-24 15:10:00', 9, 8, 1, '2014-11-27 22:30:09', '2014-11-27 22:30:09', 1, 0, NULL, 1);
INSERT INTO `flyingfar`.`FLIGHT` (`id`, `code`, `departure`, `arrival`, `origin`, `destination`, `plane`, `creationDate`, `lastModificationDAte`, `logicDeletion`, `roundTrip`, `amount`, `airline`) VALUES (5, 'AC1', '2014-12-24 09:25:00', '2014-12-24 10:35:00', 17, 18, 4, '2014-11-27 22:53:06', '2014-11-27 22:53:06', NULL, 0, NULL, 2);
INSERT INTO `flyingfar`.`FLIGHT` (`id`, `code`, `departure`, `arrival`, `origin`, `destination`, `plane`, `creationDate`, `lastModificationDAte`, `logicDeletion`, `roundTrip`, `amount`, `airline`) VALUES (6, 'AC2', '2014-11-30 22:25:00', '2014-12-01 02:55:00', 5, 15, 4, '2014-11-27 22:54:36', '2014-11-27 22:54:36', NULL, 0, NULL, 2);

COMMIT;


-- -----------------------------------------------------
-- Data for table `flyingfar`.`PATH`
-- -----------------------------------------------------
START TRANSACTION;
USE `flyingfar`;
INSERT INTO `flyingfar`.`PATH` (`id`, `position`, `flight`, `origin`, `destination`, `slot`, `duration`) VALUES (1, 1, 1, 5, 15, '2014-11-30 17:30:00', 45);
INSERT INTO `flyingfar`.`PATH` (`id`, `position`, `flight`, `origin`, `destination`, `slot`, `duration`) VALUES (3, 1, 2, 7, 11, '2014-12-04 17:20:00', 210);
INSERT INTO `flyingfar`.`PATH` (`id`, `position`, `flight`, `origin`, `destination`, `slot`, `duration`) VALUES (4, 1, 3, 7, 6, '2014-12-04 23:10:00', 40);
INSERT INTO `flyingfar`.`PATH` (`id`, `position`, `flight`, `origin`, `destination`, `slot`, `duration`) VALUES (5, 2, 3, 6, 17, '2014-12-04 23:55:00', 120);
INSERT INTO `flyingfar`.`PATH` (`id`, `position`, `flight`, `origin`, `destination`, `slot`, `duration`) VALUES (6, 3, 3, 17, 11, '2015-01-05 02:05:00', 135);
INSERT INTO `flyingfar`.`PATH` (`id`, `position`, `flight`, `origin`, `destination`, `slot`, `duration`) VALUES (7, 1, 4, 9, 8, '2014-12-24 14:25:00', 45);
INSERT INTO `flyingfar`.`PATH` (`id`, `position`, `flight`, `origin`, `destination`, `slot`, `duration`) VALUES (8, 1, 5, 17, 18, '2014-12-24 09:30:00', 65);
INSERT INTO `flyingfar`.`PATH` (`id`, `position`, `flight`, `origin`, `destination`, `slot`, `duration`) VALUES (9, 1, 6, 5, 15, '2014-11-30 22:35:00', 260);

COMMIT;


-- -----------------------------------------------------
-- Data for table `flyingfar`.`SEAT_TYPE`
-- -----------------------------------------------------
START TRANSACTION;
USE `flyingfar`;
INSERT INTO `flyingfar`.`SEAT_TYPE` (`id`, `category`) VALUES (1, 'BUSINESS');
INSERT INTO `flyingfar`.`SEAT_TYPE` (`id`, `category`) VALUES (2, 'TOURIST');
INSERT INTO `flyingfar`.`SEAT_TYPE` (`id`, `category`) VALUES (3, 'OFFER');
INSERT INTO `flyingfar`.`SEAT_TYPE` (`id`, `category`) VALUES (4, 'LAST_MINUTE');

COMMIT;


-- -----------------------------------------------------
-- Data for table `flyingfar`.`SEAT`
-- -----------------------------------------------------
START TRANSACTION;
USE `flyingfar`;
INSERT INTO `flyingfar`.`SEAT` (`id`, `plane`, `seatType`, `seatNumber`) VALUES (1, 1, 1, 1);
INSERT INTO `flyingfar`.`SEAT` (`id`, `plane`, `seatType`, `seatNumber`) VALUES (2, 1, 1, 2);
INSERT INTO `flyingfar`.`SEAT` (`id`, `plane`, `seatType`, `seatNumber`) VALUES (3, 1, 1, 3);
INSERT INTO `flyingfar`.`SEAT` (`id`, `plane`, `seatType`, `seatNumber`) VALUES (4, 1, 1, 4);
INSERT INTO `flyingfar`.`SEAT` (`id`, `plane`, `seatType`, `seatNumber`) VALUES (5, 1, 1, 5);
INSERT INTO `flyingfar`.`SEAT` (`id`, `plane`, `seatType`, `seatNumber`) VALUES (6, 1, 2, 6);
INSERT INTO `flyingfar`.`SEAT` (`id`, `plane`, `seatType`, `seatNumber`) VALUES (7, 1, 2, 7);
INSERT INTO `flyingfar`.`SEAT` (`id`, `plane`, `seatType`, `seatNumber`) VALUES (8, 1, 2, 8);
INSERT INTO `flyingfar`.`SEAT` (`id`, `plane`, `seatType`, `seatNumber`) VALUES (9, 1, 3, 9);
INSERT INTO `flyingfar`.`SEAT` (`id`, `plane`, `seatType`, `seatNumber`) VALUES (10, 1, 3, 10);
INSERT INTO `flyingfar`.`SEAT` (`id`, `plane`, `seatType`, `seatNumber`) VALUES (11, 2, 1, 1);
INSERT INTO `flyingfar`.`SEAT` (`id`, `plane`, `seatType`, `seatNumber`) VALUES (12, 2, 2, 2);
INSERT INTO `flyingfar`.`SEAT` (`id`, `plane`, `seatType`, `seatNumber`) VALUES (13, 2, 3, 3);
INSERT INTO `flyingfar`.`SEAT` (`id`, `plane`, `seatType`, `seatNumber`) VALUES (44, 4, 1, 1);
INSERT INTO `flyingfar`.`SEAT` (`id`, `plane`, `seatType`, `seatNumber`) VALUES (45, 4, 1, 2);
INSERT INTO `flyingfar`.`SEAT` (`id`, `plane`, `seatType`, `seatNumber`) VALUES (46, 4, 1, 3);
INSERT INTO `flyingfar`.`SEAT` (`id`, `plane`, `seatType`, `seatNumber`) VALUES (47, 4, 1, 4);
INSERT INTO `flyingfar`.`SEAT` (`id`, `plane`, `seatType`, `seatNumber`) VALUES (48, 4, 1, 5);
INSERT INTO `flyingfar`.`SEAT` (`id`, `plane`, `seatType`, `seatNumber`) VALUES (49, 4, 1, 6);
INSERT INTO `flyingfar`.`SEAT` (`id`, `plane`, `seatType`, `seatNumber`) VALUES (50, 4, 1, 7);
INSERT INTO `flyingfar`.`SEAT` (`id`, `plane`, `seatType`, `seatNumber`) VALUES (51, 4, 1, 8);
INSERT INTO `flyingfar`.`SEAT` (`id`, `plane`, `seatType`, `seatNumber`) VALUES (52, 4, 1, 9);
INSERT INTO `flyingfar`.`SEAT` (`id`, `plane`, `seatType`, `seatNumber`) VALUES (53, 4, 1, 10);
INSERT INTO `flyingfar`.`SEAT` (`id`, `plane`, `seatType`, `seatNumber`) VALUES (54, 4, 1, 11);
INSERT INTO `flyingfar`.`SEAT` (`id`, `plane`, `seatType`, `seatNumber`) VALUES (55, 4, 1, 12);
INSERT INTO `flyingfar`.`SEAT` (`id`, `plane`, `seatType`, `seatNumber`) VALUES (56, 4, 1, 13);
INSERT INTO `flyingfar`.`SEAT` (`id`, `plane`, `seatType`, `seatNumber`) VALUES (57, 4, 1, 14);
INSERT INTO `flyingfar`.`SEAT` (`id`, `plane`, `seatType`, `seatNumber`) VALUES (58, 4, 1, 15);
INSERT INTO `flyingfar`.`SEAT` (`id`, `plane`, `seatType`, `seatNumber`) VALUES (59, 4, 1, 16);
INSERT INTO `flyingfar`.`SEAT` (`id`, `plane`, `seatType`, `seatNumber`) VALUES (60, 4, 1, 17);
INSERT INTO `flyingfar`.`SEAT` (`id`, `plane`, `seatType`, `seatNumber`) VALUES (61, 4, 1, 18);
INSERT INTO `flyingfar`.`SEAT` (`id`, `plane`, `seatType`, `seatNumber`) VALUES (62, 4, 1, 19);
INSERT INTO `flyingfar`.`SEAT` (`id`, `plane`, `seatType`, `seatNumber`) VALUES (63, 4, 1, 20);
INSERT INTO `flyingfar`.`SEAT` (`id`, `plane`, `seatType`, `seatNumber`) VALUES (64, 4, 1, 21);
INSERT INTO `flyingfar`.`SEAT` (`id`, `plane`, `seatType`, `seatNumber`) VALUES (65, 4, 1, 22);
INSERT INTO `flyingfar`.`SEAT` (`id`, `plane`, `seatType`, `seatNumber`) VALUES (66, 4, 1, 23);
INSERT INTO `flyingfar`.`SEAT` (`id`, `plane`, `seatType`, `seatNumber`) VALUES (67, 4, 1, 24);
INSERT INTO `flyingfar`.`SEAT` (`id`, `plane`, `seatType`, `seatNumber`) VALUES (68, 4, 1, 25);
INSERT INTO `flyingfar`.`SEAT` (`id`, `plane`, `seatType`, `seatNumber`) VALUES (69, 4, 1, 26);
INSERT INTO `flyingfar`.`SEAT` (`id`, `plane`, `seatType`, `seatNumber`) VALUES (70, 4, 1, 27);
INSERT INTO `flyingfar`.`SEAT` (`id`, `plane`, `seatType`, `seatNumber`) VALUES (71, 4, 1, 28);
INSERT INTO `flyingfar`.`SEAT` (`id`, `plane`, `seatType`, `seatNumber`) VALUES (72, 4, 1, 29);
INSERT INTO `flyingfar`.`SEAT` (`id`, `plane`, `seatType`, `seatNumber`) VALUES (73, 4, 1, 30);
INSERT INTO `flyingfar`.`SEAT` (`id`, `plane`, `seatType`, `seatNumber`) VALUES (74, 4, 1, 31);
INSERT INTO `flyingfar`.`SEAT` (`id`, `plane`, `seatType`, `seatNumber`) VALUES (75, 4, 1, 32);
INSERT INTO `flyingfar`.`SEAT` (`id`, `plane`, `seatType`, `seatNumber`) VALUES (76, 4, 1, 33);
INSERT INTO `flyingfar`.`SEAT` (`id`, `plane`, `seatType`, `seatNumber`) VALUES (77, 4, 1, 34);
INSERT INTO `flyingfar`.`SEAT` (`id`, `plane`, `seatType`, `seatNumber`) VALUES (78, 4, 1, 35);
INSERT INTO `flyingfar`.`SEAT` (`id`, `plane`, `seatType`, `seatNumber`) VALUES (79, 4, 1, 36);
INSERT INTO `flyingfar`.`SEAT` (`id`, `plane`, `seatType`, `seatNumber`) VALUES (80, 4, 1, 37);
INSERT INTO `flyingfar`.`SEAT` (`id`, `plane`, `seatType`, `seatNumber`) VALUES (81, 4, 1, 38);
INSERT INTO `flyingfar`.`SEAT` (`id`, `plane`, `seatType`, `seatNumber`) VALUES (82, 4, 1, 39);
INSERT INTO `flyingfar`.`SEAT` (`id`, `plane`, `seatType`, `seatNumber`) VALUES (83, 4, 1, 40);
INSERT INTO `flyingfar`.`SEAT` (`id`, `plane`, `seatType`, `seatNumber`) VALUES (84, 4, 1, 41);
INSERT INTO `flyingfar`.`SEAT` (`id`, `plane`, `seatType`, `seatNumber`) VALUES (85, 4, 1, 42);
INSERT INTO `flyingfar`.`SEAT` (`id`, `plane`, `seatType`, `seatNumber`) VALUES (86, 4, 1, 43);
INSERT INTO `flyingfar`.`SEAT` (`id`, `plane`, `seatType`, `seatNumber`) VALUES (87, 4, 1, 44);
INSERT INTO `flyingfar`.`SEAT` (`id`, `plane`, `seatType`, `seatNumber`) VALUES (88, 4, 1, 45);
INSERT INTO `flyingfar`.`SEAT` (`id`, `plane`, `seatType`, `seatNumber`) VALUES (89, 4, 2, 46);
INSERT INTO `flyingfar`.`SEAT` (`id`, `plane`, `seatType`, `seatNumber`) VALUES (90, 4, 2, 47);
INSERT INTO `flyingfar`.`SEAT` (`id`, `plane`, `seatType`, `seatNumber`) VALUES (91, 4, 2, 48);
INSERT INTO `flyingfar`.`SEAT` (`id`, `plane`, `seatType`, `seatNumber`) VALUES (92, 4, 2, 49);
INSERT INTO `flyingfar`.`SEAT` (`id`, `plane`, `seatType`, `seatNumber`) VALUES (93, 4, 2, 50);
INSERT INTO `flyingfar`.`SEAT` (`id`, `plane`, `seatType`, `seatNumber`) VALUES (94, 4, 2, 51);
INSERT INTO `flyingfar`.`SEAT` (`id`, `plane`, `seatType`, `seatNumber`) VALUES (95, 4, 2, 52);
INSERT INTO `flyingfar`.`SEAT` (`id`, `plane`, `seatType`, `seatNumber`) VALUES (96, 4, 2, 53);
INSERT INTO `flyingfar`.`SEAT` (`id`, `plane`, `seatType`, `seatNumber`) VALUES (97, 4, 2, 54);
INSERT INTO `flyingfar`.`SEAT` (`id`, `plane`, `seatType`, `seatNumber`) VALUES (98, 4, 2, 55);
INSERT INTO `flyingfar`.`SEAT` (`id`, `plane`, `seatType`, `seatNumber`) VALUES (99, 4, 2, 56);
INSERT INTO `flyingfar`.`SEAT` (`id`, `plane`, `seatType`, `seatNumber`) VALUES (100, 4, 2, 57);
INSERT INTO `flyingfar`.`SEAT` (`id`, `plane`, `seatType`, `seatNumber`) VALUES (101, 4, 2, 58);
INSERT INTO `flyingfar`.`SEAT` (`id`, `plane`, `seatType`, `seatNumber`) VALUES (102, 4, 2, 59);
INSERT INTO `flyingfar`.`SEAT` (`id`, `plane`, `seatType`, `seatNumber`) VALUES (103, 4, 2, 60);
INSERT INTO `flyingfar`.`SEAT` (`id`, `plane`, `seatType`, `seatNumber`) VALUES (104, 4, 2, 61);
INSERT INTO `flyingfar`.`SEAT` (`id`, `plane`, `seatType`, `seatNumber`) VALUES (105, 4, 2, 62);
INSERT INTO `flyingfar`.`SEAT` (`id`, `plane`, `seatType`, `seatNumber`) VALUES (106, 4, 2, 63);
INSERT INTO `flyingfar`.`SEAT` (`id`, `plane`, `seatType`, `seatNumber`) VALUES (107, 4, 2, 64);
INSERT INTO `flyingfar`.`SEAT` (`id`, `plane`, `seatType`, `seatNumber`) VALUES (108, 4, 2, 65);
INSERT INTO `flyingfar`.`SEAT` (`id`, `plane`, `seatType`, `seatNumber`) VALUES (109, 4, 2, 66);
INSERT INTO `flyingfar`.`SEAT` (`id`, `plane`, `seatType`, `seatNumber`) VALUES (110, 4, 2, 67);
INSERT INTO `flyingfar`.`SEAT` (`id`, `plane`, `seatType`, `seatNumber`) VALUES (111, 4, 2, 68);
INSERT INTO `flyingfar`.`SEAT` (`id`, `plane`, `seatType`, `seatNumber`) VALUES (112, 4, 2, 69);
INSERT INTO `flyingfar`.`SEAT` (`id`, `plane`, `seatType`, `seatNumber`) VALUES (113, 4, 2, 70);
INSERT INTO `flyingfar`.`SEAT` (`id`, `plane`, `seatType`, `seatNumber`) VALUES (114, 4, 2, 71);
INSERT INTO `flyingfar`.`SEAT` (`id`, `plane`, `seatType`, `seatNumber`) VALUES (115, 4, 2, 72);
INSERT INTO `flyingfar`.`SEAT` (`id`, `plane`, `seatType`, `seatNumber`) VALUES (116, 4, 2, 73);
INSERT INTO `flyingfar`.`SEAT` (`id`, `plane`, `seatType`, `seatNumber`) VALUES (117, 4, 2, 74);
INSERT INTO `flyingfar`.`SEAT` (`id`, `plane`, `seatType`, `seatNumber`) VALUES (118, 4, 2, 75);
INSERT INTO `flyingfar`.`SEAT` (`id`, `plane`, `seatType`, `seatNumber`) VALUES (119, 4, 2, 76);
INSERT INTO `flyingfar`.`SEAT` (`id`, `plane`, `seatType`, `seatNumber`) VALUES (120, 4, 2, 77);
INSERT INTO `flyingfar`.`SEAT` (`id`, `plane`, `seatType`, `seatNumber`) VALUES (121, 4, 2, 78);
INSERT INTO `flyingfar`.`SEAT` (`id`, `plane`, `seatType`, `seatNumber`) VALUES (122, 4, 2, 79);
INSERT INTO `flyingfar`.`SEAT` (`id`, `plane`, `seatType`, `seatNumber`) VALUES (123, 4, 2, 80);
INSERT INTO `flyingfar`.`SEAT` (`id`, `plane`, `seatType`, `seatNumber`) VALUES (124, 4, 3, 81);
INSERT INTO `flyingfar`.`SEAT` (`id`, `plane`, `seatType`, `seatNumber`) VALUES (125, 4, 3, 82);
INSERT INTO `flyingfar`.`SEAT` (`id`, `plane`, `seatType`, `seatNumber`) VALUES (126, 4, 3, 83);
INSERT INTO `flyingfar`.`SEAT` (`id`, `plane`, `seatType`, `seatNumber`) VALUES (127, 4, 3, 84);
INSERT INTO `flyingfar`.`SEAT` (`id`, `plane`, `seatType`, `seatNumber`) VALUES (128, 4, 3, 85);
INSERT INTO `flyingfar`.`SEAT` (`id`, `plane`, `seatType`, `seatNumber`) VALUES (129, 4, 3, 86);
INSERT INTO `flyingfar`.`SEAT` (`id`, `plane`, `seatType`, `seatNumber`) VALUES (130, 4, 3, 87);
INSERT INTO `flyingfar`.`SEAT` (`id`, `plane`, `seatType`, `seatNumber`) VALUES (131, 4, 3, 88);
INSERT INTO `flyingfar`.`SEAT` (`id`, `plane`, `seatType`, `seatNumber`) VALUES (132, 4, 3, 89);
INSERT INTO `flyingfar`.`SEAT` (`id`, `plane`, `seatType`, `seatNumber`) VALUES (133, 4, 3, 90);

COMMIT;


-- -----------------------------------------------------
-- Data for table `flyingfar`.`SEAT_TYPE_INTO_FLIGHT`
-- -----------------------------------------------------
START TRANSACTION;
USE `flyingfar`;
INSERT INTO `flyingfar`.`SEAT_TYPE_INTO_FLIGHT` (`seatType`, `flight`, `fare`) VALUES (1, 1, 270);
INSERT INTO `flyingfar`.`SEAT_TYPE_INTO_FLIGHT` (`seatType`, `flight`, `fare`) VALUES (1, 2, 145);
INSERT INTO `flyingfar`.`SEAT_TYPE_INTO_FLIGHT` (`seatType`, `flight`, `fare`) VALUES (1, 3, 115);
INSERT INTO `flyingfar`.`SEAT_TYPE_INTO_FLIGHT` (`seatType`, `flight`, `fare`) VALUES (1, 4, 1);
INSERT INTO `flyingfar`.`SEAT_TYPE_INTO_FLIGHT` (`seatType`, `flight`, `fare`) VALUES (1, 5, 230);
INSERT INTO `flyingfar`.`SEAT_TYPE_INTO_FLIGHT` (`seatType`, `flight`, `fare`) VALUES (1, 6, 253);
INSERT INTO `flyingfar`.`SEAT_TYPE_INTO_FLIGHT` (`seatType`, `flight`, `fare`) VALUES (2, 1, 185);
INSERT INTO `flyingfar`.`SEAT_TYPE_INTO_FLIGHT` (`seatType`, `flight`, `fare`) VALUES (2, 2, 123);
INSERT INTO `flyingfar`.`SEAT_TYPE_INTO_FLIGHT` (`seatType`, `flight`, `fare`) VALUES (2, 3, 89);
INSERT INTO `flyingfar`.`SEAT_TYPE_INTO_FLIGHT` (`seatType`, `flight`, `fare`) VALUES (2, 4, 1);
INSERT INTO `flyingfar`.`SEAT_TYPE_INTO_FLIGHT` (`seatType`, `flight`, `fare`) VALUES (2, 5, 185);
INSERT INTO `flyingfar`.`SEAT_TYPE_INTO_FLIGHT` (`seatType`, `flight`, `fare`) VALUES (2, 6, 187);
INSERT INTO `flyingfar`.`SEAT_TYPE_INTO_FLIGHT` (`seatType`, `flight`, `fare`) VALUES (3, 1, 98);
INSERT INTO `flyingfar`.`SEAT_TYPE_INTO_FLIGHT` (`seatType`, `flight`, `fare`) VALUES (3, 2, 96);
INSERT INTO `flyingfar`.`SEAT_TYPE_INTO_FLIGHT` (`seatType`, `flight`, `fare`) VALUES (3, 3, 56);
INSERT INTO `flyingfar`.`SEAT_TYPE_INTO_FLIGHT` (`seatType`, `flight`, `fare`) VALUES (3, 4, 1);
INSERT INTO `flyingfar`.`SEAT_TYPE_INTO_FLIGHT` (`seatType`, `flight`, `fare`) VALUES (3, 5, 120);
INSERT INTO `flyingfar`.`SEAT_TYPE_INTO_FLIGHT` (`seatType`, `flight`, `fare`) VALUES (3, 6, 163);
INSERT INTO `flyingfar`.`SEAT_TYPE_INTO_FLIGHT` (`seatType`, `flight`, `fare`) VALUES (4, 1, 73);
INSERT INTO `flyingfar`.`SEAT_TYPE_INTO_FLIGHT` (`seatType`, `flight`, `fare`) VALUES (4, 2, 45);
INSERT INTO `flyingfar`.`SEAT_TYPE_INTO_FLIGHT` (`seatType`, `flight`, `fare`) VALUES (4, 3, 15);
INSERT INTO `flyingfar`.`SEAT_TYPE_INTO_FLIGHT` (`seatType`, `flight`, `fare`) VALUES (4, 4, 1);
INSERT INTO `flyingfar`.`SEAT_TYPE_INTO_FLIGHT` (`seatType`, `flight`, `fare`) VALUES (4, 5, 98);
INSERT INTO `flyingfar`.`SEAT_TYPE_INTO_FLIGHT` (`seatType`, `flight`, `fare`) VALUES (4, 6, 140);

COMMIT;

