package com.rsanchez.flyingfar.controller;

import com.rsanchez.flyingfar.domain.Airline;
import com.rsanchez.flyingfar.dto.AirlineDto;
import com.rsanchez.flyingfar.json.serializer.AirlineListSelectSerializer;
import com.rsanchez.flyingfar.json.serializer.AirlineSerializer;
import com.rsanchez.flyingfar.json.serializer.JsonUtil;
import com.rsanchez.flyingfar.service.AirlineService;
import flexjson.JSONSerializer;
import java.util.List;
import javax.persistence.EntityNotFoundException;
import javax.persistence.NoResultException;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 */
@RequestMapping(value = "/airline", produces = "application/json; charset=utf-8")
@Controller("airlineController")
public class AirlineController {

    /**
     * ************ PRIVATE ATTRIBUTES (SERVICES, ETC) *****************
     */
    @Autowired
    private AirlineService airlineService;

    /**
     * ************ PUBLIC METHODS (RESTFUL REQUEST HANDLERS) **********
     * /
     
     * /
     * @param airlineId
     * @return 
     */
    @RequestMapping(value = "/{airlineId}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<String> getById(@PathVariable("airlineId") Integer airlineId) {
        HttpStatus httpStatus;
        String body = "";

        Airline airline = airlineService.getById(airlineId);

        if (airline != null) {
            httpStatus = HttpStatus.OK;
            body = JsonUtil.serialize(airline,new AirlineSerializer());
        } else {
            httpStatus = HttpStatus.NOT_FOUND;
        }

        return new ResponseEntity<String>(body, httpStatus);
    }
    
    
    @RequestMapping(value = "/code/{airlineCode}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<String> getByCode(@PathVariable("airlineCode") String airlineCode)
    {
        HttpStatus httpStatus;
        String body = "";
        
        Airline airline = airlineService.getByCode(airlineCode);
        
        if(airline != null)
        {
            httpStatus = HttpStatus.OK;
            body = JsonUtil.serialize(airline, new AirlineSerializer());
        }
        else
        {
            httpStatus = HttpStatus.NOT_FOUND;
        }
        
        return new ResponseEntity<String>(body, httpStatus);
    }
 
    
    @RequestMapping(value = "/{airlineId}" , method = RequestMethod.DELETE)
    @ResponseBody
    public ResponseEntity<String> delete(@PathVariable("airlineId") Integer airlineId)
    {
        HttpStatus httpStatus;
    
        try
        {
            airlineService.delete(airlineId);
            httpStatus = HttpStatus.NO_CONTENT;
        }
        catch(EntityNotFoundException exception)
        {
            httpStatus = HttpStatus.NOT_FOUND;
        }
        
        return new ResponseEntity<String>(httpStatus);
    }
    
    @RequestMapping( value = "/{airlineId}", method = RequestMethod.PUT)
    @ResponseBody
    public ResponseEntity<String> update(@PathVariable("airlineId") Integer airlineId,
            @Valid @RequestBody AirlineDto airlineDto)
    {
        HttpStatus httpStatus;
        String body;
        
        try
        {
            Airline airline = airlineService.update(airlineId, airlineDto);
            httpStatus = HttpStatus.OK;
            body = JsonUtil.serialize(airline,new AirlineSerializer());
            
        }
        catch(EntityNotFoundException exception)
        {
            httpStatus = HttpStatus.NOT_FOUND;
            body = new JSONSerializer()
                    .include("message")
                    .exclude("*","*.class")
                    .serialize(exception);
        }
        
        return new ResponseEntity<String>(body,httpStatus);
    }
    
    @RequestMapping( method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<String> create(@Valid @RequestBody AirlineDto airlineDto)
    {
        Airline airline = airlineService.create(airlineDto);

        String body = JsonUtil.serialize(airline, new AirlineSerializer());
        
        return new ResponseEntity<String>(body, HttpStatus.CREATED);
    }
    
    @RequestMapping( value = "/getAll", method= RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<String> getAll(){
        
        HttpStatus httpStatus;
        String body;

        try
        {
            List<Airline> airlineList = airlineService.getAll();
            httpStatus = HttpStatus.OK;
            body = JsonUtil.serialize(airlineList, new AirlineListSelectSerializer());
        }
        catch(NoResultException exception)
        {
            httpStatus = HttpStatus.NOT_FOUND;
            body = new JSONSerializer()
                    .include("message")
                    .exclude("*","*.class")
                    .serialize(exception);
        }
        
        return new ResponseEntity<String>(body, httpStatus);
    }
    
    
    @RequestMapping(value = "/getByUserId/{userId}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<String> getByUserId(@PathVariable("userId") Integer userId)
    {
        String body = "";
        HttpStatus httpStatus;
        try
        {
            Airline airline = airlineService.getByUserId(userId);
            
            body = JsonUtil.serialize(airline, new AirlineSerializer());
            httpStatus = HttpStatus.OK;
        
        }
        catch(EntityNotFoundException exception)
        {
            httpStatus = HttpStatus.NOT_FOUND;
            body = new JSONSerializer()
                    .include("message")
                    .exclude("*","*.class")
                    .serialize(exception);
        }
        
        return new ResponseEntity<String>(body, httpStatus);
    }
}
