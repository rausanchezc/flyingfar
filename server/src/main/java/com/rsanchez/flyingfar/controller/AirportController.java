package com.rsanchez.flyingfar.controller;

import com.rsanchez.flyingfar.domain.Airport;
import com.rsanchez.flyingfar.dto.AirportDto;
import com.rsanchez.flyingfar.json.serializer.AirportListSerializer;
import com.rsanchez.flyingfar.json.serializer.AirportSerializer;
import com.rsanchez.flyingfar.json.serializer.JsonUtil;
import com.rsanchez.flyingfar.service.AirportService;
import flexjson.JSONSerializer;
import java.util.List;
import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 */
@RequestMapping(value = "/airport" ,produces = "application/json; charset=utf-8")
@Controller("airportController")
public class AirportController {

    /**
     * ************ PRIVATE ATTRIBUTES (SERVICES, ETC) *****************
     */
    @Autowired
    private AirportService airportService;

    /**
     * ************ PUBLIC METHODS (RESTFUL REQUEST HANDLERS) **********
     * /
     
     * /
     * @param airportId
     * @return 
     */
    @RequestMapping(value = "/{airportId}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<String> getById(@PathVariable("airportId") Integer airportId)
    {
        HttpStatus httpStatus;
        String body = "";

        Airport airport = airportService.getById(airportId);

        if (airport != null) {
            httpStatus = HttpStatus.OK;
            body = JsonUtil.serialize(airport,new AirportSerializer());
        } else {
            httpStatus = HttpStatus.NOT_FOUND;
        }

        return new ResponseEntity<String>(body, httpStatus);
    }
    
    
    @RequestMapping(value = "/code/{airportCode}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<String> getByAirportCode(@PathVariable("airportCode") String airportCode) 
    {
        HttpStatus httpStatus;
        String body = "";

        Airport airport = airportService.getByAirportCode(airportCode);

        if (airport != null) 
        {
            httpStatus = HttpStatus.OK;
            body = JsonUtil.serialize(airport,new AirportSerializer());
        } else 
        {
            httpStatus = HttpStatus.NOT_FOUND;
        }

        return new ResponseEntity<String>(body, httpStatus);
    }
    
    
    @RequestMapping(value = "/{airportId}" , method = RequestMethod.DELETE)
    @ResponseBody
    public ResponseEntity<String> delete(@PathVariable("airportId") Integer airportId)
    {
        HttpStatus httpStatus;
    
        try
        {
            airportService.delete(airportId);
            httpStatus = HttpStatus.NO_CONTENT;
        }
        catch(EntityNotFoundException exception)
        {
            httpStatus = HttpStatus.NOT_FOUND;
        }
        
        return new ResponseEntity<String>(httpStatus);
    }
    
    @RequestMapping( value = "/{airportId}", method = RequestMethod.PUT)
    @ResponseBody
    public ResponseEntity<String> update(@PathVariable("airportId") Integer airportId,
            @Valid @RequestBody AirportDto airportDto)
    {
        HttpStatus httpStatus;
        String body;
        
        try
        {
            Airport airport = airportService.update(airportId, airportDto);
            httpStatus = HttpStatus.OK;
            body = JsonUtil.serialize(airport,new AirportSerializer());
            
        }
        catch(EntityNotFoundException exception)
        {
            httpStatus = HttpStatus.NOT_FOUND;
            body = new JSONSerializer()
                    .include("message")
                    .exclude("*","*.class")
                    .serialize(exception);
        }
        
        return new ResponseEntity<String>(body,httpStatus);
    }
    
    
    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<String> create(@Valid @RequestBody AirportDto airportDto)
    {

        Airport airport = airportService.create(airportDto);
        
        String body = new JSONSerializer()
                .include("id","code","name","city.id","city.name")
                .exclude("*","*.class")
                .serialize(airport);
    
        return new ResponseEntity<String>(body, HttpStatus.CREATED);
    }
    
    @RequestMapping( value = "/getAll", method=RequestMethod.GET)
    public ResponseEntity<String> getAll()
    {
        
        List<Airport> airportList = airportService.getAll();
        
        String body = JsonUtil.serialize(airportList, new AirportListSerializer());
        
        return new ResponseEntity<String>(body,HttpStatus.OK);
    }
}
