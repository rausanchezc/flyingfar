package com.rsanchez.flyingfar.controller;

import com.rsanchez.flyingfar.domain.City;
import com.rsanchez.flyingfar.json.serializer.CityListSerializer;
import com.rsanchez.flyingfar.json.serializer.JsonUtil;
import com.rsanchez.flyingfar.service.CityService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 */
@RequestMapping(value = "/city" , produces = "application/json; charset=utf-8") 
@Controller("cityController")
public class CityController 
{
    /** *************************** PRIVATE ATTRIBUTES (SERVICES, ETC) **************************** */

    @Autowired
    private CityService cityService;
    
    /** ************************ PUBLIC METHODS (RESTFUL REQUEST HANDLERS) ************************ */

    /**
     * @return 
     */
    @RequestMapping( value = "/getAll" , method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<String> getAll()
    {
        
        List<City> cityList= cityService.getAll();
        
        String body = JsonUtil.serialize(cityList, new CityListSerializer());
        
        return new ResponseEntity<String>(body,HttpStatus.OK);
    }
}
