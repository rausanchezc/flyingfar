package com.rsanchez.flyingfar.controller;

import com.rsanchez.flyingfar.domain.Flight;
import com.rsanchez.flyingfar.domain.ReserveFlight;
import com.rsanchez.flyingfar.dto.FlightDto;
import com.rsanchez.flyingfar.dto.FlightReserveDto;
import com.rsanchez.flyingfar.dto.FlightSearchDto;
import com.rsanchez.flyingfar.json.serializer.FlightListByRestrictionsSerializer;
import com.rsanchez.flyingfar.json.serializer.FlightListMyDashboardSerializer;
import com.rsanchez.flyingfar.json.serializer.FlightListSerializer;
import com.rsanchez.flyingfar.json.serializer.FlightSerializer;
import com.rsanchez.flyingfar.json.serializer.FlightWithDetailsSerializer;
import com.rsanchez.flyingfar.json.serializer.JsonUtil;
import com.rsanchez.flyingfar.service.FlightService;
import com.rsanchez.flyingfar.service.ReserveFlightService;
import flexjson.JSONSerializer;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityNotFoundException;
import javax.persistence.NoResultException;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 */
@RequestMapping(value = "/flight", produces = "application/json; charset=utf-8")
@Controller("flightController")
public class FlightController {

     /* ************ PRIVATE ATTRIBUTES (SERVICES, ETC) ***************** */
    
    @Autowired
    private FlightService flightService;
    
    @Autowired
    private ReserveFlightService reserveFlightService;
    
    
    /* ************ PUBLIC METHODS (RESTFUL REQUEST HANDLERS) ********** */
    
    /**
     * @param flightId
     * @return 
     */
    @RequestMapping(value = "/{flightId}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<String> getByIdWithDetails(@PathVariable("flightId") Integer flightId)
    {
        HttpStatus httpStatus;
        String body;
        
        try
        {
            Flight flight = flightService.getByIdWithDetails(flightId);
            httpStatus = HttpStatus.OK;
            body = JsonUtil.serialize(flight, new FlightWithDetailsSerializer());
        }
        catch(NoResultException exception)
        {
            httpStatus = HttpStatus.NOT_FOUND;
            body = new JSONSerializer()
                    .include("message")
                    .exclude("*","*.class")
                    .serialize(exception);
        }
        
        return new ResponseEntity<String>(body, httpStatus);
    }
    
//    @RequestMapping(value = "/code/{airlineCode}", method = RequestMethod.GET)
//    @ResponseBody
//    public ResponseEntity<String> getByCode(@PathVariable("airlineCode") String airlineCode)
//    {
//        HttpStatus httpStatus;
//        String body = "";
//        
//        Airline airline = airlineService.getByCode(airlineCode);
//        
//        if(airline != null)
//        {
//            httpStatus = HttpStatus.OK;
//            body = JsonUtil.serialize(airline, new AirlineSerializer());
//        }
//        else
//        {
//            httpStatus = HttpStatus.NOT_FOUND;
//        }
//        
//        return new ResponseEntity<String>(body, httpStatus);
//    }
 
    
    @RequestMapping(value = "/{flightId}" , method = RequestMethod.DELETE)
    @ResponseBody
    public ResponseEntity<String> delete(@PathVariable("flightId") Integer flightId)
    {
        HttpStatus httpStatus;
    
        try
        {
            flightService.delete(flightId);
            httpStatus = HttpStatus.NO_CONTENT;
        }
        catch(EntityNotFoundException exception)
        {
            httpStatus = HttpStatus.NOT_FOUND;
        }
        
        return new ResponseEntity<String>(httpStatus);
    }
    
    @RequestMapping( value = "/{flightId}", method = RequestMethod.PUT)
    @ResponseBody
    public ResponseEntity<String> update(@PathVariable("flightId") Integer flightId,
            @Valid @RequestBody FlightDto flightDto)
    {
        HttpStatus httpStatus;
        String body;
        
        try
        {
            Flight flight = flightService.update(flightId, flightDto);
            httpStatus = HttpStatus.OK;
            body = JsonUtil.serialize(flight,new FlightSerializer());
            
        }
        catch(EntityNotFoundException exception)
        {
            httpStatus = HttpStatus.NOT_FOUND;
            body = new JSONSerializer()
                    .include("message")
                    .exclude("*","*.class")
                    .serialize(exception);
        }
        
        return new ResponseEntity<String>(body,httpStatus);
    }

    
    /**
     * 
     * @param flightDto
     * @return 
     */
    @RequestMapping( method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<String> create(@Valid @RequestBody FlightDto flightDto)
    {
        Flight flight = flightService.create(flightDto);
        String body = JsonUtil.serialize(flight, new FlightSerializer());
        
        return new ResponseEntity<String>(body, HttpStatus.CREATED);
    }
    
    /**
     * @return 
     */
    @RequestMapping( value = "/getAll", method= RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<String> getAll(){
        
        HttpStatus httpStatus;
        String body;

        try
        {
            List<Flight> flightList = flightService.getAllValid();
            httpStatus = HttpStatus.OK;
            body = JsonUtil.serialize(flightList, new FlightListSerializer());
        }
        catch(NoResultException exception)
        {
            httpStatus = HttpStatus.NOT_FOUND;
            body = new JSONSerializer()
                    .include("message")
                    .exclude("*","*.class")
                    .serialize(exception);
        }
        
        return new ResponseEntity<String>(body, httpStatus);
    }
    
    
    @RequestMapping( value = "/search", method= RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<String> getByRestrictions(FlightSearchDto flightDto){
        
        HttpStatus httpStatus;
        String body;

        try
        {
            List<Flight> flightList = flightService.getByRestrictions(flightDto);
            httpStatus = HttpStatus.OK;
                        
            body = JsonUtil.serialize(flightList, new FlightListByRestrictionsSerializer());
        }
        catch(NoResultException exception)
        {
            httpStatus = HttpStatus.NOT_FOUND;
            body = new JSONSerializer()
                    .include("message")
                    .exclude("*","*.class")
                    .serialize(exception);
        }
        
        return new ResponseEntity<String>(body, httpStatus);
    }
    
    @RequestMapping( value = "/getAllByAirlineId/{airlineId}", method= RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<String> getAllByAirlineId(@PathVariable("airlineId") Integer airlineId)
    {
        
        HttpStatus httpStatus;
        String body;

        try
        {
            List<Flight> flightList = flightService.getByAirlineId(airlineId);
            httpStatus = HttpStatus.OK;
            body = JsonUtil.serialize(flightList, new FlightListSerializer());
        }
        catch(NoResultException exception)
        {
            httpStatus = HttpStatus.NOT_FOUND;
            body = new JSONSerializer()
                    .include("message")
                    .exclude("*","*.class")
                    .serialize(exception);
        }
        
        return new ResponseEntity<String>(body, httpStatus);
    }
     
    @RequestMapping(value = "/reserve" , method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<String> reserveFlight(@RequestBody FlightReserveDto flightReserveDto)
    {
        HttpStatus httpStatus;
        String body = "";
        
        try
        {
            flightService.reserve(flightReserveDto);
            httpStatus = HttpStatus.OK;
        }
        catch(NoResultException exception)
        {
            httpStatus = HttpStatus.NOT_FOUND;
            body = new JSONSerializer()
                    .include("message")
                    .exclude("*","*.class")
                    .serialize(exception);
        }
        
        return new ResponseEntity<String>(body,httpStatus);
    }
    
    @RequestMapping(value = "/countReserves/{userId}" , method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getReservesById(@PathVariable("userId") Integer userId)
    {
        Map<String, Object> response = new HashMap<String, Object>();
        
        Integer count = reserveFlightService.countReserveFlightByUserId(userId);
        response.put("count", count);
        
        return response;
    }
    
    @RequestMapping( value = "/getAllByUserId/{userId}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<String> getAllByUserId(@PathVariable("userId") Integer userId)
    {
        HttpStatus httpStatus;
        String body; 
        
        Map<String, List<ReserveFlight>>  flightMap = flightService.getAllReservesFromUserId(userId);

        if(flightMap != null)
        {
            httpStatus = HttpStatus.OK;
            body = "";
            System.out.println(" ### Map >" + flightMap);
            body = JsonUtil.serialize(flightMap, new FlightListMyDashboardSerializer());
        }
        else
        {
            httpStatus = HttpStatus.NOT_FOUND;
            body = "";
        }
        
        return new ResponseEntity<String>(body,httpStatus);
    }
    
    
    @RequestMapping(value = "/reserve/{reserveFlightId}", method=RequestMethod.DELETE)
    @ResponseBody
    public ResponseEntity<String> deleteReserveFlight(@PathVariable("reserveFlightId") Integer reserveFlightId)
    {
        HttpStatus httpStatus;
        
        try
        {
            reserveFlightService.delete(reserveFlightId);
            httpStatus = HttpStatus.NO_CONTENT;
        }
        catch(EntityNotFoundException exception)
        {
            httpStatus = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<String>(httpStatus);
    }
    
    @RequestMapping(value = "/reserve/{reserveFlightId}", method=RequestMethod.PUT)
    @ResponseBody
    public ResponseEntity<String> payReserveFlight(@PathVariable("reserveFlightId") Integer reserveFlightId)
    {
        HttpStatus httpStatus;
        
        try
        {
            reserveFlightService.pay(reserveFlightId);
            httpStatus = HttpStatus.NO_CONTENT;
        }
        catch(EntityNotFoundException exception)
        {
            httpStatus = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<String>(httpStatus);
    }
    
    @RequestMapping(value = "/reserve/modifySeats/{reserveFlightId}", method=RequestMethod.PUT)
    @ResponseBody
    public ResponseEntity<String> modifySeatReserve(@PathVariable("reserveFlightId") Integer reserveFlightId , 
                                                    @RequestParam("seatList") List<Integer> seatList)
    {
        HttpStatus httpStatus;

        try
        {
            reserveFlightService.modifySeatList(reserveFlightId, seatList);
            httpStatus = HttpStatus.NO_CONTENT;
        }
        catch(EntityNotFoundException exception)
        {
            httpStatus = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<String>(httpStatus);
    }
    
    
    @RequestMapping(value = "/reserve/modify/{reserveFlightId}", method=RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<String> modifySeatReserve(@PathVariable("reserveFlightId") Integer reserveFlightId,
                                                    @RequestBody FlightReserveDto flightReserveDto)
    {
        HttpStatus httpStatus;

        try
        {
            reserveFlightService.replaceByNewReserve(reserveFlightId, flightReserveDto);
            httpStatus = HttpStatus.NO_CONTENT;
        }
        catch(EntityNotFoundException exception)
        {
            httpStatus = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<String>(httpStatus);
    }
    
    @RequestMapping(value = "/reserves/forms/{airlineId}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<String> getAllFlightsOccupedByAirline(@PathVariable("airlineId") Integer airlineId)
    {
       return new ResponseEntity<String>("", HttpStatus.OK);
    }
}
