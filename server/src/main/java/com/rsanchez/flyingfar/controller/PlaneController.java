package com.rsanchez.flyingfar.controller;

import com.rsanchez.flyingfar.domain.Plane;
import com.rsanchez.flyingfar.dto.PlaneDto;
import com.rsanchez.flyingfar.json.serializer.JsonUtil;
import com.rsanchez.flyingfar.json.serializer.PlaneListSerializer;
import com.rsanchez.flyingfar.service.PlaneService;
import flexjson.JSONSerializer;
import java.util.List;
import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;
import javax.ws.rs.PathParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 */
@RequestMapping(value = "/plane", produces = "application/json; charset=utf-8")
@Controller("planeController")
public class PlaneController {

    /**
     * ************ PRIVATE ATTRIBUTES (SERVICES, ETC) *****************
     */
    @Autowired
    private PlaneService planeService;

    /**
     * ************ PUBLIC METHODS (RESTFUL REQUEST HANDLERS) **********
     * /
     
     * /
     * @param planeId
     * @return 
     */
    @RequestMapping(value = "/{planeId}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<String> getById(@PathVariable("planeId") Integer planeId) {
        HttpStatus httpStatus;
        String body = "";

        Plane plane = planeService.getById(planeId);
        
        if(plane != null)
        {
            httpStatus = HttpStatus.OK;
            body =  new JSONSerializer()
                .include("id","manufacturer","model","totalSeats","manufactureDate"
                        ,"maxBusinessSeats","maxTouristSeats","maxOfferSeats")
                .exclude("*", "*.class")
                .serialize(plane);
            
        } else {
            httpStatus = HttpStatus.NOT_FOUND;
        }

        return new ResponseEntity<String>(body, httpStatus);
    }
    
    
    @RequestMapping(value = "/model/{planeModel}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<String> getByCode(@PathVariable("planeModel") String planeModel)
    {
        HttpStatus httpStatus;
        String body = "";

        List<Plane> planeList = planeService.getByModel(planeModel);
        if(planeList != null)
        {
            httpStatus = HttpStatus.OK;
            body = JsonUtil.serialize(planeList, new PlaneListSerializer());
        }
        else
        {
            httpStatus = HttpStatus.NOT_FOUND;
        }
        
        return new ResponseEntity<String>(body, httpStatus);
    }
 
    
    @RequestMapping(value = "/{planeId}" , method = RequestMethod.DELETE)
    @ResponseBody
    public ResponseEntity<String> delete(@PathVariable("planeId") Integer planeId)
    {
        HttpStatus httpStatus;
    
        try
        {
            planeService.delete(planeId);
            httpStatus = HttpStatus.NO_CONTENT;
        }
        catch(EntityNotFoundException exception)
        {
            httpStatus = HttpStatus.NOT_FOUND;
        }
        
        return new ResponseEntity<String>(httpStatus);
    }
    
    @RequestMapping( value = "/{planeId}", method = RequestMethod.PUT)
    @ResponseBody
    public ResponseEntity<String> update(@PathVariable("planeId") Integer planeId,
            @Valid @RequestBody PlaneDto planeDto)
    {
        HttpStatus httpStatus;
        String body;
        
        try
        {
            Plane plane = planeService.update(planeId, planeDto);
            httpStatus = HttpStatus.OK;
            body =  new JSONSerializer()
                .include("id","manufacturer","model","totalSeats","manufactureDate"
                        ,"maxBusinessSeats","maxTouristSeats","maxOfferSeats")
                .exclude("*", "*.class")
                .serialize(plane);
            
        }
        catch(EntityNotFoundException exception)
        {
            httpStatus = HttpStatus.NOT_FOUND;
            body = new JSONSerializer()
                    .include("message")
                    .exclude("*","*.class")
                    .serialize(exception);
        }
        
        return new ResponseEntity<String>(body,httpStatus);
    }
    
    @RequestMapping( method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<String> create(@Valid @RequestBody PlaneDto planeDto)
    {
        Plane plane = planeService.create(planeDto);

        String body =  new JSONSerializer()
                .include("id","manufacturer","model","totalSeats","manufactureDate"
                        ,"maxBusinessSeats","maxTouristSeats","maxOfferSeats")
                .exclude("*", "*.class")
                .serialize(plane);
        
        return new ResponseEntity<String>(body, HttpStatus.CREATED);
    }
    
    @RequestMapping(value = "/getAll" , method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<String> getAll()
    {
        List<Plane> planeList = planeService.getAll();
        
        String body = JsonUtil.serialize(planeList,new PlaneListSerializer());
        
        return new ResponseEntity<String>(body,HttpStatus.OK);
    }
    
    @RequestMapping(value = "/getAllByAirline/{airlineId}" , method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<String> getAllByAirline(@PathVariable ("airlineId") Integer airlineId)
    {
        List<Plane> planeList = planeService.getAllByAirline(airlineId);
        
        String body = JsonUtil.serialize(planeList,new PlaneListSerializer());
        
        return new ResponseEntity<String>(body,HttpStatus.OK);
    }
}
