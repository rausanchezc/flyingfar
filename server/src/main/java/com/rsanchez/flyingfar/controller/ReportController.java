package com.rsanchez.flyingfar.controller;

import com.rsanchez.flyingfar.domain.ReserveFlight;
import com.rsanchez.flyingfar.json.serializer.JsonUtil;
import com.rsanchez.flyingfar.json.serializer.ReserveFlightListSerializer;
import com.rsanchez.flyingfar.service.ReserveFlightService;
import flexjson.JSONSerializer;
import java.util.List;
import javax.persistence.NoResultException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@RequestMapping(value = "/reports", produces= "application/json; charset=utf-8")
@Controller("reportController")
public class ReportController{
    
    /* ************ PRIVATE ATTRIBUTES (SERVICES, ETC) ***************** */

    @Autowired
    private ReserveFlightService reserveFlightService;
    
    /* ************ PUBLIC METHODS (RESTFUL REQUEST HANDLERS) ********** */

    @RequestMapping(value = "/seat-occupancy/{airlineCode}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<String> getFlightsWithSeatOccupationByAirlineCode(@PathVariable("airlineCode") String airlineCode)
    {
        
        HttpStatus httpStatus;
        String body;
        
        try
        {
            List<ReserveFlight> reserveFlightList = reserveFlightService.getWithSeatOccupationByAirlineCode(airlineCode);
            System.out.println("[ReserveFlightList]" + reserveFlightList);
            httpStatus = HttpStatus.OK;
            body = JsonUtil.serialize(reserveFlightList, new ReserveFlightListSerializer());
        }
        catch(NoResultException exception)
        {
            httpStatus = HttpStatus.NOT_FOUND;
            body = new JSONSerializer()
                    .include("message")
                    .exclude("*","*.class")
                    .serialize(exception);
        }
        
        return new ResponseEntity<String>(body, httpStatus);   
    }
}
