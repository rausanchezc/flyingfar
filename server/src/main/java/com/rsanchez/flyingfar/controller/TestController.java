package com.rsanchez.flyingfar.controller;

import com.rsanchez.flyingfar.domain.Airline;
import com.rsanchez.flyingfar.service.AirlineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 */

@Controller("testController")
@RequestMapping(value="/test")
public class TestController 
{
    @Autowired
    private AirlineService airlineService;

    @RequestMapping(value="/status/ok", method = RequestMethod.GET)
    public ResponseEntity<String> statusOk()
    {
        return new ResponseEntity<String>(serializer("test ok"), HttpStatus.OK);
    }
    
    @RequestMapping(value="/airline/{airlineId}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<String> airlineOk(@PathVariable("airlineId") Integer airlineId)
    {
        HttpStatus httpStatus;
//        String body = "";
        
        Airline airline = airlineService.getById(airlineId);
        
        if(airline != null)
        {
            httpStatus = HttpStatus.OK;
        }
        else
        {
            httpStatus = HttpStatus.NOT_FOUND;
        }
        
        return new ResponseEntity<String>(serializer(airline.getFullName()), httpStatus);
    }
    
    private String serializer(String value)
    {
        return "{\"data\": \"" + value + "\"}";
    }
    
}
