package com.rsanchez.flyingfar.controller;

import com.rsanchez.flyingfar.domain.Role;
import com.rsanchez.flyingfar.domain.User;
import com.rsanchez.flyingfar.dto.UserDto;
import com.rsanchez.flyingfar.json.serializer.AdminCompanyListSerializer;
import com.rsanchez.flyingfar.json.serializer.JsonUtil;
import com.rsanchez.flyingfar.json.serializer.SessionSerializer;
import com.rsanchez.flyingfar.json.serializer.UserSerializer;
import com.rsanchez.flyingfar.service.UserService;
import com.rsanchez.flyingfar.spring.security.CustomUserDetails;
import flexjson.JSONSerializer;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 */
@RequestMapping(produces = "application/json; charset=utf-8")
@Controller("userController")
public class UserController 
{
     /** *************************** PRIVATE ATTRIBUTES (SERVICES, ETC) **************************** */
    
    @Autowired
    private UserService userService;
    
    /** ************************ PUBLIC METHODS (RESTFUL REQUEST HANDLERS) ************************ */
    
    /**
     * @param userId
     * @return */
    @RequestMapping(value = "/user/{userId}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<String> getById(@PathVariable("userId") Integer userId)
    {
        HttpStatus httpStatus;
        String body = "";
        
        User user = userService.getById(userId);
        
        if(user != null)
        {
            httpStatus = HttpStatus.OK;
            body = new JSONSerializer()
                .include("id","username","password","name","firstSurname","secondSurname","address","email","creditCard","role.label","email")
                .exclude("*","*.class")
                .serialize(user);
        }else
        {
            httpStatus = HttpStatus.NOT_FOUND;
        }
        
        return new ResponseEntity<String>(body,HttpStatus.OK);
    }
    
    @RequestMapping(value = "/user/{userId}", method = RequestMethod.PUT)
    @ResponseBody
    public ResponseEntity<String> update(@PathVariable("userId") Integer userId,
                                         @Valid @RequestBody UserDto userDto)
    {
        HttpStatus httpStatus;
        String body;

        try
        {
            User user  = userService.update(userId, userDto);
            
            httpStatus = HttpStatus.OK;
            body = new JSONSerializer()
                .include("id","username","password","name","firstSurname","secondSurname","address","email","creditCard","role.label","email")
                .exclude("*","*.class")
                .serialize(user);
        }
        catch(EntityNotFoundException exception)
        {
            httpStatus = HttpStatus.NOT_FOUND;
            body = new JSONSerializer()
                    .include("message")
                    .exclude("*","*.class")
                    .serialize(exception);
        }
    
        return new ResponseEntity<String>(body, httpStatus);
    }
    
    @RequestMapping(value = "/user/{userId}" , method = RequestMethod.DELETE)
    @ResponseBody
    public ResponseEntity<String> delete(@PathVariable("userId") Integer userId)
    {
        HttpStatus httpStatus;
    
        try
        {
            userService.delete(userId);
            httpStatus = HttpStatus.NO_CONTENT;
        }
        catch(EntityNotFoundException exception)
        {
            httpStatus = HttpStatus.NOT_FOUND;
        }
        
        return new ResponseEntity<String>(httpStatus);
    }
    
    /** 
    * @param userDto
    * @return  */
    
    @RequestMapping(value = "/user", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<String> create(@Valid @RequestBody UserDto userDto)
    {
        HttpStatus httpStatus;

        User user = userService.create(userDto);
        
        String body = new JSONSerializer()
                .include("id","username","password","name","firstSurname","secondSurname","address","email","creditCard","role.label")
                .exclude("*","*.class")
                .serialize(user);
        
        httpStatus = HttpStatus.CREATED;
        
        return new ResponseEntity<String>(body, httpStatus);
    }
    
    
    @RequestMapping(value = "/user/getAdminCompanyList", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<String> getAdminCompanyList()
    {
        HttpStatus httpStatus;
        String body = "";
        
        try
        {
            httpStatus = HttpStatus.OK;
            List<User> adminCompanyList = userService.getAdminCompanyList();
            
            body = JsonUtil.serialize(adminCompanyList, new AdminCompanyListSerializer());
        }
        catch(EntityNotFoundException exception)
        {
            httpStatus = HttpStatus.NOT_FOUND;
        }
    
        return new ResponseEntity<String>(body,httpStatus);
    }
    
    
    @RequestMapping( value = "/user", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<String> getByUsername(@RequestParam("username") String username)
    {
        HttpStatus httpStatus;
        String body = "";
        
        try
        {
            httpStatus = HttpStatus.OK;
            User user = userService.getByUsername(username);
            
            body = JsonUtil.serialize(user, new UserSerializer());
        }
        catch(EntityNotFoundException exception)
        {
            httpStatus = HttpStatus.NOT_FOUND;
        }
    
        return new ResponseEntity<String>(body,httpStatus);
    }
    
    /**
     * @return authentication
     */
//    @PreAuthorize("hasRole('CLIENT')") 
//    @PreAuthorize("permitAll") 
    @PreAuthorize("hasAnyRole('ROLE_CLIENT','ROLE_SYSTEM_ADMIN','ROLE_AIRLINE_ADMIN')")
    @RequestMapping(value = "/refresh", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    public ResponseEntity<String> refresh()
    {
        HttpStatus httpStatus;
        String body = "";

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null && authentication.isAuthenticated() && authentication.getPrincipal() instanceof CustomUserDetails)
        {
            httpStatus = HttpStatus.OK;
            body = JsonUtil.serialize(authentication, new SessionSerializer());
        }
        else
        {
            httpStatus = HttpStatus.UNAUTHORIZED;
        }

        return new ResponseEntity<String>(body, httpStatus);
    }
    
    /**
     * 
     * @param username
     * @return 
     */
    @RequestMapping( value = "/user/{username}/role" , method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<String> getRoleByUsername(@PathVariable("username") String username)
    {
        HttpStatus httpStatus ;
        String body = "";
        
        User user = userService.getByUsername(username);
        
        if(user != null)
        {
            
            httpStatus = HttpStatus.OK;
            body = new JSONSerializer()
                .include("role.name")
                .exclude("*","*.class")
                .serialize(user);
        }
        else
        {
            httpStatus = HttpStatus.NOT_FOUND;
            body = "";
        }

        return new ResponseEntity<String>(body,httpStatus);
    }
    
    @RequestMapping( value = "/user/exists/username" , method = RequestMethod.GET)
    @ResponseBody
    public  Map<String, Boolean> existsUsername(@RequestParam("username") String username)
    {
        HttpStatus httpStatus ;
        
        boolean isAvaliable = userService.existsUsername(username);

        Map<String,Boolean> response = new HashMap<String,Boolean>();
        httpStatus = HttpStatus.OK;
        
        response.put("exists", isAvaliable);
        
        return response;
    }
    
    
    @RequestMapping( value = "/user/exists/email" , method = RequestMethod.GET)
    @ResponseBody
    public  Map<String, Boolean> existsEmail(@RequestParam("email") String email)
    {
        HttpStatus httpStatus ;

        boolean isAvaliable = userService.existsEmail(email);

        Map<String,Boolean> response = new HashMap<String,Boolean>();
        httpStatus = HttpStatus.OK;
        
        response.put("exists", isAvaliable);
        
        return response;
    }
    
    /**
     * @param text
     * @return encodeded text
     */
    @RequestMapping(value = "/encode/{text}", method = RequestMethod.GET, produces = "application/json; charset=utf-8")
    @ResponseBody
    public Map<String, Object> encode(@PathVariable("text") String text)
    {
        Map<String, Object> model = new HashMap<String, Object>();

        model.put("text", text);
        model.put("encodedText", new ShaPasswordEncoder(512).encodePassword(text, "7d8a3ded"));

        return model;
    }
}
