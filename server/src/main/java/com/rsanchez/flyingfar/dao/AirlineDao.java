package com.rsanchez.flyingfar.dao;

import com.rsanchez.flyingfar.domain.Airline;
import java.io.Serializable;

/**
 */

public interface AirlineDao extends GenericDao<Airline>, Serializable
{
    public Airline getByCode(String airlineCode);
    
    public Airline getByUserId(Integer userId);
}
