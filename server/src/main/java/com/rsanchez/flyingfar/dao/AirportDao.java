package com.rsanchez.flyingfar.dao;

import com.rsanchez.flyingfar.domain.Airport;
import java.io.Serializable;

/**
 *
 */

public interface AirportDao extends GenericDao<Airport>, Serializable
{
    public Airport getByAirportCode(String airportCode);
}
