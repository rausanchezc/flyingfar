package com.rsanchez.flyingfar.dao;

import com.rsanchez.flyingfar.domain.City;
import java.io.Serializable;

/**
 */

public interface CityDao extends GenericDao<City>, Serializable
{
    
}
