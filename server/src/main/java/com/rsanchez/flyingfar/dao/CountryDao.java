package com.rsanchez.flyingfar.dao;

import com.rsanchez.flyingfar.domain.Country;
import java.io.Serializable;

/**
 */

public interface CountryDao extends GenericDao<Country>, Serializable
{
    
}
