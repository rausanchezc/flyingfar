package com.rsanchez.flyingfar.dao;

import com.rsanchez.flyingfar.domain.Flight;
import com.rsanchez.flyingfar.dto.FlightSearchDto;
import java.io.Serializable;
import java.util.List;

/**
 */

public interface FlightDao extends GenericDao<Flight>, Serializable
{
    public List<Flight> getValidFlightList(); 
    
    public List<Flight> getByAirlineId(Integer airlineId);
    
    public List<Flight> getByRestrictions(FlightSearchDto flightDto);
}
