package com.rsanchez.flyingfar.dao;

import java.util.List;
import java.util.Map;


public interface GenericDao<T>
{
    void create(T entity);

    void create(List<T> entities);

    void update(T entity);

    void delete(T entity);

    void delete(Integer id);

    void flush();

    void refresh(T entity);

    T find(Object id);

    List<T> find(String[] fields, Object[] values);

    List<T> findAll();

    List<T> get(Integer start, Integer size);

    Long count(Map<String, String> filter, Map<String, String>... filterOperator);

}
