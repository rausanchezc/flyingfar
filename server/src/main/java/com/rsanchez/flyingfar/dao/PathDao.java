package com.rsanchez.flyingfar.dao;

import com.rsanchez.flyingfar.domain.Path;
import java.io.Serializable;

/**
 */

public interface PathDao extends GenericDao<Path>, Serializable
{
    
}
