package com.rsanchez.flyingfar.dao;

import com.rsanchez.flyingfar.domain.Plane;
import java.io.Serializable;
import java.util.List;

/**
 */

public interface PlaneDao extends GenericDao<Plane>, Serializable
{
    public List<Plane> getByModel(String planeModel);

    public List<Plane> getByAirline(Integer airlineId);
    
}
