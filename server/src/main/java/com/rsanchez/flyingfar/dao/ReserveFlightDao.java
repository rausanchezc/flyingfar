package com.rsanchez.flyingfar.dao;

import com.rsanchez.flyingfar.domain.ReserveFlight;
import java.io.Serializable;
import java.util.List;

/**
 */

public interface ReserveFlightDao extends GenericDao<ReserveFlight>, Serializable
{       
    Integer countReserveFlightByUserId(Integer userId);
    
    List<ReserveFlight> getOnlyActivesByFlightId(Integer flightId);
    
    List<ReserveFlight> getByUserId(Integer userId);
    
    List<ReserveFlight> getWithSeatOccupationByAirlineCode(String airlineCode);
}
