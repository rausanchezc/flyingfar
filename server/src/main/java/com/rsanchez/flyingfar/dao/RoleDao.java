package com.rsanchez.flyingfar.dao;

import com.rsanchez.flyingfar.domain.Role;
import java.io.Serializable;

/**
 */

public interface RoleDao extends GenericDao<Role>, Serializable
{
    public Role getByName(String roleName);
}
