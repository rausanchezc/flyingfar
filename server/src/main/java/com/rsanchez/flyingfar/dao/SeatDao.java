package com.rsanchez.flyingfar.dao;

import com.rsanchez.flyingfar.domain.Seat;
import java.io.Serializable;

/**
 */

public interface SeatDao extends GenericDao<Seat>, Serializable
{
    
}
