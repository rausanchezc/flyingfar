package com.rsanchez.flyingfar.dao;

import com.rsanchez.flyingfar.domain.SeatType;
import java.io.Serializable;

/**
 */

public interface SeatTypeDao extends GenericDao<SeatType>, Serializable
{
    public SeatType getByCategory(String seatTypeCategory);
}
