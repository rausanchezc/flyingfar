package com.rsanchez.flyingfar.dao;

import com.rsanchez.flyingfar.domain.SeatTypeIntoFlight;
import java.io.Serializable;

/**
 */

public interface SeatTypeIntoFlightDao extends GenericDao<SeatTypeIntoFlight>, Serializable
{

}
