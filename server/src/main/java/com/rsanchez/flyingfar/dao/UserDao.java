package com.rsanchez.flyingfar.dao;

import com.rsanchez.flyingfar.domain.User;
import java.io.Serializable;
import java.util.List;

/**
 */

public interface UserDao extends GenericDao<User>, Serializable
{
    public boolean existsEmail(String email);
    
    public boolean existsUsername(String username);
    
    public List<User> getUserByRole(Integer idRole);
    
    public User getByUsername(String username);
}
