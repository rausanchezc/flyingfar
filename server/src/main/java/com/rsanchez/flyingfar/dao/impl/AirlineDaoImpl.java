package com.rsanchez.flyingfar.dao.impl;

import com.rsanchez.flyingfar.dao.AirlineDao;
import com.rsanchez.flyingfar.domain.Airline;
import java.util.List;
import org.springframework.stereotype.Repository;

/**
 *
 */
@Repository("airlineDao")
public class AirlineDaoImpl extends GenericDaoImpl<Airline> implements AirlineDao{

    private static final long serialVersionUID = 1L;
    
    public AirlineDaoImpl()
    {
        super(Airline.class);
    }

    @Override
    public Airline getByCode(String airlineCode) 
    {
        return (Airline) entityManager.createNamedQuery("Airline.findByCode")
                .setParameter("airlineCode",airlineCode)
                .getSingleResult();
    }

    @Override
    public Airline getByUserId(Integer userId) 
    {
        return (Airline) entityManager.createNamedQuery("Airline.findByUserId")
                .setParameter("userId", userId)
                .getSingleResult();
        
    }
    
    
}   
