package com.rsanchez.flyingfar.dao.impl;

import com.rsanchez.flyingfar.dao.AirportDao;
import com.rsanchez.flyingfar.domain.Airport;
import org.springframework.stereotype.Repository;

/**
 *
 */

@Repository("airportDao")
public class AirportDaoImpl extends GenericDaoImpl<Airport> implements AirportDao
{
    private static final long serialVersionUID = 1L;
    
    public AirportDaoImpl() 
    {
        super(Airport.class);
    }

    @Override
    public Airport getByAirportCode(String airportCode) 
    {
        return (Airport) entityManager.createNamedQuery("Airport.findByCode")
                .setParameter("airportCode", airportCode)
                .getSingleResult();
    }
    
    
    
}
