package com.rsanchez.flyingfar.dao.impl;

import com.rsanchez.flyingfar.dao.CityDao;
import com.rsanchez.flyingfar.domain.City;
import org.springframework.stereotype.Repository;

/**
 *
 */
@Repository("cityDao")
public class CityDaoImpl extends GenericDaoImpl<City> implements CityDao
{
    private static final long serialVersionUID = 1L;
    
    public CityDaoImpl()
    {
        super(City.class);
    }
}   
