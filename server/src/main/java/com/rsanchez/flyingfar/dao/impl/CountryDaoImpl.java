package com.rsanchez.flyingfar.dao.impl;

import com.rsanchez.flyingfar.dao.CountryDao;
import com.rsanchez.flyingfar.domain.Country;
import org.springframework.stereotype.Repository;

/**
 *
 */
@Repository("countryDao")
public class CountryDaoImpl extends GenericDaoImpl<Country> implements CountryDao
{
    private static final long serialVersionUID = 1L;
    
    public CountryDaoImpl()
    {
        super(Country.class);
    }
}   
