package com.rsanchez.flyingfar.dao.impl;

import com.rsanchez.flyingfar.dao.FlightDao;
import com.rsanchez.flyingfar.domain.Flight;
import com.rsanchez.flyingfar.dto.FlightSearchDto;
import java.util.List;
import org.springframework.stereotype.Repository;

/**
 *
 */
@Repository("flightDao")
public class FlightDaoImpl extends GenericDaoImpl<Flight> implements FlightDao
{
    private static final long serialVersionUID = 1L;
    
    public FlightDaoImpl()
    {
        super(Flight.class);
    }

    @Override
    public List<Flight> getValidFlightList() 
    {
        return (List<Flight>) entityManager.createNamedQuery("Flight.findAllValid")
                .getResultList();
    }

    @Override
    public List<Flight> getByAirlineId(Integer airlineId) 
    {
        return (List<Flight>) entityManager.createNamedQuery("Flight.findByAirlineId")
                .setParameter("airlineId", airlineId)
                .getResultList();
    }

    @Override
    public List<Flight> getByRestrictions(FlightSearchDto flightDto)
    {
        return (List<Flight>)entityManager.createNamedQuery("Flight.findByRestrictions")
                .setParameter("originId", flightDto.getOrigin())
                .setParameter("destinationId", flightDto.getDestination())
                .setParameter("departure", flightDto.getDeparture())
                .setParameter("roundTrip", flightDto.isRoundTrip())
                .getResultList();
    }


}   
