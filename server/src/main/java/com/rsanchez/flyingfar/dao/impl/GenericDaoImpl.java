package com.rsanchez.flyingfar.dao.impl;

import com.rsanchez.flyingfar.dao.GenericDao;
import java.awt.print.Pageable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;


public class GenericDaoImpl<T> implements GenericDao<T>
{
    // Greater Than
    private static final String GT_OPERATOR = "GT";

    @PersistenceContext
    protected EntityManager entityManager;

    protected Class<T> clazz;

    public GenericDaoImpl(Class<T> clazz)
    {
        this.clazz = clazz;
    }

    @Override
    public void create(T entity)
    {
        entityManager.persist(entity);
    }

    @Override
    public void create(List<T> entities)
    {
        for (T entity : entities)
        {
            entityManager.persist(entity);
        }
    }

    @Override
    public void update(T entity)
    {
        entityManager.merge(entity);
    }

    @Override
    public void delete(T entity)
    {
        entityManager.remove(entity);
    }

    @Override
    public void delete(Integer id)
    {
        entityManager.remove(entityManager.find(clazz, id));
    }

    @Override
    public void flush()
    {
        entityManager.flush();
    }

    @Override
    public void refresh(T entity)
    {
        entityManager.refresh(entity);
    }

    @Override
    public T find(Object id)
    {
        return id != null ? (T) entityManager.find(clazz, id) : null;
    }

    @Override
    public List<T> find(String[] fields,
                        Object[] values)
    {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(clazz);
        Root<T> root = criteriaQuery.from(clazz);
        List<Predicate> predicates = new ArrayList<Predicate>(fields.length);

        for (int i = 0; i < fields.length; i++)
        {
            predicates.add(criteriaBuilder.equal(root.get(fields[i]), values[i]));
        }

        criteriaQuery.select(root).where(predicates.toArray(new Predicate[predicates.size()]));

        List<T> result = entityManager.createQuery(criteriaQuery).getResultList();

        return result;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<T> findAll()
    {
        return (List<T>) entityManager.createNamedQuery(clazz.getSimpleName() + ".findAll").getResultList();
    }

    @Override
    public List<T> get(Integer start,
                       Integer size)
    {
        return (List<T>) entityManager.createNamedQuery(clazz.getSimpleName() + ".findAll")
        .setFirstResult(start)
        .setMaxResults(size)
        .getResultList();
    }

    private String deletePercentage(String value)
    {
        if (value.charAt(0) == '%' && value.charAt(value.length() - 1) == '%')
        {
            value = value.substring(1, value.length() - 1);
        }
        return value;
    }

    @Override
    public Long count(Map<String, String> filter, Map<String, String>... filterOperator)
    {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Long> criteriaQuery = criteriaBuilder.createQuery(Long.class);
        Root<T> root = criteriaQuery.from(clazz);

        criteriaQuery.select(criteriaBuilder.count(root));

        if (filter != null && !filter.isEmpty())
        {
            List<Predicate> predicates = new ArrayList<Predicate>(filter.size());

            for (Map.Entry<String, String> entry : filter.entrySet())
            {
                if (!entry.getValue().isEmpty())
                {
                    try
                    {
                        String[] fields = entry.getKey().split("\\.");
                        Path path = null;

                        String operator = null;
                        try
                        {
                            if (filterOperator != null && filterOperator[0] != null)
                            {
                                operator = filterOperator[0].get(entry.getKey());
                            }
                        }
                        catch (ArrayIndexOutOfBoundsException e)
                        {
                            operator = null;
                        }

                        if (!entry.getValue().matches("^(-)?[0-9]+$"))
                        {
                            for (String field : fields)
                            {
                                path = path != null ? path.<String>get(field) : root.<String>get(field);
                            }
                            if (operator != null && !operator.isEmpty())
                            {
                                entry.setValue(deletePercentage(entry.getValue()));
                                if (operator.equals(GT_OPERATOR))
                                {
                                    predicates.add(criteriaBuilder.greaterThan(path, entry.getValue()));
                                }
                            }
                            else
                            {
                                predicates.add(criteriaBuilder.like(path, entry.getValue()));
                            }
                        }
                        else
                        {
                            for (String field : fields)
                            {
                                path = path != null ? path.get(field) : root.get(field);
                            }

                            if (operator != null && !operator.isEmpty())
                            {
                                if (operator.equals(GT_OPERATOR))
                                {
                                    predicates.add(criteriaBuilder.greaterThan(path, entry.getValue()));
                                }
                            }
                            else
                            {
                                predicates.add(criteriaBuilder.equal(path, Integer.parseInt(entry.getValue())));
                            }
                        }

                    }
                    catch (Exception exception)
                    {
                        exception.printStackTrace();
                    }
                }
            }

            criteriaQuery.where(predicates.toArray(new Predicate[predicates.size()]));
        }

        return entityManager.createQuery(criteriaQuery).getSingleResult();
    }
}
