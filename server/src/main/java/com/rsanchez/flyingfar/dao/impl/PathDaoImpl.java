package com.rsanchez.flyingfar.dao.impl;

import com.rsanchez.flyingfar.dao.PathDao;
import com.rsanchez.flyingfar.domain.Path;
import org.springframework.stereotype.Repository;

/**
 *
 */
@Repository("pathDao")
public class PathDaoImpl extends GenericDaoImpl<Path> implements PathDao
{
    private static final long serialVersionUID = 1L;
    
    public PathDaoImpl()
    {
        super(Path.class);
    }
}   
