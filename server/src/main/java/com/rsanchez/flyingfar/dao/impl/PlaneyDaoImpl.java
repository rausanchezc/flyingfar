package com.rsanchez.flyingfar.dao.impl;

import com.rsanchez.flyingfar.dao.PlaneDao;
import com.rsanchez.flyingfar.domain.Plane;
import java.util.List;
import org.springframework.stereotype.Repository;

/**
 *
 */
@Repository("planeDao")
public class PlaneyDaoImpl extends GenericDaoImpl<Plane> implements PlaneDao
{
    private static final long serialVersionUID = 1L;
    
    public PlaneyDaoImpl()
    {
        super(Plane.class);
    }

    @Override
    public List<Plane> getByModel(String planeModel) 
    {
        return (List<Plane>) entityManager.createNamedQuery("Plane.findByModel")
                .setParameter("model", planeModel)
                .getResultList();
    }

    @Override
    public List<Plane> getByAirline(Integer airlineId)
    {
        return (List<Plane>) entityManager.createNamedQuery("Plane.findByAirline")
                .setParameter("airlineId", airlineId)
                .getResultList();
    }
}   
