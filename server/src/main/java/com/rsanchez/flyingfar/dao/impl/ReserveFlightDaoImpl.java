package com.rsanchez.flyingfar.dao.impl;

import com.rsanchez.flyingfar.dao.ReserveFlightDao;
import com.rsanchez.flyingfar.domain.ReserveFlight;
import com.rsanchez.flyingfar.util.Util;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import org.springframework.stereotype.Repository;

/**
 *
 */
@Repository("reserveFlightDao")
public class ReserveFlightDaoImpl extends GenericDaoImpl<ReserveFlight> implements ReserveFlightDao{

    private static final long serialVersionUID = 1L;

    public ReserveFlightDaoImpl() {
        super(ReserveFlight.class);
    }
    
    @Override
    public Integer countReserveFlightByUserId(Integer userId)
    {
        List<ReserveFlight> reserveFlightList =  (List<ReserveFlight>) entityManager.createNamedQuery("ReserveFlight.countReservesByUserId")
                .setParameter("userId", userId)
                .getResultList();

        Iterator<ReserveFlight> it = reserveFlightList.iterator();
        Date prev24H = Util.subtractOneDay(new Date());

        while(it.hasNext())
        {
            ReserveFlight reserveFlight = it.next();
            
            if(reserveFlight.getReserveDate().before(prev24H) ||  reserveFlight.getIsPaid())
            {
                it.remove();
            }
        }
        
        return reserveFlightList.size();
    }

    @Override
    public List<ReserveFlight> getOnlyActivesByFlightId(Integer flightId) 
    {
        List<ReserveFlight> reserveFlightList = (List<ReserveFlight>) entityManager.createNamedQuery("ReserveFlight.findActivesByFlightId")
                .setParameter("flightId",flightId)
                .getResultList();
        
        Iterator<ReserveFlight> it = reserveFlightList.iterator();
        Date prev24H = Util.subtractOneDay(new Date());

        while(it.hasNext())
        {
            ReserveFlight reserveFlight = it.next();
            if(reserveFlight.getReserveDate().before(prev24H) ||  reserveFlight.getIsPaid()){it.remove();}
        }
        
        return reserveFlightList;
    }

    @Override
    public List<ReserveFlight> getByUserId(Integer userId) 
    {
        return (List<ReserveFlight>) entityManager.createNamedQuery("ReserveFlight.findAllByUserId")
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    public List<ReserveFlight> getWithSeatOccupationByAirlineCode(String airlineCode) 
    {
        return (List<ReserveFlight>)entityManager.createNamedQuery("ReserveFlight.findAllByAirlineCode")
                .setParameter("airlineCode", airlineCode)
                .getResultList();
    }
    
}   
