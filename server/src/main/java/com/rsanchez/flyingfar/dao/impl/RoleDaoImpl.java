package com.rsanchez.flyingfar.dao.impl;

import com.rsanchez.flyingfar.dao.RoleDao;
import com.rsanchez.flyingfar.domain.Role;
import org.springframework.stereotype.Repository;

/**
 *
 */
@Repository("roleDao")
public class RoleDaoImpl extends GenericDaoImpl<Role> implements RoleDao
{
    private static final long serialVersionUID = 1L;
    
    public RoleDaoImpl()
    {
        super(Role.class);
    }

    @Override
    public Role getByName(String roleName)
    {
        return (Role) entityManager.createNamedQuery("Role.findByName")
                .setParameter("roleName", roleName)
                .getSingleResult();
    }
}   
