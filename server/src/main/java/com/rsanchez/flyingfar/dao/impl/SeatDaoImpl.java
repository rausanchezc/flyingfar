package com.rsanchez.flyingfar.dao.impl;

import com.rsanchez.flyingfar.dao.SeatDao;
import com.rsanchez.flyingfar.domain.Seat;
import org.springframework.stereotype.Repository;

/**
 *
 */
@Repository("seatDao")
public class SeatDaoImpl extends GenericDaoImpl<Seat> implements SeatDao
{
    private static final long serialVersionUID = 1L;
    
    public SeatDaoImpl()
    {
        super(Seat.class);
    }
}   
