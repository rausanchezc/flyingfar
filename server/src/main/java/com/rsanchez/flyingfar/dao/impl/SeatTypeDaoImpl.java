package com.rsanchez.flyingfar.dao.impl;

import com.rsanchez.flyingfar.dao.SeatTypeDao;
import com.rsanchez.flyingfar.domain.SeatType;
import org.springframework.stereotype.Repository;

/**
 *
 */
@Repository("seatTypeDao")
public class SeatTypeDaoImpl extends GenericDaoImpl<SeatType> implements SeatTypeDao
{
    private static final long serialVersionUID = 1L;
    
    public SeatTypeDaoImpl()
    {
        super(SeatType.class);
    }

    @Override
    public SeatType getByCategory(String seatTypeCategory) 
    {
        return (SeatType) entityManager.createNamedQuery("SeatType.findByCategory")
                .setParameter("category", seatTypeCategory)
                .getSingleResult();
    }
    
    
}   
