package com.rsanchez.flyingfar.dao.impl;

import com.rsanchez.flyingfar.dao.SeatTypeIntoFlightDao;
import com.rsanchez.flyingfar.domain.SeatTypeIntoFlight;
import org.springframework.stereotype.Repository;

/**
 *
 */
@Repository("seatTypeIntoFlightDao")
public class SeatTypeIntoFlightDaoImpl extends GenericDaoImpl<SeatTypeIntoFlight> implements SeatTypeIntoFlightDao
{
    private static final long serialVersionUID = 1L;
    
    public SeatTypeIntoFlightDaoImpl()
    {
        super(SeatTypeIntoFlight.class);
    }
}   
