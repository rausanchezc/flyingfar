package com.rsanchez.flyingfar.dao.impl;

import com.rsanchez.flyingfar.dao.UserDao;
import com.rsanchez.flyingfar.domain.User;
import java.util.List;
import org.springframework.stereotype.Repository;

/**
 *
 */
@Repository("userDao")
public class UserDaoImpl extends GenericDaoImpl<User> implements UserDao
{
    private static final long serialVersionUID = 1L;
    
    public UserDaoImpl()
    {
        super(User.class);
    }

    @Override
    public boolean existsEmail(String email)
    {
        return (boolean)!entityManager.createNamedQuery("User.findByEmail")
                .setParameter("email", email)
                .getResultList().isEmpty();
    }
    
    @Override
    public boolean existsUsername(String username)
    {
           return (boolean) !entityManager.createNamedQuery("User.findByUsername")
                 .setParameter("username", username)
                 .getResultList().isEmpty();
                
    }

    @Override
    public List<User> getUserByRole(Integer idRole) 
    {
        return (List<User>) entityManager.createNamedQuery("User.findByRole")
                .setParameter("idRole", idRole)
                .getResultList();
    }

    @Override
    public User getByUsername(String username) 
    {
        User user = (User) entityManager.createNamedQuery("User.findByUsername")
                .setParameter("username", username)
                .getSingleResult();
        
        return user;
    }

    }
