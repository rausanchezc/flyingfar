/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.rsanchez.flyingfar.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.codehaus.jackson.annotate.JsonIgnore;

/**
 *
 * @author rsanchez
 */
@Entity
@Table(name = "airline")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Airline.findAll", query = "SELECT a FROM Airline a"),
    @NamedQuery(name = "Airline.findById", query = "SELECT a FROM Airline a WHERE a.id = :id"),
    @NamedQuery(name = "Airline.findByCode", query = "SELECT a FROM Airline a WHERE a.code = :code"),
    @NamedQuery(name = "Airline.findByFullName", query = "SELECT a FROM Airline a WHERE a.fullName = :fullName"),
    @NamedQuery(name = "Airline.findByCreationDate", query = "SELECT a FROM Airline a WHERE a.creationDate = :creationDate"),
    @NamedQuery(name = "Airline.findByLastModificationDate", query = "SELECT a FROM Airline a WHERE a.lastModificationDate = :lastModificationDate"),
    @NamedQuery(name = "Airline.findByLastFlightCode", query = "SELECT a FROM Airline a WHERE a.lastFlightCode = :lastFlightCode"),
    // Custom Queries
    @NamedQuery(name = "Airline.findByUserId" , query = "SELECT a from Airline a WHERE EXISTS (SELECT u.airline FROM User u WHERE a.id = u.airline.id AND u.id = :userId)")
})

public class Airline implements Serializable 
{
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "code")
    private String code;
    
    @Size(max = 60)
    @Column(name = "fullName")
    private String fullName;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "creationDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date creationDate;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "lastModificationDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastModificationDate;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "lastFlightCode")
    private int lastFlightCode;
    
    @JoinTable(name = "AIRLINE_OPERATE_AIRPORT", joinColumns = {
        @JoinColumn(name = "airline", referencedColumnName = "id")}, inverseJoinColumns = {
        @JoinColumn(name = "airport", referencedColumnName = "id")})
    @ManyToMany
    private List<Airport> airportList;
    
    @OneToMany(mappedBy = "airline")
    private List<User> userList;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "airline")
    private List<Flight> flightList;
    
    @OneToMany(mappedBy = "airline")
    private List<Plane> planeList;

    public Airline() {
    }

    public Airline(Integer id) {
        this.id = id;
    }

    public Airline(Integer id, String code, Date creationDate, Date lastModificationDate, int lastFlightCode) {
        this.id = id;
        this.code = code;
        this.creationDate = creationDate;
        this.lastModificationDate = lastModificationDate;
        this.lastFlightCode = lastFlightCode;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getLastModificationDate() {
        return lastModificationDate;
    }

    public void setLastModificationDate(Date lastModificationDate) {
        this.lastModificationDate = lastModificationDate;
    }

    public int getLastFlightCode() {
        return lastFlightCode;
    }

    public void setLastFlightCode(int lastFlightCode) {
        this.lastFlightCode = lastFlightCode;
    }

    @XmlTransient
    @JsonIgnore
    public List<Airport> getAirportList() {
        return airportList;
    }

    public void setAirportList(List<Airport> airportList) {
        this.airportList = airportList;
    }

    @XmlTransient
    @JsonIgnore
    public List<User> getUserList() {
        return userList;
    }

    public void setUserList(List<User> userList) {
        this.userList = userList;
    }

    @XmlTransient
    @JsonIgnore
    public List<Flight> getFlightList() {
        return flightList;
    }

    public void setFlightList(List<Flight> flightList) {
        this.flightList = flightList;
    }

    @XmlTransient
    @JsonIgnore
    public List<Plane> getPlaneList() {
        return planeList;
    }

    public void setPlaneList(List<Plane> planeList) {
        this.planeList = planeList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Airline)) {
            return false;
        }
        Airline other = (Airline) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.sanchez.flyingfar.domain.Airline[ id=" + id + " ]";
    }

    }
