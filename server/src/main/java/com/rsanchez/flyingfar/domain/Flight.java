package com.rsanchez.flyingfar.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.codehaus.jackson.annotate.JsonIgnore;

/**
 *
 * @author rsanchez
 */
@Entity
@Table(name = "flight")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Flight.findAll", query = "SELECT f FROM Flight f"),
    @NamedQuery(name = "Flight.findById", query = "SELECT f FROM Flight f WHERE f.id = :id"),
    @NamedQuery(name = "Flight.findByCode", query = "SELECT f FROM Flight f WHERE f.code = :code"),
    @NamedQuery(name = "Flight.findByDeparture", query = "SELECT f FROM Flight f WHERE f.departure = :departure"),
    @NamedQuery(name = "Flight.findByArrival", query = "SELECT f FROM Flight f WHERE f.arrival = :arrival"),
    @NamedQuery(name = "Flight.findByCreationDate", query = "SELECT f FROM Flight f WHERE f.creationDate = :creationDate"),
    @NamedQuery(name = "Flight.findByLastModificationDAte", query = "SELECT f FROM Flight f WHERE f.lastModificationDAte = :lastModificationDAte"),
    @NamedQuery(name = "Flight.findByLogicDeletion", query = "SELECT f FROM Flight f WHERE f.logicDeletion = :logicDeletion"),
    @NamedQuery(name = "Flight.findByRoundTrip", query = "SELECT f FROM Flight f WHERE f.roundTrip = :roundTrip"),
    @NamedQuery(name = "Flight.findByAmount", query = "SELECT f FROM Flight f WHERE f.amount = :amount"),
    // Custom Queries
    @NamedQuery(name = "Flight.findAllValid", query = "SELECT f FROM Flight f WHERE f.logicDeletion is null"),
    @NamedQuery(name = "Flight.findByAirlineId", query="SELECT f FROM Flight f WHERE f.airline.id = :airlineId AND f.logicDeletion is null"),
    @NamedQuery(name = "Flight.findByRestrictions", query ="SELECT f FROM Flight f WHERE f.origin.id  = :originId AND f.destination.id = :destinationId AND f.departure >= :departure AND f.logicDeletion is null AND f.roundTrip = :roundTrip")
})

public class Flight implements Serializable 
{
    @ManyToMany(mappedBy = "flightList")
    private List<Airline> airlineList;

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "code")
    private String code;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "departure")
    @Temporal(TemporalType.TIMESTAMP)
    private Date departure;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "arrival")
    @Temporal(TemporalType.TIMESTAMP)
    private Date arrival;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "creationDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date creationDate;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "lastModificationDAte")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastModificationDAte;
    
    @Column(name = "logicDeletion")
    private Boolean logicDeletion;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "roundTrip")
    private boolean roundTrip;

    @Column(name = "amount")
    private BigDecimal amount;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "flight")
    private List<Path> pathList;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "flight")
    private List<SeatTypeIntoFlight> seatTypeIntoFlightList;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "flight")
    private List<ReserveFlight> reserveFlightList;
    
    @JoinColumn(name = "origin", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private City origin;
    
    @JoinColumn(name = "destination", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private City destination;
    
    @JoinColumn(name = "plane", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Plane plane;
    
    @JoinColumn(name = "airline", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Airline airline;

    public Flight() {}

    public Flight(Integer id)
    {
        this.id = id;
    }

    public Flight(Integer id, String code, Date departure, Date arrival, Date creationDate, Date lastModificationDAte, boolean roundTrip) 
    {
        this.id = id;
        this.code = code;
        this.departure = departure;
        this.arrival = arrival;
        this.creationDate = creationDate;
        this.lastModificationDAte = lastModificationDAte;
        this.roundTrip = roundTrip;
    }

    public Integer getId() 
    {
        return id;
    }

    public void setId(Integer id) 
    {
        this.id = id;
    }

    public String getCode() 
    {
        return code;
    }

    public void setCode(String code) 
    {
        this.code = code;
    }

    public Date getDeparture() 
    {
        return departure;
    }

    public void setDeparture(Date departure) 
    {
        this.departure = departure;
    }

    public Date getArrival() 
    {
        return arrival;
    }

    public void setArrival(Date arrival) 
    {
        this.arrival = arrival;
    }

    public Date getCreationDate() 
    {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) 
    {
        this.creationDate = creationDate;
    }

    public Date getLastModificationDAte() 
    {
        return lastModificationDAte;
    }

    public void setLastModificationDAte(Date lastModificationDAte) 
    {
        this.lastModificationDAte = lastModificationDAte;
    }

    public Boolean getLogicDeletion() 
    {
        return logicDeletion;
    }

    public void setLogicDeletion(Boolean logicDeletion) 
    {
        this.logicDeletion = logicDeletion;
    }

    public boolean getRoundTrip() 
    {
        return roundTrip;
    }

    public void setRoundTrip(boolean roundTrip) 
    {
        this.roundTrip = roundTrip;
    }

    public BigDecimal getAmount() 
    {
        return amount;
    }

    public void setAmount(BigDecimal amount) 
    {
        this.amount = amount;
    }

    @XmlTransient
    @JsonIgnore
    public List<Path> getPathList() 
    {
        return pathList;
    }

    public void setPathList(List<Path> pathList) {
        this.pathList = pathList;
    }

    @XmlTransient
    @JsonIgnore
    public List<SeatTypeIntoFlight> getSeatTypeIntoFlightList() {
        return seatTypeIntoFlightList;
    }

    public void setSeatTypeIntoFlightList(List<SeatTypeIntoFlight> seatTypeIntoFlightList) {
        this.seatTypeIntoFlightList = seatTypeIntoFlightList;
    }

    @XmlTransient
    @JsonIgnore
    public List<ReserveFlight> getReserveFlightList() {
        return reserveFlightList;
    }

    public void setReserveFlightList(List<ReserveFlight> reserveFlightList) {
        this.reserveFlightList = reserveFlightList;
    }

    public City getOrigin() {
        return origin;
    }

    public void setOrigin(City origin) {
        this.origin = origin;
    }

    public City getDestination() {
        return destination;
    }

    public void setDestination(City destination) {
        this.destination = destination;
    }

    public Plane getPlane() {
        return plane;
    }

    public void setPlane(Plane plane) {
        this.plane = plane;
    }

    public Airline getAirline() {
        return airline;
    }

    public void setAirline(Airline airline) {
        this.airline = airline;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Flight)) {
            return false;
        }
        Flight other = (Flight) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.rsanchez.flyingfar.domain.Flight[ id=" + id + " ]";
    }

    @XmlTransient
    @JsonIgnore
    public List<Airline> getAirlineList() {
        return airlineList;
    }

    public void setAirlineList(List<Airline> airlineList) {
        this.airlineList = airlineList;
    }

    }
