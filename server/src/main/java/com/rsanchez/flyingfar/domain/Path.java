/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.rsanchez.flyingfar.domain;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**

 */
@Entity
@Table(name = "path")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Path.findAll", query = "SELECT p FROM Path p"),
    @NamedQuery(name = "Path.findById", query = "SELECT p FROM Path p WHERE p.id = :id"),
//    @NamedQuery(name = "Path.findByFlight", query = "SELECT p FROM Path p WHERE p.pathPK.flight = :flight"),
    @NamedQuery(name = "Path.findBySlot", query = "SELECT p FROM Path p WHERE p.slot = :slot"),
    @NamedQuery(name = "Path.findByDuration", query = "SELECT p FROM Path p WHERE p.duration = :duration")
})

public class Path implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @NotNull
    @Column(name = "id")
    private Integer id;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "slot")
    @Temporal(TemporalType.TIMESTAMP)
    private Date slot;
    
    @JoinColumn(name = "destination", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private City destination;
    
    @JoinColumn(name = "origin", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private City origin;
    
    @JoinColumn(name = "flight", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Flight flight;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "duration")
    private int duration;

    @Basic(optional = false)
    @NotNull
    @Column(name = "position")
    private int position;
    
    public Path() {
    }

    public Path(Integer id) {
        this.id = id;
    }
    
    public Date getSlot() {
        return slot;
    }

    public void setSlot(Date slot) {
        this.slot = slot;
    }

    public City getDestination() {
        return destination;
    }

    public void setDestination(City destination) {
        this.destination = destination;
    }

    public City getOrigin() {
        return origin;
    }

    public void setOrigin(City origin) {
        this.origin = origin;
    }

    public Flight getFlight() {
        return flight;
    }

    public void setFlight(Flight flight) {
        this.flight = flight;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    
    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }
    
    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Path)) {
            return false;
        }
        Path other = (Path) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.rsanchez.flyingfar.domain.Path[ id=" + id + " ]";
    }

}
