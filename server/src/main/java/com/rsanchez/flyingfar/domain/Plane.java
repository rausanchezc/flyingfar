/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rsanchez.flyingfar.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.codehaus.jackson.annotate.JsonIgnore;

/**
 *
 * @author rsanchez
 */
@Entity
@Table(name = "plane")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Plane.findAll", query = "SELECT p FROM Plane p"),
    @NamedQuery(name = "Plane.findById", query = "SELECT p FROM Plane p WHERE p.id = :id"),
    @NamedQuery(name = "Plane.findByManufacturer", query = "SELECT p FROM Plane p WHERE p.manufacturer = :manufacturer"),
    @NamedQuery(name = "Plane.findByModel", query = "SELECT p FROM Plane p WHERE p.model = :model"),
    @NamedQuery(name = "Plane.findByTotalSeats", query = "SELECT p FROM Plane p WHERE p.totalSeats = :totalSeats"),
    @NamedQuery(name = "Plane.findByManufactureDate", query = "SELECT p FROM Plane p WHERE p.manufactureDate = :manufactureDate"),
    @NamedQuery(name = "Plane.findByMaxBusinessSeats", query = "SELECT p FROM Plane p WHERE p.maxBusinessSeats = :maxBusinessSeats"),
    @NamedQuery(name = "Plane.findByMaxTouristSeats", query = "SELECT p FROM Plane p WHERE p.maxTouristSeats = :maxTouristSeats"),
    @NamedQuery(name = "Plane.findByMaxOfferSeats", query = "SELECT p FROM Plane p WHERE p.maxOfferSeats = :maxOfferSeats"),
    // Custom Queries
    @NamedQuery(name = "Plane.findByAirline", query = "SELECT p FROM Plane p, Airline a WHERE p.airline.id = a.id AND a.id = :airlineId")
})

public class Plane implements Serializable 
{
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
   
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 80)
    @Column(name = "manufacturer")
    private String manufacturer;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "model")
    private String model;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "totalSeats")
    private int totalSeats;
    
    @Column(name = "manufactureDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date manufactureDate;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "maxBusinessSeats")
    private int maxBusinessSeats;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "maxTouristSeats")
    private int maxTouristSeats;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "maxOfferSeats")
    private int maxOfferSeats;
    
    @JoinColumn(name = "airline", referencedColumnName = "id")
    @ManyToOne
    private Airline airline;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "plane")
    private List<Flight> flightList;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "creationDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date creationDate;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "lastModificationDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastModificationDate;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "plane")
    private List<Seat> seatList;

    public Plane() {
    }

    public Plane(Integer id) {
        this.id = id;
    }

    public Plane(Integer id, String manufacturer, String model, int totalSeats, int maxBusinessSeats, int maxTouristSeats, int maxOfferSeats) {
        this.id = id;
        this.manufacturer = manufacturer;
        this.model = model;
        this.totalSeats = totalSeats;
        this.maxBusinessSeats = maxBusinessSeats;
        this.maxTouristSeats = maxTouristSeats;
        this.maxOfferSeats = maxOfferSeats;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getTotalSeats() {
        return totalSeats;
    }

    public void setTotalSeats(int totalSeats) {
        this.totalSeats = totalSeats;
    }

    public Date getManufactureDate() {
        return manufactureDate;
    }

    public void setManufactureDate(Date manufactureDate) {
        this.manufactureDate = manufactureDate;
    }

    public int getMaxBusinessSeats() {
        return maxBusinessSeats;
    }

    public void setMaxBusinessSeats(int maxBusinessSeats) {
        this.maxBusinessSeats = maxBusinessSeats;
    }

    public int getMaxTouristSeats() {
        return maxTouristSeats;
    }

    public void setMaxTouristSeats(int maxTouristSeats) {
        this.maxTouristSeats = maxTouristSeats;
    }

    public int getMaxOfferSeats() {
        return maxOfferSeats;
    }

    public void setMaxOfferSeats(int maxOfferSeats) {
        this.maxOfferSeats = maxOfferSeats;
    }

    public Airline getAirline() {
        return airline;
    }

    public void setAirline(Airline airline) {
        this.airline = airline;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getLastModificationDate() {
        return lastModificationDate;
    }

    public void setLastModificationDate(Date lastModificationDate) {
        this.lastModificationDate = lastModificationDate;
    }
    
    @XmlTransient
    @JsonIgnore
    public List<Seat> getSeatList() {
        return seatList;
    }

    public void setSeatList(List<Seat> seatList) {
        this.seatList = seatList;
    }

    @XmlTransient
    @JsonIgnore
    public List<Flight> getFlightList() {
        return flightList;
    }

    public void setFlightList(List<Flight> flightList) {
        this.flightList = flightList;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Plane)) {
            return false;
        }
        Plane other = (Plane) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.rsanchez.flyingfar.domain.Plane[ id=" + id + " ]";
    }
}
