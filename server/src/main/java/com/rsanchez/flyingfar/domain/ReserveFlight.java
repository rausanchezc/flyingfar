/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.rsanchez.flyingfar.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.codehaus.jackson.annotate.JsonIgnore;

/**
 *
 * @author rsanchez
 */
@Entity
@Table(name = "reserve_flight")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ReserveFlight.findAll", query = "SELECT r FROM ReserveFlight r"),
    @NamedQuery(name = "ReserveFlight.findByReserveDate", query = "SELECT r FROM ReserveFlight r WHERE r.reserveDate = :reserveDate"),
    @NamedQuery(name = "ReserveFlight.findByNumSeats", query = "SELECT r FROM ReserveFlight r WHERE r.numSeats = :numSeats"),
    @NamedQuery(name = "ReserveFlight.findByIsPaid", query = "SELECT r FROM ReserveFlight r WHERE r.isPaid = :isPaid"),
    // Custom Queries
    @NamedQuery(name = "ReserveFlight.countReservesByUserId", query="SELECT r FROM ReserveFlight r  WHERE r.user.id = :userId"),
    @NamedQuery(name = "ReserveFlight.findActivesByFlightId", query="SELECT r FROM ReserveFlight r  WHERE r.flight.id = :flightId"),
    @NamedQuery(name = "ReserveFlight.findAllByUserId", query="SELECT r FROM ReserveFlight r WHERE  r.canceled is null AND  r.user.id = :userId"),
    @NamedQuery(name = "ReserveFlight.findAllByAirlineCode", query = "SELECT r FROM ReserveFlight r  WHERE r.canceled is null AND r.flight.departure > CURRENT_TIMESTAMP AND r.flight.airline.code = :airlineCode")
})

public class ReserveFlight implements Serializable 
{
    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "reserveDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date reserveDate;
    
    @Column(name = "numSeats")
    private Integer numSeats;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "isPaid")
    private boolean isPaid;
    
    @Column(name = "canceled")
    private Boolean canceled;
    
    @Column(name = "pending")
    private Boolean pending;
    
    @JoinTable(name = "seat_into_reserve_flight", joinColumns = {
        @JoinColumn(name = "reserveFlight", referencedColumnName = "id")}, inverseJoinColumns = {
        @JoinColumn(name = "seat", referencedColumnName = "id")})
    @ManyToMany
    private List<Seat> seatList;
    
    @JoinColumn(name = "flight", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Flight flight;
    
    @JoinColumn(name = "user", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private User user;

    public ReserveFlight() {
    }
    
    public ReserveFlight(Integer id) {
        this.id = id;
    }

    public Date getReserveDate() {
        return reserveDate;
    }

    public void setReserveDate(Date reserveDate) {
        this.reserveDate = reserveDate;
    }

    public Integer getNumSeats() {
        return numSeats;
    }

    public void setNumSeats(Integer numSeats) {
        this.numSeats = numSeats;
    }

    public boolean getIsPaid() {
        return isPaid;
    }

    public void setIsPaid(boolean isPaid) {
        this.isPaid = isPaid;
    }

    @XmlTransient
    @JsonIgnore
    public List<Seat> getSeatList() {
        return seatList;
    }

    public void setSeatList(List<Seat> seatList) {
        this.seatList = seatList;
    }

    public Flight getFlight() {
        return flight;
    }

    public void setFlight(Flight flight) {
        this.flight = flight;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    
    public Boolean getCanceled() {
        return canceled;
    }

    public void setCanceled(Boolean canceled) {
        this.canceled = canceled;
    }

    public Boolean getPending() {
        return pending;
    }

    public void setPending(Boolean pending) {
        this.pending = pending;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ReserveFlight)) {
            return false;
        }
        ReserveFlight other = (ReserveFlight) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.rsanchez.flyingfar.domain.ReserveFlight[ id=" + id + " ]";
    }

    }
