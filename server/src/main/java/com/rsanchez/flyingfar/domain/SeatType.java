/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.rsanchez.flyingfar.domain;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.codehaus.jackson.annotate.JsonIgnore;

/**
 *
 * @author rsanchez
 */
@Entity
@Table(name = "seat_type")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SeatType.findAll", query = "SELECT s FROM SeatType s"),
    @NamedQuery(name = "SeatType.findById", query = "SELECT s FROM SeatType s WHERE s.id = :id"),
    @NamedQuery(name = "SeatType.findByCategory", query = "SELECT s FROM SeatType s WHERE s.category = :category")
})

public class SeatType implements Serializable 
{

    private static final long serialVersionUID = 1L;

    public static final String BUSINESS = "BUSINESS";
    
    public static final String TOURIST = "TOURIST";
    
    public static final String OFFER = "OFFER";
    
    public static final String LAST_MINUTE = "LAST_MINUTE";
    
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id")
    private Integer id;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "category")
    private String category;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "seatType")
    private List<Seat> seatList;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "seatType")
    private List<SeatTypeIntoFlight> seatTypeIntoFlightList;
    
    public SeatType() {
    }

    public SeatType(Integer id) {
        this.id = id;
    }

    public SeatType(Integer id, String category) {
        this.id = id;
        this.category = category;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    @XmlTransient
    @JsonIgnore
    public List<SeatTypeIntoFlight> getSeatTypeIntoFlightList() {
        return seatTypeIntoFlightList;
    }

    public void setSeatTypeIntoFlightList(List<SeatTypeIntoFlight> seatTypeIntoFlightList) {
        this.seatTypeIntoFlightList = seatTypeIntoFlightList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SeatType)) {
            return false;
        }
        SeatType other = (SeatType) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.rsanchez.flyingfar.domain.SeatType[ id=" + id + " ]";
    }

    @XmlTransient
    @JsonIgnore
    public List<Seat> getSeatList() {
        return seatList;
    }

    public void setSeatList(List<Seat> seatList) {
        this.seatList = seatList;
    }
    
}
