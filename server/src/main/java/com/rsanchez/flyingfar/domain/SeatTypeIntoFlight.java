/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.rsanchez.flyingfar.domain;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author rsanchez
 */
@Entity
@Table(name = "seat_type_into_flight")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SeatTypeIntoFlight.findAll", query = "SELECT s FROM SeatTypeIntoFlight s"),
    @NamedQuery(name = "SeatTypeIntoFlight.findBySeatType", query = "SELECT s FROM SeatTypeIntoFlight s WHERE s.seatTypeIntoFlightPK.seatType = :seatType"),
    @NamedQuery(name = "SeatTypeIntoFlight.findByFlight", query = "SELECT s FROM SeatTypeIntoFlight s WHERE s.seatTypeIntoFlightPK.flight = :flight"),
    @NamedQuery(name = "SeatTypeIntoFlight.findByFare", query = "SELECT s FROM SeatTypeIntoFlight s WHERE s.fare = :fare")
})

public class SeatTypeIntoFlight implements Serializable 
{
    private static final long serialVersionUID = 1L;
    
    @EmbeddedId
    protected SeatTypeIntoFlightPK seatTypeIntoFlightPK;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "fare")
    private double fare;
    
    @JoinColumn(name = "flight", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Flight flight;
    
    @JoinColumn(name = "seatType", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private SeatType seatType;

    public SeatTypeIntoFlight() {
    }

    public SeatTypeIntoFlight(SeatTypeIntoFlightPK seatTypeIntoFlightPK) {
        this.seatTypeIntoFlightPK = seatTypeIntoFlightPK;
    }

    public SeatTypeIntoFlight(SeatTypeIntoFlightPK seatTypeIntoFlightPK, double fare) {
        this.seatTypeIntoFlightPK = seatTypeIntoFlightPK;
        this.fare = fare;
    }

    public SeatTypeIntoFlight(int seatType, int flight) {
        this.seatTypeIntoFlightPK = new SeatTypeIntoFlightPK(seatType, flight);
    }

    public SeatTypeIntoFlightPK getSeatTypeIntoFlightPK() {
        return seatTypeIntoFlightPK;
    }

    public void setSeatTypeIntoFlightPK(SeatTypeIntoFlightPK seatTypeIntoFlightPK) {
        this.seatTypeIntoFlightPK = seatTypeIntoFlightPK;
    }

    public double getFare() {
        return fare;
    }

    public void setFare(double fare) {
        this.fare = fare;
    }

    public Flight getFlight() {
        return flight;
    }

    public void setFlight(Flight flight) {
        this.flight = flight;
    }

    public SeatType getSeatType() {
        return seatType;
    }

    public void setSeatType(SeatType seatType) {
        this.seatType = seatType;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (seatTypeIntoFlightPK != null ? seatTypeIntoFlightPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SeatTypeIntoFlight)) {
            return false;
        }
        SeatTypeIntoFlight other = (SeatTypeIntoFlight) object;
        if ((this.seatTypeIntoFlightPK == null && other.seatTypeIntoFlightPK != null) || (this.seatTypeIntoFlightPK != null && !this.seatTypeIntoFlightPK.equals(other.seatTypeIntoFlightPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.rsanchez.flyingfar.domain.SeatTypeIntoFlight[ seatTypeIntoFlightPK=" + seatTypeIntoFlightPK + " ]";
    }
    
}
