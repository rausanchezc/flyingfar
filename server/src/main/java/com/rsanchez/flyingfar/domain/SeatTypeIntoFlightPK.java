/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.rsanchez.flyingfar.domain;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author rsanchez
 */
@Embeddable
public class SeatTypeIntoFlightPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "seatType")
    private int seatType;
    @Basic(optional = false)
    @NotNull
    @Column(name = "flight")
    private int flight;

    public SeatTypeIntoFlightPK() {
    }

    public SeatTypeIntoFlightPK(int seatType, int flight) {
        this.seatType = seatType;
        this.flight = flight;
    }

    public int getSeatType() {
        return seatType;
    }

    public void setSeatType(int seatType) {
        this.seatType = seatType;
    }

    public int getFlight() {
        return flight;
    }

    public void setFlight(int flight) {
        this.flight = flight;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) seatType;
        hash += (int) flight;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SeatTypeIntoFlightPK)) {
            return false;
        }
        SeatTypeIntoFlightPK other = (SeatTypeIntoFlightPK) object;
        if (this.seatType != other.seatType) {
            return false;
        }
        if (this.flight != other.flight) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.rsanchez.flyingfar.domain.SeatTypeIntoFlightPK[ seatType=" + seatType + ", flight=" + flight + " ]";
    }
    
}
