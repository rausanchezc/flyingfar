package com.rsanchez.flyingfar.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 */

public class AirlineDto 
{
    private Integer id;
    
    @NotNull
    @Size(min = 1, max = 4)
    private String code;
    
    @Size(max = 60)
    private String fullName;
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    @Override
    public String toString() {
        return "com.rsanchez.flyingfar.dto.AirlineDto{" + "code=" + code + ", fullName=" + fullName + '}';
    }
    
    
    
}
