package com.rsanchez.flyingfar.dto;

import java.util.List;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 */

public class AirportDto 
{
    private Integer id;
    
    @NotNull
    @Size(min = 1, max = 45)
    private String code;

    @Size(max = 120)
    private String name;
    
    @Valid
    private List<Integer> airlineList; 
       
    @NotNull
    private Integer cityId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    
    public String getCode() 
    {
        return code;
    }

    public void setCode(String code) 
    {
        this.code = code;
    }

    public String getName() 
    {
        return name;
    }

    public void setName(String name) 
    {
        this.name = name;
    }

    public Integer getCityId() {
        return cityId;
    }

    public void setCityId(Integer cityId) {
        this.cityId = cityId;
    }

    public List<Integer> getAirlineList() {
        return airlineList;
    }

    public void setAirlineList(List<Integer> airlineList) {
        this.airlineList = airlineList;
    }

    @Override
    public String toString() {
        return "com.rsanchez.flyingfar.dto.AirportDto{" + "code=" + code + ", name=" + name + ", airlineList=" + airlineList + ", cityId=" + cityId + '}';
    }
    
    
    
}
