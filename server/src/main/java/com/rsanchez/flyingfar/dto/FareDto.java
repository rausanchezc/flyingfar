package com.rsanchez.flyingfar.dto;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 */

public class FareDto 
{
    /* ****************** Attributes ***************************** */
    
    @Min(value = 1)
    private Integer id;
    
    @NotNull
    private String type;
    
    @NotNull
    @Min( value = 1)
    private Double value;

    /* ***************** Getters and Setters ******************** */
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "com.rsanchez.flyingfar.dto.FareDto{" + "type=" + type + ", value=" + value + '}';
    }

    
}
