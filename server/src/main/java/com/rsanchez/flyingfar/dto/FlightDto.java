package com.rsanchez.flyingfar.dto;

import java.util.Date;
import java.util.List;
import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import org.springframework.format.annotation.DateTimeFormat;

/**
 */

public class FlightDto 
{
    /* ********************** Attributes ********************** */
    
    private Integer id;
    
    @Min(value = 1)
    @NotNull
    private Integer airlineId;
    
    private String code;
    
    @Min(value = 1)
    @NotNull
    private Integer origin;
    
    @Min(value = 1)
    @NotNull
    private Integer destination;
    
    @NotNull
    private Integer plane;
    
    @NotNull Boolean roundTrip;
    
    @NotNull
    private List<FareDto> fareList;
    
    @NotNull
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private Date departure;
    
    @NotNull
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private Date arrival;

    @NotNull
    @Valid
    List<PathDto> pathList;
    
    /* **************** Getters & Setters ********************* */
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAirlineId() {
        return airlineId;
    }

    public void setAirlineId(Integer airlineId) {
        this.airlineId = airlineId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Integer getOrigin() {
        return origin;
    }

    public void setOrigin(Integer origin) {
        this.origin = origin;
    }

    public Integer getDestination() {
        return destination;
    }

    public void setDestination(Integer destination) {
        this.destination = destination;
    }

    public List<FareDto> getFareList() {
        return fareList;
    }

    public void setFareList(List<FareDto> fareList) {
        this.fareList = fareList;
    }

    public Date getDeparture() {
        return departure;
    }

    public void setDeparture(Date departure) {
        this.departure = departure;
    }

    public Date getArrival() {
        return arrival;
    }

    public void setArrival(Date arrival) {
        this.arrival = arrival;
    }

    public List<PathDto> getPathList() {
        return pathList;
    }

    public void setPathList(List<PathDto> pathList) {
        this.pathList = pathList;
    }

    public Integer getPlane() {
        return plane;
    }

    public void setPlane(Integer plane) {
        this.plane = plane;
    }

    public Boolean isRoundTrip() {
        return roundTrip;
    }

    public void setRoundTrip(Boolean roundTrip) {
        this.roundTrip = roundTrip;
    }
    
    @Override
    public String toString() 
    {
        return "com.rsanchez.flyingfar.dto.FlightDto{" + "id=" + id + '}';
    }
    

}
