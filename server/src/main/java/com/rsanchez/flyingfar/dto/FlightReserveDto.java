package com.rsanchez.flyingfar.dto;

import java.util.List;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 *
 */

public class FlightReserveDto
{
    /* ********************** Attributes ********************** */

    @NotNull
    @Min(value = 1)
    private Integer userId;
    
    @NotNull
    @Min(value = 1)
    private Integer flightId;
    
    @NotNull
    private List<Integer> seatList;
    
    private Boolean paid;
    
    private Boolean canceled;
    
    private Boolean pending;
    
    private Integer oldReserveId; 
    
    /* **************** Getters & Setters ********************* */
    
    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getFlightId() {
        return flightId;
    }

    public void setFlightId(Integer flightId) {
        this.flightId = flightId;
    }

    public List<Integer> getSeatList() {
        return seatList;
    }

    public void setSeatList(List<Integer> seatList) {
        this.seatList = seatList;
    }

    public Boolean isPaid() {
        return paid;
    }

    public void setPaid(Boolean paid) {
        this.paid = paid;
    }

    public Boolean isCanceled() {
        return canceled;
    }

    public void setCanceled(Boolean canceled) {
        this.canceled = canceled;
    }

    public Boolean isPending() {
        return pending;
    }

    public void setPending(Boolean pending) {
        this.pending = pending;
    }

    public Integer getOldReserveId() {
        return oldReserveId;
    }

    public void setOldReserveId(Integer oldReserveId) {
        this.oldReserveId = oldReserveId;
    }
    

    @Override
    public String toString() {
        return "FlightReserveDto{" + "userId=" + userId + ", flightId=" + flightId + ", seatList=" + seatList + ", paid=" + paid + ", canceled=" + canceled + ", pending=" + pending + '}';
    }

    
}
