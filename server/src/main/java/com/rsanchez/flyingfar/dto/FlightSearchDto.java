package com.rsanchez.flyingfar.dto;

import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;

/**
 */

public class FlightSearchDto 
{
    /* ************************ Attributes ******************** */
    private Integer origin;
    
    private Integer destination;
    
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private Date departure;
    
    private Integer seats;
    
    private Boolean roundTrip;

    public Integer getOrigin() {
        return origin;
    }

    /* *************** Getters & Setters ******************* */
    
    public void setOrigin(Integer origin) {
        this.origin = origin;
    }

    public Integer getDestination() {
        return destination;
    }

    public void setDestination(Integer destination) {
        this.destination = destination;
    }

    public Date getDeparture() {
        return departure;
    }

    public void setDeparture(Date departure) {
        this.departure = departure;
    }

    public Integer getSeats() {
        return seats;
    }

    public void setSeats(Integer seats) {
        this.seats = seats;
    }

    public Boolean isRoundTrip() {
        return roundTrip;
    }

    public void setRoundTrip(Boolean roundTrip) {
        this.roundTrip = roundTrip;
    }

    @Override
    public String toString() {
        return "com.rsanchez.flyingfar.dto.FlightSearchDto{" + "origin=" + origin + ", destination=" + destination + ", departure=" + departure + ", seats=" + seats + ", roundTrip=" + roundTrip + '}';
    }
    
    
}
