package com.rsanchez.flyingfar.dto;

import java.util.Date;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import org.springframework.format.annotation.DateTimeFormat;

/**
 */

public class PathDto 
{
    /* ********************* Attributes ************************ */
    
    @Min(value = 1)
    private Integer id;
    
    @NotNull
    @Min(value = 1)
    private Integer position;
    
    @NotNull
    @Min(value = 1)
    private Integer origin;

    @NotNull
    @Min(value = 1)
    private Integer destination;
    
    @NotNull
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private Date slot;
    
    @NotNull
    @Min(value = 1)
    private Integer duration;
    
    /* ***************** Getters & Setters ********************** */
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getOrigin() {
        return origin;
    }

    public void setOrigin(Integer origin) {
        this.origin = origin;
    }

    public Integer getDestination() {
        return destination;
    }

    public void setDestination(Integer destination) {
        this.destination = destination;
    }

    public Date getSlot() {
        return slot;
    }

    public void setSlot(Date slot) {
        this.slot = slot;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public Integer getPosition() {
        return position;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }

    @Override
    public String toString() 
    {
        return "com.rsanchez.flyingfar.dto.PathDto{" + "id=" + id + ", origin=" + origin + ", destination=" + destination + ", slot=" + slot + ", duration=" + duration + '}';
    }
    

}
