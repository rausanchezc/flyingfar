package com.rsanchez.flyingfar.dto;

import java.util.Date;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.springframework.format.annotation.DateTimeFormat;

/**
 */

public class PlaneDto 
{
    private Integer id;

    @NotNull
    @Size(min = 1, max = 80)
    private String manufacturer;
    
    @NotNull
    @Size(min = 1, max = 45)
    private String model;
    
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private Date manufactureDate;
    
    @NotNull
    @Min(value = 1)
    @Max(value = 9999)
    private Integer maxBusinessSeats;
    
    @NotNull
    @Min(value = 1)
    @Max(value = 9999)
    private Integer maxTouristSeats;
    
    @NotNull
    @Min(value = 1)
    @Max(value = 9999)
    private Integer maxOfferSeats;
    
    @Min(value = 1)
    private Integer airlineId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Date getManufactureDate() {
        return manufactureDate;
    }

    public void setManufactureDate(Date manufactureDate) {
        this.manufactureDate = manufactureDate;
    }

    public Integer getMaxBusinessSeats() {
        return maxBusinessSeats;
    }

    public void setMaxBusinessSeats(Integer maxBusinessSeats) {
        this.maxBusinessSeats = maxBusinessSeats;
    }

    public Integer getMaxTouristSeats() {
        return maxTouristSeats;
    }

    public void setMaxTouristSeats(Integer maxTouristSeats) {
        this.maxTouristSeats = maxTouristSeats;
    }

    public Integer getMaxOfferSeats() {
        return maxOfferSeats;
    }

    public void setMaxOfferSeats(Integer maxOfferSeats) {
        this.maxOfferSeats = maxOfferSeats;
    }

    public Integer getAirlineId() {
        return airlineId;
    }

    public void setAirlineId(Integer airlineId) {
        this.airlineId = airlineId;
    }

    @Override
    public String toString() {
        return "PlaneDto{" + "id=" + id + ", manufacturer=" + manufacturer + ", model=" + model + ", manufactureDate=" + manufactureDate + ", maxBusinessSeats=" + maxBusinessSeats + ", maxTouristSeats=" + maxTouristSeats + ", maxOfferSeats=" + maxOfferSeats + ", airlineId=" + airlineId + '}';
    }
}