package com.rsanchez.flyingfar.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 */
public class UserDto {
    
    private Integer id;

    @NotNull
    @Size(min = 1, max = 48)
    private String username;
    
    @NotNull
    @Size(min = 1, max = 256)
    private String password;
    
    @Size(max = 60)
    private String name;
    
    @Size(max = 60)
    private String firstSurname;
    
    @Size(max = 60)
    private String secondSurname;
    
    @Size(max = 100)
    private String address;
    
    @NotNull
    @Size(max = 100)
    private String email;

    private Integer creditCard;
    
    @NotNull
    @Size(min = 1, max = 60)
    private String role;

    private Integer airlineId;
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFirstSurname() {
        return firstSurname;
    }

    public void setFirstSurname(String firstSurname) {
        this.firstSurname = firstSurname;
    }

    public String getSecondSurname() {
        return secondSurname;
    }

    public void setSecondSurname(String secondSurname) {
        this.secondSurname = secondSurname;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getCreditCard() {
        return creditCard;
    }

    public void setCreditCard(Integer creditCard) {
        this.creditCard = creditCard;
    }
    
    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public Integer getAirlineId() {
        return airlineId;
    }

    public void setAirlineId(Integer airlineId) {
        this.airlineId = airlineId;
    }


    @Override
    public int hashCode() {
        int hash = 3;
        hash = 13 * hash + (this.username != null ? this.username.hashCode() : 0);
        hash = 13 * hash + (this.email != null ? this.email.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final UserDto other = (UserDto) obj;
        if ((this.username == null) ? (other.username != null) : !this.username.equals(other.username)) {
            return false;
        }
        if ((this.email == null) ? (other.email != null) : !this.email.equals(other.email)) {
            return false;
        }
        return true;
    }

    
    @Override
    public String toString() 
    {
        return "com.rsanchez.flyingfar.dto.UserDto{username=" + username + ", password=" + password + ", name=" + name + ", firstSurname=" + firstSurname + ", secondSurname=" + secondSurname + ", address=" + address + ", email=" + email + ", creditCard=" + creditCard + ", role=" + role + '}';
    }

    
}
