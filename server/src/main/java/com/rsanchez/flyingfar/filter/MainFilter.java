package com.rsanchez.flyingfar.filter;

import java.io.IOException;
import java.util.Enumeration;
import java.util.Locale;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.i18n.CookieLocaleResolver;
import org.springframework.web.util.WebUtils;

public class MainFilter implements Filter
{
    private static final String[] AVAILABLE_LOCALES = new String[]
    {
        "en_US", "es_ES"
    };

    private static final String DEFAULT_LOCALE = "en_US";

    private static final String LOCALE_COOKIE_NAME = "language";

    private static final int SESSION_COOKIE_TIME = 1800;

    private static final int LOCALE_COOKIE_TIME = -1;

    private FilterConfig filterConfig = null;

    public MainFilter()
    {
    }

    @SuppressWarnings("unchecked")
    private void resolveLocale(HttpServletRequest request,
                               HttpServletResponse response) throws IOException,
                                                                    ServletException
    {
        Cookie cookie = WebUtils.getCookie(request, LOCALE_COOKIE_NAME);
        if (cookie == null)
        {
            Enumeration<Locale> locales = request.getLocales();
            String locale = DEFAULT_LOCALE;

            while (locales.hasMoreElements())
            {
                locale = locales.nextElement().toString();
                if (isAvailableLocale(locale))
                {
                    break;
                }
            }
            cookie = new Cookie(LOCALE_COOKIE_NAME, locale);
            cookie.setMaxAge(LOCALE_COOKIE_TIME);
            cookie.setSecure(false);
            cookie.setPath(request.getContextPath() + "/");
            response.addCookie(cookie);
            request.setAttribute(CookieLocaleResolver.LOCALE_REQUEST_ATTRIBUTE_NAME, StringUtils.parseLocaleString(locale));
        }
        else
        {
            if (!isAvailableLocale(cookie.getValue()))
            {
                cookie.setValue(DEFAULT_LOCALE);
            }
        }
    }

    private boolean isAvailableLocale(String locale)
    {
        for (String availableLocale : AVAILABLE_LOCALES)
        {
            if (locale.equals(availableLocale))
            {
                return true;
            }
        }
        return false;
    }

    private void copySessionCookie(HttpServletRequest request,
                                   HttpServletResponse response) throws IOException,
                                                                        ServletException
    {
        HttpSession session = request.getSession(false);

        if (session != null)
        {
            final Cookie cookie = new Cookie("JSESSIONID", session.getId());
            cookie.setMaxAge(SESSION_COOKIE_TIME);
            cookie.setSecure(false);
            cookie.setPath(request.getContextPath() + "/");
            response.addCookie(cookie);
        }
    }

    @Override
    public void doFilter(ServletRequest request,
                         ServletResponse response,
                         FilterChain chain) throws IOException,
                                                   ServletException
    {
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        HttpServletResponse httpServletResponse = (HttpServletResponse) response;

        request.setCharacterEncoding("UTF-8");
        resolveLocale(httpServletRequest, httpServletResponse);
        //copySessionCookie(httpServletRequest, httpServletResponse);

        if (httpServletRequest.getHeader("origin") != null
     )
        {
            httpServletResponse.setHeader("Access-Control-Allow-Origin", httpServletRequest.getHeader("origin"));
            httpServletResponse.setHeader("Access-Control-Allow-Credentials", "true");
            httpServletResponse.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
            httpServletResponse.setHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
        }

        try
        {
            chain.doFilter(request, response);
        }
        catch (Throwable t)
        {
            t.printStackTrace();
        }
    }

    public FilterConfig getFilterConfig()
    {
        return (filterConfig);
    }

    public void setFilterConfig(FilterConfig filterConfig)
    {
        this.filterConfig = filterConfig;
    }

    @Override
    public void destroy()
    {

    }

    @Override
    public void init(FilterConfig filterConfig)
    {
        this.filterConfig = filterConfig;
    }
}