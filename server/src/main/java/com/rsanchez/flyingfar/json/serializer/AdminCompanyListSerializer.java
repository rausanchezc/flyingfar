package com.rsanchez.flyingfar.json.serializer;

import com.rsanchez.flyingfar.domain.User;
import java.util.List;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;

/**
*/
public class AdminCompanyListSerializer implements JsonSerializer<List<User>>
{

    @Override
    public void serialize(ObjectNode objectNode, List<User> adminCompanyList) 
    {
        ArrayNode arrayNode = objectNode.putArray("adminCompanyList");
        
        UserSerializer userSerializer = new UserSerializer();
        
        for(User adminCompany : adminCompanyList)
        {
            userSerializer.serialize(arrayNode.addObject(), adminCompany);
        }
    }
    
}
