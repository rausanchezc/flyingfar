package com.rsanchez.flyingfar.json.serializer;

import com.rsanchez.flyingfar.domain.Airline;
import com.rsanchez.flyingfar.domain.Plane;
import java.util.List;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;

/**
*/
public class AirlineListSelectSerializer implements JsonSerializer<List<Airline>>
{

    @Override
    public void serialize(ObjectNode objectNode, List<Airline> airlineList) 
    {
        ArrayNode arrayNode = objectNode.putArray("airlineList");
        
        for(Airline airline : airlineList)
        {
            ObjectNode airlineNode = arrayNode.addObject();
            airlineNode.put("id", airline.getId());
            airlineNode.put("code", airline.getCode());
            airlineNode.put("fullName", airline.getFullName());
            airlineNode.put("creationDate", airline.getCreationDate().toString());
            airlineNode.put("lastModificationDate",airline.getLastModificationDate().toString());
        }
    }
    
}
