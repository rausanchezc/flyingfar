package com.rsanchez.flyingfar.json.serializer;

import com.rsanchez.flyingfar.domain.Airline;
import org.codehaus.jackson.node.ObjectNode;

/**
*/
public class AirlineSerializer implements JsonSerializer<Airline>
{

    @Override
    public void serialize(ObjectNode objectNode, Airline airline) 
    {
        objectNode.put("id", airline.getId());
        objectNode.put("code",airline.getCode());
        objectNode.put("lastFlightCode", airline.getLastFlightCode());
        objectNode.put("fullName",airline.getFullName());
        objectNode.put("creationDate", airline.getCreationDate().toString());
        objectNode.put("lastModificationDate",airline.getLastModificationDate().toString());
    }
    
}
