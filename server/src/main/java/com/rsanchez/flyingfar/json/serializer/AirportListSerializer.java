package com.rsanchez.flyingfar.json.serializer;

import com.rsanchez.flyingfar.domain.Airport;
import java.util.List;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;

/**
 */
public class AirportListSerializer implements JsonSerializer<List<Airport>>
{

    @Override
    public void serialize(ObjectNode airportListObjectNode, List<Airport> airportList) 
    {
        ArrayNode airportListNode = airportListObjectNode.putArray("airportList");
        
        AirportSerializer airportSerializer = new AirportSerializer();
        
        for(Airport airport: airportList)
        {
            airportSerializer.serialize(airportListNode.addObject(),airport);
        }
    }
    
}
