package com.rsanchez.flyingfar.json.serializer;

import com.rsanchez.flyingfar.domain.Airport;
import java.math.BigDecimal;
import org.codehaus.jackson.node.ObjectNode;

/**
*/

public class AirportSerializer implements JsonSerializer<Airport>
{
    @Override
    public void serialize(ObjectNode objectNode, Airport airport) 
    {
        objectNode.put("id", airport.getId());
        objectNode.put("code",airport.getCode());
        objectNode.put("name",airport.getName());
        objectNode.put("creationDate",airport.getCreationDate().toString());
        objectNode.put("lastModificationDate",airport.getLastModificationDate().toString());
        
        objectNode.put("cityId",airport.getCity().getId());
        objectNode.put("cityName",airport.getCity().getName());
        

    
    }
    
}
