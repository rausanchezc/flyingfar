package com.rsanchez.flyingfar.json.serializer;

import com.rsanchez.flyingfar.domain.City;
import java.util.List;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;

/**
 */
public class CityListSerializer implements JsonSerializer<List<City>>
{

    @Override
    public void serialize(ObjectNode cityListObjectNode, List<City> cityList) 
    {
        ArrayNode cityListNode = cityListObjectNode.putArray("cityList");
        
        CitySerializer citySerializer = new CitySerializer();
        
        for(City city: cityList)
        {
            citySerializer.serialize(cityListNode.addObject(),city);
        }
    }
    
}
