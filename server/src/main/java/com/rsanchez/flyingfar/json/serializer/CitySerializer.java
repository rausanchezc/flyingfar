package com.rsanchez.flyingfar.json.serializer;

import com.rsanchez.flyingfar.domain.City;
import java.math.BigDecimal;
import org.codehaus.jackson.node.ObjectNode;

/**
*/
public class CitySerializer implements JsonSerializer<City>
{

    @Override
    public void serialize(ObjectNode cityNode, City city) 
    {
        cityNode.put("id", city.getId());
        
        if(city.getCode() != null)
            cityNode.put("code", city.getCode());
        
        if(city.getName() != null)
            cityNode.put("name", city.getName());

        ObjectNode countryNode = cityNode.putObject("country");
        
        countryNode.put("id",city.getCountry().getId());
        countryNode.put("code",city.getCountry().getCode());
        
        if(city.getCountry().getName() != null)
            countryNode.put("name",city.getCountry().getName());
    }
       
    
}
