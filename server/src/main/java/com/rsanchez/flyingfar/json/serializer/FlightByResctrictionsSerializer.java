package com.rsanchez.flyingfar.json.serializer;

import com.rsanchez.flyingfar.domain.Flight;
import com.rsanchez.flyingfar.domain.Path;
import com.rsanchez.flyingfar.domain.ReserveFlight;
import com.rsanchez.flyingfar.domain.Seat;
import com.rsanchez.flyingfar.domain.SeatType;
import com.rsanchez.flyingfar.domain.SeatTypeIntoFlight;
import com.rsanchez.flyingfar.util.Util;
import java.math.BigDecimal;
import java.util.Date;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;

/**
*/
public class FlightByResctrictionsSerializer implements JsonSerializer<Flight>
{

    @Override
    public void serialize(ObjectNode flightNode, Flight flight) 
    {
        flightNode.put("id", flight.getId());
        flightNode.put("code", flight.getCode());
        flightNode.put("origin",flight.getOrigin().getId());
        flightNode.put("originName",flight.getOrigin().getName());
        flightNode.put("destination",flight.getDestination().getId());
        flightNode.put("destinationName",flight.getDestination().getName());
        flightNode.put("departure",flight.getDeparture().toString());
        flightNode.put("arrival",flight.getArrival().toString());
        flightNode.put("roundTrip", flight.getRoundTrip());
        
        ObjectNode planeNode = flightNode.putObject("plane");
        
        planeNode.put("id", flight.getPlane().getId());
        planeNode.put("model", flight.getPlane().getModel());
        planeNode.put("totalSeat", flight.getPlane().getTotalSeats());
        planeNode.put("maxBusinessSeat", flight.getPlane().getMaxBusinessSeats());
        planeNode.put("maxTouristSeat", flight.getPlane().getMaxTouristSeats());
        planeNode.put("maxOfferSeat", flight.getPlane().getMaxOfferSeats());
        
        ObjectNode airlineNode = flightNode.putObject("airline");
        
        airlineNode.put("name", flight.getAirline().getFullName());
        airlineNode.put("code", flight.getAirline().getCode());
                
        ArrayNode pathListNode = flightNode.putArray("pathList");
        
        for(Path path: flight.getPathList())
        {
            ObjectNode pathNode = pathListNode.addObject();
            pathNode.put("id", path.getId());
            pathNode.put("origin", path.getOrigin().getId());
            pathNode.put("originName", path.getOrigin().getName());
            pathNode.put("destination", path.getDestination().getId());
            pathNode.put("destinationName", path.getDestination().getName());

        }
        
        ArrayNode fareListNode = flightNode.putArray("fareList");
        
        for(SeatTypeIntoFlight fare: flight.getSeatTypeIntoFlightList())
        {
            if(fare.getSeatType() != null)
            {
                ObjectNode fareNode = fareListNode.addObject();
                
                String fareType = fare.getSeatType().getCategory();
                if(fareType.equals(SeatType.BUSINESS))
                {
                    fareNode.put("type", SeatType.BUSINESS);
                }
                else if(fareType.equals(SeatType.TOURIST))
                {
                    fareNode.put("type", SeatType.TOURIST);
                }
                else if(fareType.equals(SeatType.OFFER)) 
                { 
                    fareNode.put("type", SeatType.OFFER);
                }
                else // SeatType.LAST_MINUTE
                {
                    fareNode.put("type", SeatType.LAST_MINUTE);
                }
                fareNode.put("id", fare.getSeatType().getId());
                fareNode.put("value", fare.getFare());
            }
        }
        
        ObjectNode countReservesNode = flightNode.putObject("countReservedOrPaid");
        
        int numBusinessReserve = 0, numTouristReserve = 0, numOfferReserve = 0;
        
        Date prev24H = Util.subtractOneDay(new Date());
        
        for(ReserveFlight reserveFlight : flight.getReserveFlightList())
        {
            
            if(reserveFlight.getReserveDate().after(prev24H) || reserveFlight.getIsPaid())
            {
                for(Seat seat : reserveFlight.getSeatList())
                {
                    String seatType = seat.getSeatType().getCategory();

                    if(seatType.equals(SeatType.BUSINESS))
                    {
                        numBusinessReserve++;
                    }else if(seatType.equals(SeatType.TOURIST))
                    {
                        numTouristReserve++;
                    }else if(seatType.equals(SeatType.OFFER))
                    {
                        numOfferReserve++;
                    }
                }
            }
        }
        
        countReservesNode.put("total", (numBusinessReserve + numTouristReserve + numOfferReserve));
        countReservesNode.put("business", numBusinessReserve);
        countReservesNode.put("tourist", numTouristReserve);
        countReservesNode.put("offer", numOfferReserve);
    }
    
}
