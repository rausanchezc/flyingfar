package com.rsanchez.flyingfar.json.serializer;

import com.rsanchez.flyingfar.domain.Flight;
import com.rsanchez.flyingfar.domain.ReserveFlight;
import java.util.List;
import java.util.Map;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;

/**
 */

public class FlightListMyDashboardSerializer implements JsonSerializer<Map<String, List<ReserveFlight>>>
{

    @Override
    public void serialize(ObjectNode objectNode, Map<String,List<ReserveFlight>> flightMap)
    {
        FlightMyDashboardSerializer flightSerializer = new FlightMyDashboardSerializer();
        
        /*  *********** Pending Flights ******************* */
        
        List<ReserveFlight> pendingReserveList = flightMap.get("pendingFlights");
        
        ArrayNode pendingFlightListNode = objectNode.putArray("pendingFlights");
        
        for(ReserveFlight pendingReserve : pendingReserveList)
        {
            flightSerializer.serialize(pendingFlightListNode.addObject(), pendingReserve);
        }
        
        /*  *********** Reserved Flights ******************* */
        
        List<ReserveFlight> reservedReserveList = flightMap.get("reservedFlights");
        
        ArrayNode reservedFlightListNode = objectNode.putArray("reservedFlights");
        
        for(ReserveFlight reservedReserve : reservedReserveList)
        {
            flightSerializer.serialize(reservedFlightListNode.addObject(), reservedReserve);
        }
        
        /*  *********** Paid Flights *********************** */
        
        List<ReserveFlight> paidReserveList = flightMap.get("paidFlights");
        
        ArrayNode paidFlightListNode = objectNode.putArray("paidFlights");
        
        for(ReserveFlight paidReserve : paidReserveList)
        {
            flightSerializer.serialize(paidFlightListNode.addObject(), paidReserve);
        }  
        
    }
    
}
