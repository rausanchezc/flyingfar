package com.rsanchez.flyingfar.json.serializer;

import com.rsanchez.flyingfar.domain.Flight;
import java.util.List;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;

/**
 */

public class FlightListSerializer implements JsonSerializer<List<Flight>>
{

    @Override
    public void serialize(ObjectNode objectNode, List<Flight> flightList)
    {
        ArrayNode flightListNode = objectNode.putArray("flightList");
        
        FlightSerializer flightSerializer = new FlightSerializer();
        
        for(Flight flight : flightList)
        {
            flightSerializer.serialize(flightListNode.addObject(), flight);
        }
    }
    
}
