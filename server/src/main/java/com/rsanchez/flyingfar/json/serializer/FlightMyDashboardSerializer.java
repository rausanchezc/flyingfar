package com.rsanchez.flyingfar.json.serializer;

import com.rsanchez.flyingfar.domain.Flight;
import com.rsanchez.flyingfar.domain.ReserveFlight;
import com.rsanchez.flyingfar.domain.Seat;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;

/**
*/
public class FlightMyDashboardSerializer implements JsonSerializer<ReserveFlight>
{

    @Override
    public void serialize(ObjectNode flightNode, ReserveFlight reserveFlight) 
    {
        
        Flight flight = reserveFlight.getFlight();
        
        flightNode.put("flightId", flight.getId());
        flightNode.put("code", flight.getCode());
        flightNode.put("origin",flight.getOrigin().getId());
        flightNode.put("originName",flight.getOrigin().getName());
        flightNode.put("destination",flight.getDestination().getId());
        flightNode.put("destinationName",flight.getDestination().getName());
        flightNode.put("departure",flight.getDeparture().toString());
        flightNode.put("arrival",flight.getArrival().toString());
        flightNode.put("planeName", flight.getPlane().getModel());
        flightNode.put("roundTrip", flight.getRoundTrip());
        
        flightNode.put("numSeats", reserveFlight.getNumSeats());
        flightNode.put("reserveDate", reserveFlight.getReserveDate().toString());
        flightNode.put("reserveId", reserveFlight.getId());
        
        
        ArrayNode seatReservedListNode = flightNode.putArray("seatReservedList");
        
        for(Seat seat : reserveFlight.getSeatList())
        {
            ObjectNode seatNode = seatReservedListNode.addObject();
            
            seatNode.put("id", seat.getId());
            seatNode.put("number", seat.getSeatNumber());
            seatNode.put("type", seat.getSeatType().getCategory());
            seatNode.put("reserved", true);
        }
        
    }
    
}
