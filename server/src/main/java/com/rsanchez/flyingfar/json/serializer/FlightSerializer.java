package com.rsanchez.flyingfar.json.serializer;

import com.rsanchez.flyingfar.domain.Flight;
import com.rsanchez.flyingfar.domain.Path;
import com.rsanchez.flyingfar.domain.SeatType;
import com.rsanchez.flyingfar.domain.SeatTypeIntoFlight;
import java.math.BigDecimal;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;

/**
*/
public class FlightSerializer implements JsonSerializer<Flight>
{

    @Override
    public void serialize(ObjectNode flightNode, Flight flight) 
    {
        flightNode.put("id", flight.getId());
        flightNode.put("code", flight.getCode());
        flightNode.put("origin",flight.getOrigin().getId());
        flightNode.put("originName",flight.getOrigin().getName());
        flightNode.put("destination",flight.getDestination().getId());
        flightNode.put("destinationName",flight.getDestination().getName());
        flightNode.put("departure",flight.getDeparture().toString());
        flightNode.put("arrival",flight.getArrival().toString());
        flightNode.put("creationDate",flight.getCreationDate().toString());
        flightNode.put("lastModificationDate",flight.getLastModificationDAte().toString());
        flightNode.put("plane", flight.getPlane().getId());
        flightNode.put("planeName", flight.getPlane().getModel());
        flightNode.put("roundTrip", flight.getRoundTrip());
        
        ObjectNode airlineNode = flightNode.putObject("airline");
        
        airlineNode.put("name", flight.getAirline().getFullName());
        airlineNode.put("code", flight.getAirline().getCode());
                
        ArrayNode pathListNode = flightNode.putArray("pathList");
        
        for(Path path: flight.getPathList())
        {
            ObjectNode pathNode = pathListNode.addObject();
            pathNode.put("id", path.getId());
            pathNode.put("position", path.getPosition());
            pathNode.put("origin", path.getOrigin().getId());
            pathNode.put("originName", path.getOrigin().getName());
            pathNode.put("destination", path.getDestination().getId());
            pathNode.put("destinationName", path.getDestination().getName());
            pathNode.put("slot", path.getSlot().toString());
            pathNode.put("duration", path.getDuration());

        }
        
        ArrayNode fareListNode = flightNode.putArray("fareList");
        
        for(SeatTypeIntoFlight fare: flight.getSeatTypeIntoFlightList())
        {
            if(fare.getSeatType() != null)
            {
                ObjectNode fareNode = fareListNode.addObject();
                
                String fareType = fare.getSeatType().getCategory();
                if(fareType.equals(SeatType.BUSINESS))
                {
                    fareNode.put("type", SeatType.BUSINESS);
                }
                else if(fareType.equals(SeatType.TOURIST))
                {
                    fareNode.put("type", SeatType.TOURIST);
                }
                else if(fareType.equals(SeatType.OFFER)) 
                { 
                    fareNode.put("type", SeatType.OFFER);
                }
                else // SeatType.LAST_MINUTE
                {
                    fareNode.put("type", SeatType.LAST_MINUTE);
                }
                fareNode.put("id", fare.getSeatType().getId());
                fareNode.put("value", fare.getFare());
            }
        }        
    }
    
}
