package com.rsanchez.flyingfar.json.serializer;

import com.rsanchez.flyingfar.domain.Flight;
import com.rsanchez.flyingfar.domain.Path;
import com.rsanchez.flyingfar.domain.ReserveFlight;
import com.rsanchez.flyingfar.domain.Seat;
import com.rsanchez.flyingfar.domain.SeatTypeIntoFlight;
import com.rsanchez.flyingfar.util.Util;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;

/**
*/
public class FlightWithDetailsSerializer implements JsonSerializer<Flight>
{

    @Override
    public void serialize(ObjectNode flightNode, Flight flight) 
    {
        flightNode.put("id", flight.getId());
        flightNode.put("code", flight.getCode());
        flightNode.put("origin",flight.getOrigin().getId());
        flightNode.put("originName",flight.getOrigin().getName());
        flightNode.put("destination",flight.getDestination().getId());
        flightNode.put("destinationName",flight.getDestination().getName());
        flightNode.put("departure",flight.getDeparture().toString());
        flightNode.put("arrival",flight.getArrival().toString());
        
        ObjectNode planeNode = flightNode.putObject("plane");
        
        planeNode.put("id", flight.getPlane().getId());
        planeNode.put("model", flight.getPlane().getModel());
        planeNode.put("totalSeat", flight.getPlane().getTotalSeats());
        planeNode.put("maxBusinessSeat", flight.getPlane().getMaxBusinessSeats());
        planeNode.put("maxTouristSeat", flight.getPlane().getMaxTouristSeats());
        planeNode.put("maxOfferSeat", flight.getPlane().getMaxOfferSeats());
        
        flightNode.put("roundTrip", flight.getRoundTrip());
        
        ObjectNode airlineNode = flightNode.putObject("airline");
        
        airlineNode.put("name", flight.getAirline().getFullName());
        airlineNode.put("code", flight.getAirline().getCode());
                
        ArrayNode pathListNode = flightNode.putArray("pathList");
        
        for(Path path: flight.getPathList())
        {
            ObjectNode pathNode = pathListNode.addObject();
            pathNode.put("id", path.getId());
            pathNode.put("position", path.getPosition());
            pathNode.put("origin", path.getOrigin().getId());
            pathNode.put("originName", path.getOrigin().getName());
            pathNode.put("destination", path.getDestination().getId());
            pathNode.put("destinationName", path.getDestination().getName());
            pathNode.put("slot", path.getSlot().toString());
            pathNode.put("duration", path.getDuration());

        }
        
        ArrayNode fareListNode = flightNode.putArray("fareList");
        
        for(SeatTypeIntoFlight seatTypeIntoFlight : flight.getSeatTypeIntoFlightList())
        {
            ObjectNode fareNode = fareListNode.addObject();
            
            fareNode.put("id", seatTypeIntoFlight.getSeatType().getId());
            fareNode.put("type", seatTypeIntoFlight.getSeatType().getCategory());
            fareNode.put("value", seatTypeIntoFlight.getFare());
        }
        
        Set<Integer> idSeatReserveFlightSet = new HashSet<Integer>();
        Date oneDayBefore = Util.subtractOneDay(new Date());
        
        for(ReserveFlight reserveFlight : flight.getReserveFlightList())
        {
            if(reserveFlight.getReserveDate().after(oneDayBefore) || reserveFlight.getIsPaid())
            {
                for(Seat seat : reserveFlight.getSeatList())
                {
                    idSeatReserveFlightSet.add(seat.getId());
                }
            }
        }
        
        ArrayNode seatArrayNode = flightNode.putArray("seatList");
        
        for(Seat seat: flight.getPlane().getSeatList())
        {
            ObjectNode seatFlightNode = seatArrayNode.addObject();
            
            seatFlightNode.put("id", seat.getId());
            seatFlightNode.put("number", seat.getSeatNumber());
            seatFlightNode.put("type", seat.getSeatType().getCategory());
            if(idSeatReserveFlightSet.contains(seat.getId()))
            {
                seatFlightNode.put("reserved", true);
            }
        }
        
    }
    
}
