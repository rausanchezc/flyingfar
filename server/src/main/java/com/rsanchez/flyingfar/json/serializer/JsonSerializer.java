package com.rsanchez.flyingfar.json.serializer;

import org.codehaus.jackson.node.ObjectNode;


public interface JsonSerializer<T>
{
    void serialize(ObjectNode objectNode, T data);
}
