package com.rsanchez.flyingfar.json.serializer;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ObjectNode;

public class JsonUtil
{
    public static String serialize(Object object, JsonSerializer jsonSerializer)
    {
        ObjectNode objectNode = new ObjectMapper().createObjectNode();

        jsonSerializer.serialize(objectNode, object);

        return objectNode.toString();
    }
}
