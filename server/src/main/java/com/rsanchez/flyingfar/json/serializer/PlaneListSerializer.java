package com.rsanchez.flyingfar.json.serializer;

import com.rsanchez.flyingfar.domain.Plane;
import java.util.List;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;

/**
*/
public class PlaneListSerializer implements JsonSerializer<List<Plane>>
{

    @Override
    public void serialize(ObjectNode objectNode, List<Plane> planeList) 
    {
        ArrayNode arrayNode = objectNode.putArray("planeList");
        
        PlaneSerializer planeSerializer = new PlaneSerializer();
        
        for(Plane plane : planeList)
        {
            planeSerializer.serialize(arrayNode.addObject(), plane);
        }
    }
    
}
