package com.rsanchez.flyingfar.json.serializer;

import com.rsanchez.flyingfar.domain.Plane;
import org.codehaus.jackson.node.ObjectNode;

/**
*/
public class PlaneSerializer implements JsonSerializer<Plane>
{

    @Override
    public void serialize(ObjectNode objectNode, Plane plane) 
    {
        objectNode.put("id", plane.getId());
        objectNode.put("manufacturer",plane.getManufacturer());
        
        if(plane.getManufactureDate() != null)
            objectNode.put("manufactureDate", plane.getManufactureDate().toString()); 

        objectNode.put("creationDate",plane.getCreationDate().toString());
        objectNode.put("lastModificationDate",plane.getLastModificationDate().toString());
        objectNode.put("model",plane.getModel());
        objectNode.put("totalSeats",plane.getTotalSeats());
        objectNode.put("businessSeats",plane.getMaxBusinessSeats());
        objectNode.put("touristSeats",plane.getMaxTouristSeats());
        objectNode.put("offerSeats",plane.getMaxOfferSeats());
        
        if(plane.getAirline() != null){
            objectNode.put("airlineId",plane.getAirline().getId());
            objectNode.put("airlineCode",plane.getAirline().getCode());
            objectNode.put("airlineFullName",plane.getAirline().getFullName());
        }
        
        
    }
    
}
