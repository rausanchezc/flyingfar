package com.rsanchez.flyingfar.json.serializer;

import com.rsanchez.flyingfar.domain.ReserveFlight;
import java.util.List;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;

public class ReserveFlightListSerializer implements JsonSerializer<List<ReserveFlight>>{

    @Override
    public void serialize(ObjectNode objectNode, List<ReserveFlight> reserveFlightList) 
    {
        ReserveFlightSerializer reserveFlightSerializer = new ReserveFlightSerializer();
        
        ArrayNode reserveFlightListNode = objectNode.putArray("flightList");
        
        for(ReserveFlight reserveFlight : reserveFlightList)
        {
            reserveFlightSerializer.serialize(reserveFlightListNode.addObject(), reserveFlight);
        }
    }
}
