package com.rsanchez.flyingfar.json.serializer;

import com.rsanchez.flyingfar.spring.security.CustomUserDetails;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;


public class SessionSerializer implements JsonSerializer<Authentication>
{
    @Override
    public void serialize(ObjectNode objectNode,
                          Authentication authentication)
    {
        ArrayNode roleArray = objectNode.putArray("roleList");

        for (GrantedAuthority authority : authentication.getAuthorities())
        {
            roleArray.add(authority.getAuthority());
        }

        if (authentication.getDetails() != null)
        {
            //objectNode.put("token", ((CustomUserDetails) authentication.getPrincipal()).get("JSESSIONID").toString());
        }
        objectNode.put("expirationTime", 1800000);

        new UserSecuritySerializer().serialize(objectNode.putObject("user"), ((CustomUserDetails) authentication.getPrincipal()).getUser());
    }
}
