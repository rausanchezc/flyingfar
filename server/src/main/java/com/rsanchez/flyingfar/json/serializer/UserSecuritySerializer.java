package com.rsanchez.flyingfar.json.serializer;

import com.rsanchez.flyingfar.domain.User;
import org.codehaus.jackson.node.ObjectNode;

/**
 */

public class UserSecuritySerializer implements JsonSerializer<User>
{

    @Override
    public void serialize(ObjectNode objectNode, User user)
    {
        objectNode.put("id", user.getId());
        objectNode.put("username", user.getUsername());
        objectNode.put("name", user.getName());
        objectNode.put("surname", user.getFirstSurname()+" "+ user.getSecondSurname());
        objectNode.put("email", user.getEmail());
        if(user.getAirline() != null)
        {
            objectNode.put("airlineCode", user.getAirline().getCode());
        }
    }
}
