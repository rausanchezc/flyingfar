package com.rsanchez.flyingfar.json.serializer;

import com.rsanchez.flyingfar.domain.User;
import org.codehaus.jackson.node.ObjectNode;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;

/**
*/
public class UserSerializer implements JsonSerializer<User>
{

    @Override
    public void serialize(ObjectNode objectNode, User user) 
    {
        objectNode.put("id", user.getId());
        objectNode.put("username", user.getUsername());
        objectNode.put("password", user.getPassword()); 
        
        objectNode.put("creationDate", user.getCreationDate().toString());
        objectNode.put("lastModificationDate", user.getLastModificationDate().toString());
        
        if(user.getName() != null)
            objectNode.put("name", user.getName());
    
        if(user.getFirstSurname() != null)
            objectNode.put("firstSurname", user.getFirstSurname());
    
        if(user.getSecondSurname() != null)
            objectNode.put("secondSurname", user.getSecondSurname());
    
        if(user.getAddress() != null)
            objectNode.put("address", user.getAddress());
    
        objectNode.put("email", user.getEmail());

        if(user.getCreditCard() != null)
            objectNode.put("creditCard", user.getCreditCard());
        
        ObjectNode roleObject = objectNode.putObject("role");
        
        roleObject.put("id", user.getRole().getId());
        roleObject.put("label", user.getRole().getLabel());
        
        if(user.getAirline() != null){
            objectNode.put("airlineId", user.getAirline().getId());
            objectNode.put("airlineName", user.getAirline().getFullName());
            objectNode.put("airlineCode", user.getAirline().getCode());
        }
    }
    
}
