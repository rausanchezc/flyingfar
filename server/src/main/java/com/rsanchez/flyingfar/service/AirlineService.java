package com.rsanchez.flyingfar.service;

import com.rsanchez.flyingfar.domain.Airline;
import com.rsanchez.flyingfar.dto.AirlineDto;
import java.io.Serializable;
import java.util.List;

/**
 *
 */

public interface AirlineService extends Serializable
{
    public void create(Airline airline);

    public void update(Airline airline);
    
    public List<Airline> getAll();
    
    public Airline getById(Integer airlineId);

    public Airline getByUserId(Integer userId);
    
    public Airline getByCode(String airlineCode);
    
    public Airline create(AirlineDto airlineDto);
    
    public Airline update(Integer airlineId, AirlineDto airlineDto);
    
    public void delete(Integer airlineId);
}
