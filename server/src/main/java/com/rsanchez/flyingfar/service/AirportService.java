package com.rsanchez.flyingfar.service;

import com.rsanchez.flyingfar.domain.Airport;
import com.rsanchez.flyingfar.dto.AirportDto;
import java.io.Serializable;
import java.util.List;

/**
 *
 */

public interface AirportService extends Serializable
{
    void create(Airport airport);
    
    void update(Airport airport);
    
    List<Airport> getAll();
    
    public Airport getById(Integer airportId);
    
    public Airport getByAirportCode(String airportCode);
    
    public Airport create(AirportDto airportDto);

    public Airport update(Integer airportId, AirportDto airportDto);
    
    void delete(Integer airportId);
}
