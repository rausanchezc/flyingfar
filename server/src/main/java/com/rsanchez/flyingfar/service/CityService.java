package com.rsanchez.flyingfar.service;

import com.rsanchez.flyingfar.domain.City;
import java.io.Serializable;
import java.util.List;

/**
 *
 */

public interface CityService extends Serializable
{
    public List<City> getAll();
    
    public City getById(Integer cityId);
    
    public List<City> getByCountry(Integer countryId);
}
