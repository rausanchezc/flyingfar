package com.rsanchez.flyingfar.service;

import com.rsanchez.flyingfar.domain.Flight;
import com.rsanchez.flyingfar.domain.ReserveFlight;
import com.rsanchez.flyingfar.dto.FlightDto;
import com.rsanchez.flyingfar.dto.FlightReserveDto;
import com.rsanchez.flyingfar.dto.FlightSearchDto;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 *
 */

public interface FlightService extends Serializable
{
    public void create(Flight flight);

    public void update(Flight flight);
    
    public List<Flight> getAll();
    
    public List<Flight> getAllValid();
    
    public Flight getById(Integer flightId);
    
    public Flight getByIdWithDetails(Integer flightId);
    
    public Flight getByCode(String flightCode);
    
    public List<Flight> getByRestrictions(FlightSearchDto flightDto);
    
    public List<Flight> getByAirlineId(Integer airlineId);
    
    public Flight create(FlightDto flightDto);
    
    public Flight update(Integer flightId, FlightDto flightDto);
    
    public void delete(Integer flightId);
    
    public void reserve(FlightReserveDto flightReserveDto);
    
    public Map<String, List<Flight>> getAllByUserId(Integer userId);
    
    public Map<String, List<ReserveFlight> > getAllReservesFromUserId(Integer userId);
    
}
