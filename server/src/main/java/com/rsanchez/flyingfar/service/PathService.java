package com.rsanchez.flyingfar.service;

import com.rsanchez.flyingfar.domain.Flight;
import com.rsanchez.flyingfar.domain.Path;
import com.rsanchez.flyingfar.dto.PathDto;
import java.io.Serializable;
import java.util.List;

/**
 *
 */

public interface PathService extends Serializable
{
    public void create(Path path);

    public void update(Path path);
    
    public void delete(Path path);
    
    public List<Path> getAll();
    
    public Path getById(Integer pathId);
    
    public Path create(PathDto pathDto);
    
    public Path update(Integer pathId, PathDto pathDto);
    
    public void delete(Integer pathId);
}
