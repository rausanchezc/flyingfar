package com.rsanchez.flyingfar.service;

import com.rsanchez.flyingfar.domain.Plane;
import com.rsanchez.flyingfar.dto.PlaneDto;
import java.io.Serializable;
import java.util.List;

/**
 *
 */

public interface PlaneService extends Serializable
{
    public void create(Plane plane);

    public void update(Plane plane);
    
    public List<Plane> getAll();
    
    public Plane getById(Integer planeId);
    
    public List<Plane> getByModel(String planeModel);
    
    public List<Plane> getAllByAirline(Integer airlineId);
    
    public Plane create(PlaneDto planeDto);
    
    public Plane update(Integer planeId, PlaneDto planeDto);
    
    public void delete(Integer planeId);
}
