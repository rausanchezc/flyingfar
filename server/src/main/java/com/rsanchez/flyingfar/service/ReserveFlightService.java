package com.rsanchez.flyingfar.service;

import com.rsanchez.flyingfar.domain.ReserveFlight;
import com.rsanchez.flyingfar.dto.FlightReserveDto;
import java.io.Serializable;
import java.util.List;

/**
 *
 */

public interface ReserveFlightService extends Serializable
{
    public void create(ReserveFlight reserveFlight);

    public void update(ReserveFlight reserveFlight);
    
    public List<ReserveFlight> getAll();
    
    public ReserveFlight getById(Integer reserveFlightId);

    public List<ReserveFlight> getByUserId(Integer userId);
    
    public ReserveFlight getByCode(String reserveFlightCode);
    
    public void delete(Integer reserveFlightId);
    
    Integer countReserveFlightByUserId(Integer userId);
    
    public List<ReserveFlight> getOnlyActivesByFlightId(Integer flightId);
    
    public void pay(Integer reserveFlightId);
    
    public void modifySeatList(Integer reserveFlightId, List<Integer> seatList);
    
    public void replaceByNewReserve(Integer oldReserveId, FlightReserveDto flightReserveDto);
    
    public List<ReserveFlight> getWithSeatOccupationByAirlineCode(String airlineCode);
}
