package com.rsanchez.flyingfar.service;

import com.rsanchez.flyingfar.domain.Role;
import java.io.Serializable;
import java.util.List;

/**
 *
 */

public interface RoleService extends Serializable
{
    public void create(Role role);
    
    public void update(Role role);
    
    public void delete(Role role);
    
    public List<Role> getAll();
    
    public Role getById(Integer roleId);

    public Role getByName(String roleName);
}
