package com.rsanchez.flyingfar.service;

import com.rsanchez.flyingfar.domain.Seat;
import com.rsanchez.flyingfar.domain.SeatType;
import java.io.Serializable;
import java.util.List;

/**
 *
 */

public interface SeatService extends Serializable
{
    public void create(Seat seat);
    
    public List<SeatType> getAll();
    
    public Seat getById(Integer seatId);

    public SeatType getByCategory(String seatTypeCategoryt);
}
