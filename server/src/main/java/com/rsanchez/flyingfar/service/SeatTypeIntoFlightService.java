package com.rsanchez.flyingfar.service;

import com.rsanchez.flyingfar.domain.SeatTypeIntoFlight;
import java.io.Serializable;
import java.util.List;

/**
 *
 */

public interface SeatTypeIntoFlightService extends Serializable
{
    public List<SeatTypeIntoFlight> getAll();
    
//    public SeatType getById(Integer seatTypeId);
//
//    public SeatType getByCategory(String seatTypeCategory);
//    
//    public void update(SeatType seatType);
    
    public void delete(SeatTypeIntoFlight seatTypeIntoFlight);
}
