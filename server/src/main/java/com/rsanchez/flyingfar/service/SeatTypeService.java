package com.rsanchez.flyingfar.service;

import com.rsanchez.flyingfar.domain.SeatType;
import java.io.Serializable;
import java.util.List;

/**
 *
 */

public interface SeatTypeService extends Serializable
{
    public List<SeatType> getAll();
    
    public SeatType getById(Integer seatTypeId);

    public SeatType getByCategory(String seatTypeCategory);
    
    public void update(SeatType seatType);
}
