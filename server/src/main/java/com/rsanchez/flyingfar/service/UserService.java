package com.rsanchez.flyingfar.service;

import com.rsanchez.flyingfar.domain.User;
import com.rsanchez.flyingfar.dto.UserDto;
import java.io.Serializable;
import java.util.List;

/**
 *
 */

public interface UserService extends Serializable
{
    /** CRUD Operations **/
    
    public void create(User user);
    
    public void update(User user);
    
    public List<User> getAll();
    
    public User getById(Integer userId);
    
    public User create(UserDto userDto);
    
    public User update(Integer userId, UserDto userDto);

    public void delete(Integer userId);
    
    public List<User> getAdminCompanyList();
    
    public User getByUsername(String username);
    
    public boolean existsUsername(String username);
    
    public boolean existsEmail(String email);
}
