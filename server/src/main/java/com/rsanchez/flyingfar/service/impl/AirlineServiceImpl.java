package com.rsanchez.flyingfar.service.impl;

import com.rsanchez.flyingfar.dao.AirlineDao;
import com.rsanchez.flyingfar.domain.Airline;
import com.rsanchez.flyingfar.dto.AirlineDto;
import com.rsanchez.flyingfar.service.AirlineService;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 */
@Service("airlineService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class AirlineServiceImpl implements AirlineService{
    
    private static final long serialVersionUID = 1L;
    
    /**
     * ************ PRIVATE ATTRIBUTES (SERVICES, ETC) *****************
     */
    
    @Autowired
    private AirlineDao airlineDao;
    
    
    /**
     * ************ PRIVATE ATTRIBUTES (SERVICES, ETC) *****************
     * @param airline
     */
    
    @Override
    public void create(Airline airline) 
    {
        airlineDao.create(airline);
    }

    @Override
    public void update(Airline airline) 
    {
        airlineDao.update(airline);
    }

    @Override
    public List<Airline> getAll() 
    {
        return airlineDao.findAll();
    }
    
    @Override
    public Airline getById(Integer airlineId) 
    {
        return airlineDao.find(airlineId);
    }

    @Override
    public Airline getByCode(String airlineCode) 
    {
        return airlineDao.getByCode(airlineCode);
    }

    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    @Override
    public Airline create(AirlineDto airlineDto) 
    {
        Airline airline = new Airline();
        
        airline.setCode(airlineDto.getCode());
        airline.setFullName(airlineDto.getFullName());
        airline.setCreationDate(new Date());
        airline.setLastModificationDate(new Date());
        
        airlineDao.create(airline);
        
        return airline;
    }

    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    @Override
    public void delete(Integer airlineId) 
    {
        Airline airline = airlineDao.find(airlineId);
        
        if(airline != null)
        {
            airlineDao.delete(airline);
        }
        else
        {
            throw new EntityNotFoundException("Airline");
        }
    }

    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    @Override
    public Airline update(Integer airlineId, AirlineDto airlineDto) 
    {
        Airline airline = airlineDao.find(airlineId);
        
        if(airline != null)
        {
            airline.setCode(airlineDto.getCode());
            airline.setFullName(airlineDto.getFullName());
            airline.setLastModificationDate(new Date());
            
            airlineDao.update(airline);
        }
        else
        {
            throw new EntityNotFoundException("Airline");
        }
        
        return airline;
    }
    
    
    @Override
    public Airline getByUserId(Integer userId) 
    {
        Airline airline =  airlineDao.getByUserId(userId);
        
        System.out.println("Airline: " + airline);
        return airline;
    }
    
    
}
