package com.rsanchez.flyingfar.service.impl;

import com.rsanchez.flyingfar.dao.AirportDao;
import com.rsanchez.flyingfar.domain.Airline;
import com.rsanchez.flyingfar.domain.Airport;
import com.rsanchez.flyingfar.dto.AirportDto;
import com.rsanchez.flyingfar.service.AirlineService;
import com.rsanchez.flyingfar.service.AirportService;
import com.rsanchez.flyingfar.service.CityService;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 */

@Service("airportService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class AirportServiceImpl implements AirportService
{
    private static final long serialVersionUID = 1L;

    /**
     * ************ PRIVATE ATTRIBUTES (SERVICES, ETC) *****************
     */
    
    @Autowired
    private AirportDao airportDao;
    
    @Autowired
    private CityService cityService;
    
    @Autowired
    private AirlineService airlineService;
    
    /**
     * ************ PUBLIC METHODS (RESTFUL REQUEST HANDLERS) **********
     */

    /**
     * ************ PUBLIC METHODS (RESTFUL REQUEST HANDLERS) **********
     **/
    

    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    @Override
    public void create(Airport airport) 
    {
        airportDao.create(airport);
    }

    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    @Override
    public void update(Airport airport) 
    {
        airportDao.update(airport);
    }

    @Override
    public List<Airport> getAll()
    {
        return airportDao.findAll();
    }
    

    @Override
    public Airport getById(Integer airportId) 
    {
        return airportDao.find(airportId);
    }

    @Override
    public Airport getByAirportCode(String airportCode)
    {
        return airportDao.getByAirportCode(airportCode);
    }

    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    @Override
    public Airport create(AirportDto airportDto) 
    {
        Airport airport = new Airport();
        
        airport.setName(airportDto.getName());
        airport.setCode(airportDto.getCode());
        airport.setCity(cityService.getById(airportDto.getCityId()));
        airport.setCreationDate(new Date());
        airport.setLastModificationDate(new Date());
        
        airportDao.create(airport);
        
        if(airportDto.getAirlineList()!=null && !airportDto.getAirlineList().isEmpty())
        {
            airport.setAirlineList(new ArrayList<Airline>());
            
            for(Integer airlineId: airportDto.getAirlineList())
            {
                Airline airline = airlineService.getById(airlineId);

                if(airline != null)
                {
                    airport.getAirlineList().add(airline);
                    airline.getAirportList().add(airport);
                    airlineService.update(airline);
                }
            }
        }

        airportDao.update(airport);
        
        return airport;
    }
    
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    @Override
    public Airport update(Integer airportId, AirportDto airportDto)
    {
        Airport airport = airportDao.find(airportId);
        
        if(airport != null)
        {
            airport.setName(airportDto.getName());
            airport.setCode(airportDto.getCode());
            airport.setLastModificationDate(new Date());
            airport.setCity(cityService.getById(airportDto.getCityId()));
            
            if(airportDto.getAirlineList()!=null && !airportDto.getAirlineList().isEmpty())
            {
                airport.setAirlineList(new ArrayList<Airline>());

                for(Integer airlineId: airportDto.getAirlineList())
                {
                    Airline airline = airlineService.getById(airlineId);

                    if(airline != null)
                    {
                        airport.getAirlineList().add(airline);
                        airline.getAirportList().add(airport);
                        airlineService.update(airline);
                    }
                }
            }
            airportDao.update(airport);
        }
        else
        {
            throw new EntityNotFoundException("Airport");
        }

        return airport;
    } 
    
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    @Override
    public void delete(Integer airportId)
    {
        Airport airport = airportDao.find(airportId);
        
        if(airport != null)
        {
            airportDao.delete(airport);
        }
        else
        {
            throw new EntityNotFoundException("Airport");
        }
    }

}
