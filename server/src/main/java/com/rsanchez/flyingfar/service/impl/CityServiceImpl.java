package com.rsanchez.flyingfar.service.impl;

import com.rsanchez.flyingfar.dao.CityDao;
import com.rsanchez.flyingfar.domain.City;
import com.rsanchez.flyingfar.service.CityService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 */

@Service("cityService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class CityServiceImpl implements CityService
{
    private static final long serialVersionUID = 1L;

    /**
     * ************ PRIVATE ATTRIBUTES (SERVICES, ETC) *****************
     */
    
    @Autowired
    private CityDao cityDao;
    
    /**
     * ************ PRIVATE ATTRIBUTES (SERVICES, ETC) *****************
     */

    @Override
    public List<City> getAll() 
    {
        return cityDao.findAll();
    }

    @Override
    public City getById(Integer cityId) 
    {
        return cityDao.find(cityId);
    }

    @Override
    public List<City> getByCountry(Integer countryId) 
    {
        //TODO getByCountry(Integer countryId)
        return null;
    }

}
