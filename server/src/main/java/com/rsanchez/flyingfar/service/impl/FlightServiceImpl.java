package com.rsanchez.flyingfar.service.impl;

import com.rsanchez.flyingfar.dao.FlightDao;
import com.rsanchez.flyingfar.domain.Airline;
import com.rsanchez.flyingfar.domain.Flight;
import com.rsanchez.flyingfar.domain.Path;
import com.rsanchez.flyingfar.domain.ReserveFlight;
import com.rsanchez.flyingfar.domain.Seat;
import com.rsanchez.flyingfar.domain.SeatType;
import com.rsanchez.flyingfar.domain.SeatTypeIntoFlight;
import com.rsanchez.flyingfar.dto.FareDto;
import com.rsanchez.flyingfar.dto.FlightDto;
import com.rsanchez.flyingfar.dto.FlightReserveDto;
import com.rsanchez.flyingfar.dto.FlightSearchDto;
import com.rsanchez.flyingfar.dto.PathDto;
import com.rsanchez.flyingfar.service.AirlineService;
import com.rsanchez.flyingfar.service.CityService;
import com.rsanchez.flyingfar.service.FlightService;
import com.rsanchez.flyingfar.service.PathService;
import com.rsanchez.flyingfar.service.PlaneService;
import com.rsanchez.flyingfar.service.ReserveFlightService;
import com.rsanchez.flyingfar.service.SeatService;
import com.rsanchez.flyingfar.service.SeatTypeIntoFlightService;
import com.rsanchez.flyingfar.service.SeatTypeService;
import com.rsanchez.flyingfar.service.UserService;
import com.rsanchez.flyingfar.util.Util;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 */
@Service("flightService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class FlightServiceImpl implements FlightService{
    
    private static final long serialVersionUID = 1L;
    
    
     /* ************ PRIVATE ATTRIBUTES (SERVICES, ETC) **************** */
    
    @Autowired
    private FlightDao flightDao;
    
    @Autowired
    private CityService cityService;
    
    @Autowired
    private SeatTypeService seatTypeService;
    
    @Autowired
    private SeatTypeIntoFlightService seatTypeIntoFlightService;
    
    @Autowired
    private PathService pathService;
    
    @Autowired
    private AirlineService airlineService;
    
    @Autowired
    private PlaneService planeService;
    
    @Autowired
    private UserService userService;
    
    @Autowired
    private SeatService seatService;
    
    @Autowired
    private ReserveFlightService reserveFlightService;
    
    /* ************ PRIVATE ATTRIBUTES (SERVICES, ETC) **************** */
    
    
    /**
     * 
     * @param flight 
     */
    @Override
    public void create(Flight flight) 
    {
        flightDao.create(flight);
    }

    @Override
    public void update(Flight flight) 
    {
        flightDao.update(flight);
    }

    @Override
    public List<Flight> getAll() 
    {
        List<Flight> flightList = flightDao.findAll();
        
        for(Flight flight : flightList)
        {
            flight.getPathList().size();
            flight.getSeatTypeIntoFlightList().size();
        }
        
        return flightList;
    }

    @Override
    public List<Flight> getAllValid()
    {
        List<Flight> flightList = flightDao.getValidFlightList();
        
        for(Flight flight: flightList)
        {
            flight.getPathList().size();
            flight.getSeatTypeIntoFlightList().size();
        }
        
        return flightList;
    }
    


    @Override
    public Flight getById(Integer flightId) 
    {
        return flightDao.find(flightId);
    }

    @Override
    public Flight getByIdWithDetails(Integer flightId) 
    {
        Flight flight = flightDao.find(flightId);

        flight.getPathList().size();

        flight.getPlane().getSeatList().size();
        
        flight.getSeatTypeIntoFlightList().size();
        
        for(ReserveFlight reserveFlight :  flight.getReserveFlightList())
        {
            reserveFlight.getSeatList().size();
        }
        
        return flight;
    }
    
    @Override
    public Flight getByCode(String flightCode) 
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    
    /**
     * @param flightDto
     * @return 
     */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    @Override
    public Flight create(FlightDto flightDto) 
    {
        Flight flight = new Flight();
        Airline airline = airlineService.getById(flightDto.getAirlineId());
        Integer lastFlightCode = airline.getLastFlightCode();
        lastFlightCode ++;
        airline.setLastFlightCode(lastFlightCode);
        
        flight.setCode(airline.getCode() + lastFlightCode);
        flight.setDeparture(flightDto.getDeparture());
        flight.setArrival(flightDto.getArrival());
        flight.setOrigin(cityService.getById(flightDto.getOrigin()));
        flight.setDestination(cityService.getById(flightDto.getDestination()));
        flight.setCreationDate(new Date());
        flight.setPlane(planeService.getById(flightDto.getPlane()));
        flight.setRoundTrip(flightDto.isRoundTrip());
        flight.setLastModificationDAte(new Date());
        flight.setAirline(airline);
        
        flightDao.create(flight);
        
        /* Paths */
        flight.setPathList(new LinkedList<Path>());
        for(PathDto dtoPath: flightDto.getPathList())
        {
            Path path = pathService.create(dtoPath);
            path.setFlight(flight);
            flight.getPathList().add(path);
        }
        
        SeatType seatType  = null;
        SeatTypeIntoFlight seatTypeIntoFlight = null;
        
        /* Fares */
        for(FareDto fareDto:  flightDto.getFareList())
        {
            
            if(fareDto.getType().equals(SeatType.BUSINESS))
            {
               seatType = seatTypeService.getByCategory(SeatType.BUSINESS);
            }
            else if(fareDto.getType().equals(SeatType.TOURIST))
            {
                seatType = seatTypeService.getByCategory(SeatType.TOURIST);
            }
            else if(fareDto.getType().equals(SeatType.OFFER)) 
            {
                seatType = seatTypeService.getByCategory(SeatType.OFFER);
            }
            else // SeatType.LAST_MINUTE
            {
                seatType = seatTypeService.getByCategory(SeatType.LAST_MINUTE);
            }
            
            seatTypeIntoFlight = new SeatTypeIntoFlight(seatType.getId(), flight.getId());
            seatTypeIntoFlight.setFare(fareDto.getValue());
            seatTypeIntoFlight.setSeatType(seatType);
            
            if(flight.getSeatTypeIntoFlightList() == null)
            {
                flight.setSeatTypeIntoFlightList(new LinkedList<SeatTypeIntoFlight>());
            }
            flight.getSeatTypeIntoFlightList().add(seatTypeIntoFlight);
        }
        
        /* Airline */
        if(airline.getFlightList() == null)
        {
            airline.setFlightList(new LinkedList<Flight>());
        }
        airline.getFlightList().add(flight);
        
        airlineService.update(airline);
                
        flightDao.update(flight);
        
        return flight;
    }


    /**
     * @param flightId
     * @param flightDto
     * @return 
     */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    @Override
    public Flight update(Integer flightId, FlightDto flightDto) 
    {
        Flight flight = flightDao.find(flightId);
        
        if(flight != null)
        {
            flight.setDeparture(flightDto.getDeparture());
            flight.setArrival(flightDto.getArrival());
            flight.setOrigin(cityService.getById(flightDto.getOrigin()));
            flight.setDestination(cityService.getById(flightDto.getDestination()));
            flight.setLastModificationDAte(new Date());
            flight.setPlane(planeService.getById(flightDto.getPlane()));
            flight.setRoundTrip(flightDto.isRoundTrip());
            
            /* Paths */
            List<PathDto> dtoPathList = flightDto.getPathList();
            List<Path> pathList = flight.getPathList();
            Iterator<Path> iteratorPath = pathList.iterator();
            
            while(iteratorPath.hasNext())
            {
                Iterator<PathDto> iteratorDtoPath = dtoPathList.iterator();
                Path path = iteratorPath.next();
                boolean delete = true;
                
                while(iteratorDtoPath.hasNext())
                {
                    PathDto dtoPath = iteratorDtoPath.next();
                    
                    if(dtoPath.getId() != null && dtoPath.getId().equals(path.getId()))
                    {
                        // update path
                        path.setPosition(dtoPath.getPosition());
                        path.setOrigin(cityService.getById(dtoPath.getOrigin()));
                        path.setDestination(cityService.getById(dtoPath.getDestination()));
                        path.setSlot(dtoPath.getSlot());
                        path.setDuration(dtoPath.getDuration());
                        path.setFlight(flight);
                        System.out.println("[UPDATE] path="+path);
                        iteratorDtoPath.remove();
                        delete = false;
                        System.out.println("[UPDATE] dtoPathList" + dtoPathList);
                        break;
                    }
                }
                
                if(delete)
                {
                    System.out.println("[DELETE] pathList" + pathList);
                    iteratorPath.remove();
                    pathService.delete(path);
                }
            }
            
            update(flight);
            
            for(PathDto dtoPath : dtoPathList)
            {
                Path path = pathService.create(dtoPath);
                path.setFlight(flight);
                flight.getPathList().add(path);
            }
            
            /**************************************************/
            /* Fares */
            this.updateFareList(flightDto.getFareList(), flight.getSeatTypeIntoFlightList(), flight);

            update(flight);
        }
        else
        {
            throw new EntityNotFoundException("Flight");
        }
        
        flight.getPathList().size();
        flight.getSeatTypeIntoFlightList().size();
        
        return flight;
    }

    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    @Override
    public void delete(Integer flightId) 
    {
        Flight flight = flightDao.find(flightId);
        
        if(flight != null)
        {
            flight.setLogicDeletion(true);
            update(flight);
        }
    }

    
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    private void updateFareList(List<FareDto> dtoFareList, List<SeatTypeIntoFlight> fareList, Flight flight)
    {
        Iterator<SeatTypeIntoFlight> iteratorFare = fareList.iterator();
        List<SeatTypeIntoFlight> fareToDeleteList = new LinkedList<SeatTypeIntoFlight>();
        
        while(iteratorFare.hasNext())
        {
            SeatTypeIntoFlight fare = iteratorFare.next();
            Iterator<FareDto> iteratorDtoFare = dtoFareList.iterator();
            boolean delete = true;
            
            while(iteratorDtoFare.hasNext())
            {
                FareDto dtoFare = iteratorDtoFare.next();

                if(dtoFare.getId()!= null && dtoFare.getId().equals(fare.getSeatType().getId()))
                {
                    // Update fare
                    fare.setFare(dtoFare.getValue());
                    iteratorDtoFare.remove();
                    delete = false;
                    System.out.println("[UPDATED] " + dtoFareList);
                    break;
                }
            }
            
            if(delete)
            {
                fareToDeleteList.add(fare);
                iteratorFare.remove();

                System.out.println("[DELETED]" + fareList);
            }
        }

        for(SeatTypeIntoFlight fare : fareToDeleteList)
        {
            seatTypeIntoFlightService.delete(fare);
        }

        // Create New Fares
        for(FareDto dtoFare: dtoFareList)
        {
            System.out.println("[dtoFare.getType()] " + dtoFare.getType());
            SeatType seatType = seatTypeService.getByCategory(dtoFare.getType());
            SeatTypeIntoFlight fare = new SeatTypeIntoFlight(seatType.getId(), flight.getId());
            fare.setFare(dtoFare.getValue());
            fare.setSeatType(seatType);
            
            if(flight.getSeatTypeIntoFlightList() == null)
            {
                flight.setSeatTypeIntoFlightList(new LinkedList<SeatTypeIntoFlight>());
            }
            flight.getSeatTypeIntoFlightList().add(fare);
            System.out.println("[fareList]" + flight.getSeatTypeIntoFlightList());
        }
    }

    @Override
    public List<Flight> getByAirlineId(Integer airlineId) 
    {
        
        List<Flight> flightList = flightDao.getByAirlineId(airlineId);
        
        for(Flight flight: flightList)
        {
            flight.getPathList().size();
            
            flight.getSeatTypeIntoFlightList().size();
        }
        
        return flightList;
    }
    
    

    @Override
    public List<Flight> getByRestrictions(FlightSearchDto flightDto) 
    {
        List<Flight> flightList = flightDao.getByRestrictions(flightDto);
        
        Iterator<Flight> itFlight = flightList.iterator();
        
        while(itFlight.hasNext())
        {
            Flight flight = itFlight.next();
            
            // Skip lazily exception
            flight.getPathList().size();
            flight.getSeatTypeIntoFlightList().size();
            
            for(ReserveFlight reserveFlight : flight.getReserveFlightList())
            {
                reserveFlight.getSeatList().size();
            }
           
        }
        
        return flightList;
    }

    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    @Override
    public void reserve(FlightReserveDto flightReserveDto)
    {
        Flight flight = flightDao.find(flightReserveDto.getFlightId());
        
        if(flight != null)
        {
            ReserveFlight reserveFlight = new ReserveFlight();
            
            reserveFlight.setFlight(flight);
            if(flightReserveDto.isPaid() != null)
            {
                reserveFlight.setIsPaid(flightReserveDto.isPaid());
            }
            if(flightReserveDto.isPending() != null)
            {
                reserveFlight.setPending(flightReserveDto.isPending());
            }
            if(flightReserveDto.isCanceled() != null)
            {
                reserveFlight.setCanceled(flightReserveDto.isCanceled());
            }
            reserveFlight.setNumSeats(flightReserveDto.getSeatList().size());
            reserveFlight.setUser(userService.getById(flightReserveDto.getUserId()));
            reserveFlight.setReserveDate(new Date());
            
            if(flight.getReserveFlightList() == null)
            {
                flight.setReserveFlightList(new LinkedList<ReserveFlight>());
            }
            flight.getReserveFlightList().add(reserveFlight);
            
            for(Integer idSeat : flightReserveDto.getSeatList())
            {
                Seat seat = seatService.getById(idSeat);
                
                if(seat != null)
                {                
                    if(seat.getReserveFlightList() == null)
                    {
                        seat.setReserveFlightList(new LinkedList<ReserveFlight>());
                    }
                    seat.getReserveFlightList().add(reserveFlight);
                    
                    if(reserveFlight.getSeatList() == null)
                    {
                        reserveFlight.setSeatList(new LinkedList<Seat>());
                    }
                    reserveFlight.getSeatList().add(seat);
                }
            }
            
        }
        else
        {
            throw new EntityNotFoundException("Flight");
        }
    }

    @Override
    public Map<String, List<Flight> > getAllByUserId(Integer userId) 
    {
        Map<String, List<Flight>> flightMap = new HashMap<String, List<Flight>>();
        
//        List<Flight> flightList = new LinkedList<Flight>();
//        
//        List<ReserveFlight> reserveFlightList = reserveFlightService.getByUserId(userId);
//        
////        Map<Integer, Flight> flightMap = new HashMap<Integer, Flight>();
//        
//        
//        if(reserveFlightList != null)
//        {
//            for(ReserveFlight reserveFlight: reserveFlightList)
//            {
//                Flight flight = reserveFlight.getFlight();
//                
//            }
//        }
        
//       return new ArrayList<Flight>(flightMap.values());
        
        
        List<ReserveFlight> reserveFlightList = reserveFlightService.getByUserId(userId);
        
        
        Map<Integer, Flight> pendingMap = new HashMap<Integer, Flight>();
        Map<Integer, Flight> reservedMap = new HashMap<Integer, Flight>();
        Map<Integer, Flight> paidMap = new HashMap<Integer, Flight>();
        
        for(ReserveFlight reserveFlight: reserveFlightList)
        {
            
            if(reserveFlight.getIsPaid())
            {
                // Reserve Paid
                if(!paidMap.containsKey(reserveFlight.getFlight().getId()))
                {
                    paidMap.put(reserveFlight.getFlight().getId(), reserveFlight.getFlight());
                }
            }
            else if(reserveFlight.getReserveDate().after(Util.subtractOneDay(new Date())))
            {
                // reserve date  < 24h
                if((!reserveFlight.getIsPaid()) && 
                        (reserveFlight.getPending() == null || !reserveFlight.getPending()))
                {
                    // Reserve pending of pay
                    if(!reservedMap.containsKey(reserveFlight.getFlight().getId()))
                    {
                        reservedMap.put(reserveFlight.getFlight().getId(), reserveFlight.getFlight());
                    }
                }
                else if(reserveFlight.getPending() != null || reserveFlight.getPending())
                {
                    // Reserva en trámite
                    if(!pendingMap.containsKey(reserveFlight.getFlight().getId()))
                    {
                        pendingMap.put(reserveFlight.getFlight().getId(), reserveFlight.getFlight());
                    }
                }  
            }
        }
        
        flightMap.put("pendingFlights", new LinkedList<Flight>(pendingMap.values()));
        flightMap.put("reservedFlights", new LinkedList<Flight>(reservedMap.values()));
        flightMap.put("paidFlights", new LinkedList<Flight>(paidMap.values()));
        
        
       return flightMap;
    }

    @Override
    public Map<String, List<ReserveFlight>> getAllReservesFromUserId(Integer userId) 
    {
        Map<String, List<ReserveFlight>> reserveFlightMap = new HashMap<String, List<ReserveFlight>>();
        
        List<ReserveFlight> pendingReserves = new LinkedList<ReserveFlight>();
        List<ReserveFlight> reservedReserves = new LinkedList<ReserveFlight>();
        List<ReserveFlight> paidReserves = new LinkedList<ReserveFlight>();
        
        List<ReserveFlight> reserveFlights = reserveFlightService.getByUserId(userId);
        
        for(ReserveFlight reserveFlight : reserveFlights)
        {
            reserveFlight.getSeatList().size();
            
            if(reserveFlight.getIsPaid()) // Reserve Paid
            {
                paidReserves.add(reserveFlight);
                                
            }
            else if(reserveFlight.getReserveDate().after(Util.subtractOneDay(new Date()))) // reserve date  < 24h
            {
                
                if((!reserveFlight.getIsPaid()) && (reserveFlight.getPending() == null || !reserveFlight.getPending()))
                {
                    // Reserve pending of pay
                    reservedReserves.add(reserveFlight);
                }
                else if(reserveFlight.getPending() != null || reserveFlight.getPending())
                {
                    // Reserva en trámite
                    pendingReserves.add(reserveFlight);
                }  
            }
        }
        
        reserveFlightMap.put("pendingFlights", pendingReserves);
        reserveFlightMap.put("reservedFlights", reservedReserves);
        reserveFlightMap.put("paidFlights", paidReserves);
        
        return reserveFlightMap;
    }


    
    
}
