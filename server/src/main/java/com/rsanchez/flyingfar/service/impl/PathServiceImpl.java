package com.rsanchez.flyingfar.service.impl;

import com.rsanchez.flyingfar.dao.PathDao;
import com.rsanchez.flyingfar.domain.Flight;
import com.rsanchez.flyingfar.domain.Path;
import com.rsanchez.flyingfar.dto.PathDto;
import com.rsanchez.flyingfar.service.CityService;
import com.rsanchez.flyingfar.service.PathService;
import com.rsanchez.flyingfar.service.PlaneService;
import java.util.List;
import javax.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 */
@Service("pathService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class PathServiceImpl implements PathService{
    
    private static final long serialVersionUID = 1L;
    
    /* ************ PRIVATE ATTRIBUTES (SERVICES, ETC) ***************** */
    
    @Autowired
    private PathDao pathDao;
    
    @Autowired
    private PlaneService planeService;
    
    @Autowired
    private CityService cityService;

    /* ************ PRIVATE ATTRIBUTES (SERVICES, ETC) ***************** */
    @Override
    public void create(Path path) 
    {
        pathDao.create(path);
    }

    @Override
    public void update(Path path)
    {
        pathDao.update(path);
    }

    @Override
    public void delete(Path path) 
    {
        pathDao.delete(path);
    }

    @Override
    public List<Path> getAll() 
    {
        return pathDao.findAll();
    }

    @Override
    public Path getById(Integer pathId) 
    {
        return pathDao.find(pathId);
    }

    @Override
    public Path create(PathDto pathDto) 
    {
        Path path = new Path();
        path.setPosition(pathDto.getPosition());
        path.setOrigin(cityService.getById(pathDto.getOrigin()));
        path.setDestination(cityService.getById(pathDto.getDestination()));
        path.setSlot(pathDto.getSlot());
        path.setDuration(pathDto.getDuration());
        
        return path;
    }

    @Override
    public Path update(Integer pathId, PathDto pathDto) 
    {
        Path path = pathDao.find(pathId);
        
        if(path != null)
        {
            path.setPosition(pathDto.getPosition());
            path.setOrigin(cityService.getById(pathDto.getOrigin()));
            path.setDestination(cityService.getById(pathDto.getDestination()));
            path.setSlot(pathDto.getSlot());
            path.setDuration(pathDto.getDuration());
        }
        else
        {
            throw new EntityNotFoundException("Path");
        }
        
        return path;
    }

    @Override
    public void delete(Integer pathId)
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    
}
