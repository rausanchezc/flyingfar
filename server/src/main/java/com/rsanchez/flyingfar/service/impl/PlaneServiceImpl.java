package com.rsanchez.flyingfar.service.impl;

import com.rsanchez.flyingfar.dao.PlaneDao;
import com.rsanchez.flyingfar.domain.Plane;
import com.rsanchez.flyingfar.domain.Seat;
import com.rsanchez.flyingfar.domain.SeatType;
import com.rsanchez.flyingfar.dto.PlaneDto;
import com.rsanchez.flyingfar.service.AirlineService;
import com.rsanchez.flyingfar.service.PlaneService;
import com.rsanchez.flyingfar.service.SeatService;
import com.rsanchez.flyingfar.service.SeatTypeService;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 */
@Service("planeService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class PlaneServiceImpl implements PlaneService{
    
    private static final long serialVersionUID = 1L;
    
    /**
     * ************ PRIVATE ATTRIBUTES (SERVICES, ETC) *****************
     */
    
    @Autowired
    private PlaneDao planeDao;
    
    @Autowired
    private AirlineService airlineService;
    
    @Autowired
    private SeatService seatService;
    
    @Autowired
    private SeatTypeService seatTypeService;
    
    /**
     * ************ PRIVATE ATTRIBUTES (SERVICES, ETC) *****************
     * @param plane
     */
    
    @Override
    public void create(Plane plane) 
    {
        planeDao.create(plane);
    }

    @Override
    public void update(Plane plane) 
    {
        planeDao.update(plane);
    }

    @Override
    public List<Plane> getAll() 
    {
        return planeDao.findAll();
    }

    @Override
    public Plane getById(Integer planeId) 
    {
        return planeDao.find(planeId);
    }

    @Override
    public List<Plane> getByModel(String planeModel) 
    {        
        return planeDao.getByModel(planeModel);
    }

    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    @Override
    public Plane create(PlaneDto planeDto) 
    {
        Plane plane = new Plane();
        
        plane.setManufacturer(planeDto.getManufacturer());
        plane.setModel(planeDto.getModel());
        plane.setManufactureDate(planeDto.getManufactureDate());
        plane.setTotalSeats(planeDto.getMaxBusinessSeats()+ planeDto.getMaxTouristSeats()+ planeDto.getMaxOfferSeats());
        plane.setMaxBusinessSeats(planeDto.getMaxBusinessSeats());
        plane.setMaxTouristSeats(planeDto.getMaxTouristSeats());
        plane.setMaxOfferSeats(planeDto.getMaxOfferSeats());
        plane.setCreationDate(new Date());
        plane.setLastModificationDate(new Date());
        
        plane.setAirline(airlineService.getById(planeDto.getAirlineId()));
        planeDao.create(plane);
        
        int seatCounter = 1;
        plane.setSeatList(new LinkedList<Seat>());
        
        SeatType businessType = seatTypeService.getByCategory(SeatType.BUSINESS);
        
        // Create business seats
        for(int businessSeat = 0; businessSeat < planeDto.getMaxBusinessSeats(); businessSeat ++)
        {
            Seat seat = new Seat();
            seat.setSeatType(businessType);
            seat.setPlane(plane);
            seat.setSeatNumber(seatCounter);
            seatCounter++;
            plane.getSeatList().add(seat);
        }
        
        SeatType touristType = seatTypeService.getByCategory(SeatType.TOURIST);

//        // Create tourist seats
        for(int touristSeat = 0; touristSeat < planeDto.getMaxTouristSeats(); touristSeat++)
        {
            Seat seat = new Seat();
            seat.setPlane(plane);
            seat.setSeatType(touristType);
            seat.setSeatNumber(seatCounter);
            seatCounter++;
            plane.getSeatList().add(seat);    
        }
        
        SeatType offerType = seatTypeService.getByCategory(SeatType.OFFER);

        // Create offer seats
        for(int offerSeat = 0; offerSeat < planeDto.getMaxOfferSeats(); offerSeat++)
        {
            Seat seat = new Seat();
            seat.setPlane(plane);
            seat.setSeatNumber(seatCounter);
            seatCounter++;
            seat.setSeatType(offerType);
            plane.getSeatList().add(seat);
        }
        
        planeDao.update(plane);
        
        return plane;
    }

    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    @Override
    public Plane update(Integer planeId, PlaneDto planeDto) 
    {
        Plane plane = planeDao.find(planeId);
        
        if(plane != null)
        {
            plane.setManufacturer(planeDto.getManufacturer());
            plane.setManufactureDate(planeDto.getManufactureDate());
            plane.setModel(planeDto.getModel());
//            plane.setTotalSeats(planeDto.getTotalSeats());
//            plane.setMaxBusinessSeats(planeDto.getMaxBusinessSeats());
//            plane.setMaxTouristSeats(planeDto.getMaxTouristSeats());
//            plane.setMaxOfferSeats(planeDto.getMaxOfferSeats());
            plane.setLastModificationDate(new Date());
            //TODO Rediseñar la actulización de nº asientos / tipo de asientos
            planeDao.update(plane);
        }
        else
        {
            throw new EntityNotFoundException("Plane");
        }
        
        return plane;
    }

    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    @Override
    public void delete(Integer planeId)
    {
        Plane plane = planeDao.find(planeId);

        if(plane != null)
        {
            planeDao.delete(plane);
        }
        else
        {
            throw new EntityNotFoundException("Plane");
        }
    }

    @Override
    public List<Plane> getAllByAirline(Integer airlineId) 
    {
        return planeDao.getByAirline(airlineId);
    }
    
    
}
