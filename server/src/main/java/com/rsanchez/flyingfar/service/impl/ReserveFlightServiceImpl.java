package com.rsanchez.flyingfar.service.impl;

import com.rsanchez.flyingfar.dao.ReserveFlightDao;
import com.rsanchez.flyingfar.domain.ReserveFlight;
import com.rsanchez.flyingfar.domain.Seat;
import com.rsanchez.flyingfar.dto.FlightReserveDto;
import com.rsanchez.flyingfar.service.FlightService;
import com.rsanchez.flyingfar.service.ReserveFlightService;
import com.rsanchez.flyingfar.service.SeatService;
import com.rsanchez.flyingfar.service.UserService;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 */
@Service("reserveFlightService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class ReserveFlightServiceImpl implements ReserveFlightService{
    
    private static final long serialVersionUID = 1L;
    
    /**
     * ************ PRIVATE ATTRIBUTES (SERVICES, ETC) *****************
     */
    
    @Autowired
    private ReserveFlightDao reserveFlightDao;
    
    @Autowired
    private SeatService seatService;
    
    @Autowired
    private UserService userService;
    
    @Autowired
    private FlightService flightService;
    
    /**
     * ************ PRIVATE ATTRIBUTES (SERVICES, ETC) *****************
     */
    @Override
    public void create(ReserveFlight reserveFlight) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void update(ReserveFlight reserveFlight) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<ReserveFlight> getAll() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ReserveFlight getById(Integer reserveFlightId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<ReserveFlight> getByUserId(Integer userId) 
    {
        return reserveFlightDao.getByUserId(userId);
    }

    @Override
    public ReserveFlight getByCode(String reserveFlightCode) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    @Override
    public void delete(Integer reserveFlightId) 
    {
        reserveFlightDao.delete(reserveFlightId);
    }    

    @Override
    public Integer countReserveFlightByUserId(Integer userId) 
    {
        return reserveFlightDao.countReserveFlightByUserId(userId);
    }

    @Override
    public List<ReserveFlight> getOnlyActivesByFlightId(Integer flightId) 
    {
        return reserveFlightDao.getOnlyActivesByFlightId(flightId);
    }

    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    @Override
    public void pay(Integer reserveFlightId) 
    {
        ReserveFlight reserveFlight = reserveFlightDao.find(reserveFlightId);
        
        if(reserveFlight != null)
        {
            reserveFlight.setIsPaid(true);
            reserveFlight.setPending(false);
            reserveFlightDao.update(reserveFlight);
        }
        else
        {
            throw new EntityNotFoundException("Reserve Flight");
        }
    }

    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    @Override
    public void modifySeatList(Integer reserveFlightId, List<Integer> seatList) 
    {
        ReserveFlight reserveFlight = reserveFlightDao.find(reserveFlightId);
        
        if(reserveFlight != null)
        {
            reserveFlight.getSeatList().clear();
            
            for(Integer idSeat : seatList)
            {
                Seat seat = seatService.getById(idSeat);
                
                if(seat != null)
                {
                    reserveFlight.getSeatList().add(seat);
                }
            }
            reserveFlight.setNumSeats(reserveFlight.getSeatList().size());
            
            reserveFlightDao.update(reserveFlight);
        }
        else
        {
            throw new EntityNotFoundException("Reserve Flight");
        }
    }

    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    @Override
    public void replaceByNewReserve(Integer oldReserveId, FlightReserveDto flightReserveDto) 
    {
        ReserveFlight reserveFlight = reserveFlightDao.find(oldReserveId);
        
        if(reserveFlight != null)
        {
            reserveFlightDao.delete(reserveFlight);
            
            ReserveFlight reserve = new ReserveFlight();
            reserve.setUser(userService.getById(flightReserveDto.getUserId()));
            reserve.setFlight(flightService.getById(flightReserveDto.getFlightId()));
            reserve.setReserveDate(new Date());
            reserve.setNumSeats(flightReserveDto.getSeatList().size());
            reserve.setIsPaid(false);
            
            reserveFlightDao.create(reserve);
            
            reserve.setSeatList(new LinkedList<Seat>());
            
            for(Integer seatId : flightReserveDto.getSeatList())
            {
                Seat seat = seatService.getById(seatId);
                reserve.getSeatList().add(seat);
            }
            
            reserveFlightDao.update(reserve);
            
        }
    }

    @Override
    public List<ReserveFlight> getWithSeatOccupationByAirlineCode(String airlineCode) 
    {
        List<ReserveFlight> reserveFlightList =  reserveFlightDao.getWithSeatOccupationByAirlineCode(airlineCode);
        
        for(ReserveFlight reserveFlight : reserveFlightList)
        {
            reserveFlight.getSeatList().size();
        }
        
        return reserveFlightList;
    }
}
