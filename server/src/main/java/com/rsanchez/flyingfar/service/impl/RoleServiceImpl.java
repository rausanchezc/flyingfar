package com.rsanchez.flyingfar.service.impl;

import com.rsanchez.flyingfar.dao.RoleDao;
import com.rsanchez.flyingfar.domain.Role;
import com.rsanchez.flyingfar.service.RoleService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 */
@Service("roleService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class RoleServiceImpl implements RoleService {

    private static final long serialVersionUID = 1L;

    /**
     * ************ PRIVATE ATTRIBUTES (SERVICES, ETC) *****************
     */
    @Autowired
    private RoleDao roleDao;

    /**
     * ************ PRIVATE ATTRIBUTES (SERVICES, ETC)
     *
     *****************
     * @param user
     */
    
    
    @Override
    public void create(Role role) 
    {
        roleDao.create(role);
    }

    @Override
    public void update(Role role) 
    {
        roleDao.update(role);
    }

    @Override
    public void delete(Role role) 
    {
        roleDao.delete(role);
    }

    @Override
    public List<Role> getAll() 
    {
        return roleDao.findAll();
    }

    @Override
    public Role getById(Integer roleId) 
    {
        return roleDao.find(roleId);
    }

    @Override
    public Role getByName(String roleName) 
    {
        return roleDao.getByName(roleName);
    }
}
