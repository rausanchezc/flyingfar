package com.rsanchez.flyingfar.service.impl;

import com.rsanchez.flyingfar.dao.SeatDao;
import com.rsanchez.flyingfar.domain.Seat;
import com.rsanchez.flyingfar.domain.SeatType;
import com.rsanchez.flyingfar.service.SeatService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 */
@Service("seatService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class SeatServiceImpl implements SeatService {

    private static final long serialVersionUID = 1L;

    /**
     * ************ PRIVATE ATTRIBUTES (SERVICES, ETC) *****************
     */
    @Autowired
    private SeatDao seatDao;

    /**
     * ************ PRIVATE ATTRIBUTES (SERVICES, ETC)
     *
     *****************
     * @return 
     */
    
    @Override
    public void create(Seat seat)
    {
        seatDao.create(seat);
    }

    @Override
    public List<SeatType> getAll() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Seat getById(Integer seatId)
    {
        return seatDao.find(seatId);
    }

    @Override
    public SeatType getByCategory(String seatTypeCategoryt) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
