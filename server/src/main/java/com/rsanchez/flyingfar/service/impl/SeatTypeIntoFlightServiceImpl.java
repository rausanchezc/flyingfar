package com.rsanchez.flyingfar.service.impl;

import com.rsanchez.flyingfar.dao.SeatTypeIntoFlightDao;
import com.rsanchez.flyingfar.domain.SeatTypeIntoFlight;
import com.rsanchez.flyingfar.service.SeatTypeIntoFlightService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 */
@Service("seatTypeIntoFlightService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class SeatTypeIntoFlightServiceImpl implements SeatTypeIntoFlightService {

    private static final long serialVersionUID = 1L;

    /**
     * ************ PRIVATE ATTRIBUTES (SERVICES, ETC) *****************
     */
    @Autowired
    private SeatTypeIntoFlightDao seatTypeIntoFlightDao;

    /**
     * ************ PRIVATE ATTRIBUTES (SERVICES, ETC)
     *
     *****************
     * @return 
     */
    @Override
    public List<SeatTypeIntoFlight> getAll()
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete(SeatTypeIntoFlight seatTypeIntoFlight) 
    {
        seatTypeIntoFlightDao.delete(seatTypeIntoFlight);
    }


    
}
