package com.rsanchez.flyingfar.service.impl;

import com.rsanchez.flyingfar.dao.SeatTypeDao;
import com.rsanchez.flyingfar.domain.SeatType;
import com.rsanchez.flyingfar.service.SeatTypeService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 */
@Service("seatTypeService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class SeatTypeServiceImpl implements SeatTypeService {

    private static final long serialVersionUID = 1L;

    /**
     * ************ PRIVATE ATTRIBUTES (SERVICES, ETC) *****************
     */
    @Autowired
    private SeatTypeDao seatTypeDao;

    /**
     * ************ PRIVATE ATTRIBUTES (SERVICES, ETC)
     *
     *****************
     * @return 
     */
    @Override
    public List<SeatType> getAll() 
    {
        return seatTypeDao.findAll();
    }

    @Override
    public SeatType getById(Integer seatTypeId)
    {
        return seatTypeDao.find(seatTypeId);
    }

    @Override
    public SeatType getByCategory(String seatTypeCategory) 
    {
        return seatTypeDao.getByCategory(seatTypeCategory);
    }

    @Override
    public void update(SeatType seatType) 
    {
        seatTypeDao.update(seatType);
    }
    
    

}
