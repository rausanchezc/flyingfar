package com.rsanchez.flyingfar.service.impl;

import com.rsanchez.flyingfar.dao.UserDao;
import com.rsanchez.flyingfar.domain.Role;
import com.rsanchez.flyingfar.domain.User;
import com.rsanchez.flyingfar.dto.UserDto;
import com.rsanchez.flyingfar.service.AirlineService;
import com.rsanchez.flyingfar.service.RoleService;
import com.rsanchez.flyingfar.service.UserService;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 */
@Service("userService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class UserServiceImpl implements UserService{
    
    private static final long serialVersionUID = 1L;
    
    /**
     * ************ PRIVATE ATTRIBUTES (SERVICES, ETC) *****************
     */
    
    @Autowired
    private UserDao userDao;
    
    @Autowired
    private RoleService roleService;
    
    @Autowired
    private AirlineService airlineService;
    
    /**
     * ************ PRIVATE ATTRIBUTES (SERVICES, ETC) *****************
     * @param user
     */

    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    @Override
    public void create(User user) 
    {
        userDao.create(user);
    }

    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    @Override
    public void update(User user) 
    {
        userDao.update(user);
    }

    @Override
    public List<User> getAll() 
    {
        return userDao.findAll();
    }

    @Override
    public User getById(Integer userId) 
    {
        return userDao.find(userId);
    }

    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    @Override
    public User create(UserDto userDto) 
    {
        User user = new User();
        
        user.setUsername(userDto.getUsername());
        user.setPassword(encodePassword(userDto.getPassword(),"7d8a3ded"));

        user.setName(userDto.getName());
        user.setFirstSurname(userDto.getFirstSurname());
        user.setSecondSurname(userDto.getSecondSurname());
        user.setAddress(userDto.getAddress());
        user.setCreditCard(userDto.getCreditCard());
        user.setEmail(userDto.getEmail());
        user.setCreationDate(new Date());
        user.setLastModificationDate(new Date());
        
        user.setRole(roleService.getByName(userDto.getRole()));
        user.setAirline(airlineService.getById(userDto.getAirlineId()));
        
        userDao.create(user);
        
        return user;
    }    

    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    @Override
    public User update(Integer userId, UserDto userDto)
    {
        User user = userDao.find(userId);
        if(user != null)
        {
            user.setUsername(userDto.getUsername());
            if(userDto.getPassword() != null)
            {
                user.setPassword(encodePassword(userDto.getPassword(),"7d8a3ded"));
            }
            user.setName(userDto.getName());
            user.setFirstSurname(userDto.getFirstSurname());
            user.setSecondSurname(userDto.getSecondSurname());
            user.setAddress(userDto.getAddress());
            user.setCreditCard(userDto.getCreditCard());
            user.setEmail(userDto.getEmail());
            user.setLastModificationDate(new Date());
            
            user.setRole(roleService.getByName(userDto.getRole()));
            user.setAirline(airlineService.getById(userDto.getAirlineId()));
            
            userDao.update(user);
        }
        else
        {
            throw new EntityNotFoundException("User");
        }
        
        return user;
    }
    
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    @Override
    public void delete(Integer userId) 
    {
        User user = userDao.find(userId);
        
        if(user != null)
        {
            userDao.delete(user);
        }
        else
        {
            throw new EntityNotFoundException("User");
        }
    }

    @Override
    public List<User> getAdminCompanyList()
    {
        Role adminCompanyRole = roleService.getByName(Role.AIRLINE_ADMIN);
        return userDao.getUserByRole(adminCompanyRole.getId());
    }

    @Override
    public User getByUsername(String username) 
    {
        return userDao.getByUsername(username);
    }

    @Override
    public boolean existsUsername(String username)
    {
        return userDao.existsUsername(username);
    }

    @Override
    public boolean existsEmail(String email) 
    {
        return userDao.existsEmail(email);
    }
  
    private String encodePassword(String password, String base)
    {
        return new ShaPasswordEncoder(512).encodePassword(password, base);
    }
}
