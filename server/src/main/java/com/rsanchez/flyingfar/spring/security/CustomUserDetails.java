package com.rsanchez.flyingfar.spring.security;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;


public class CustomUserDetails extends User
{
    private static final long serialVersionUID = -5221039131683844428L;

    private final Map<String, Object> attributes = new HashMap<String, Object>();

    private com.rsanchez.flyingfar.domain.User user;

    public CustomUserDetails(String username,
                             String password,
                             boolean enabled,
                             boolean accountNonExpired,
                             boolean credentialsNonExpired,
                             boolean accountNonLocked,
                             Collection<GrantedAuthority> authorities)
    {
        super(username, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
    }

    public CustomUserDetails(String username,
                             String password,
                             Collection<GrantedAuthority> authorities)
    {
        super(username, password, authorities);
    }

    public Object get(String attributeName)
    {
        return attributes.get(attributeName);
    }

    public void put(String attributeName,
                    Object attribute)
    {
        attributes.put(attributeName, attribute);
    }

    public Map<String, Object> getAttributes()
    {
        return attributes;
    }

    public com.rsanchez.flyingfar.domain.User getUser()
    {
        return user;
    }

    public void setUser(com.rsanchez.flyingfar.domain.User user)
    {
        this.user = user;
    }
}
