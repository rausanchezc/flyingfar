package com.rsanchez.flyingfar.spring.security;

import com.rsanchez.flyingfar.domain.User;
import com.rsanchez.flyingfar.service.UserService;
import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;


public class CustomUserDetailsService implements UserDetailsService, Serializable
{
    private static final long serialVersionUID = 1L;

    @Autowired
    private UserService userService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException
    {
        User user = userService.getByUsername(username);
        if (user == null)
        {
            throw new UsernameNotFoundException("User not found");
        }

        Collection<GrantedAuthority> grantedAuthorities = new HashSet<GrantedAuthority>();
        if(user.getRole() != null)
        {
            grantedAuthorities.add(new SimpleGrantedAuthority(user.getRole().getName()));
        }       
        else
        {
            grantedAuthorities.add(new SimpleGrantedAuthority("USER"));
        }

        CustomUserDetails userDetails = new CustomUserDetails(user.getUsername(), 
                                                              user.getPassword(),
                                                                true,
                                                                true,
                                                                true,
                                                                true,
                                                                grantedAuthorities);
        userDetails.setUser(user);

        return userDetails;
    }
    
    
}
