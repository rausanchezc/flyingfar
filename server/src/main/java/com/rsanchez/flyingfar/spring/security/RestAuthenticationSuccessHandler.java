package com.rsanchez.flyingfar.spring.security;

import com.rsanchez.flyingfar.json.serializer.JsonUtil;
import com.rsanchez.flyingfar.json.serializer.SessionSerializer;
//import com.cycleit.agora.json.serializer.SessionSerializer;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.security.web.savedrequest.RequestCache;
import org.springframework.security.web.savedrequest.SavedRequest;
import org.springframework.util.StringUtils;


public class RestAuthenticationSuccessHandler extends SimpleUrlAuthenticationSuccessHandler
{

    private RequestCache requestCache = new HttpSessionRequestCache();

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request,
                                        HttpServletResponse response,
                                        Authentication authentication) throws ServletException,
                                                                              IOException
    {
        SavedRequest savedRequest = requestCache.getRequest(request, response);

        response.setCharacterEncoding("UTF-8");
        response.getWriter().print(JsonUtil.serialize(authentication, new SessionSerializer()));
        response.getWriter().flush();
        System.out.println("Authentication (SuccessHandler)" + authentication);
        if (savedRequest == null)
        {
            clearAuthenticationAttributes(request);
            return;
        }

        String targetUrlParam = getTargetUrlParameter();

        if (isAlwaysUseDefaultTargetUrl() || (targetUrlParam != null && StringUtils.hasText(request.getParameter(targetUrlParam))))
        {
            requestCache.removeRequest(request, response);
            clearAuthenticationAttributes(request);
            return;
        }

        clearAuthenticationAttributes(request);
    }

    public void setRequestCache(RequestCache requestCache)
    {
        this.requestCache = requestCache;
    }
}
