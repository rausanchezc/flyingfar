package com.rsanchez.flyingfar.util;

import java.util.Calendar;
import java.util.Date;

/**
 *
 */

public class Util {
    
    public static Date subtractOneDay(Date date)
    {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DAY_OF_MONTH, -1);
        return calendar.getTime();
    }
}
