<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ page isErrorPage="true" %>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<html>
    <head>
        <title><spring:message code="error.head.title"/></title>
    </head>
    <body>
        <h2><spring:message code="error.title"/></h2>
        <p>
            <c:choose>
                <c:when test="${error eq 400}">
                    <spring:message code="error.code400"/>
                </c:when>
                <c:when test="${error eq 403}">
                    <spring:message code="error.code403"/>
                </c:when>
                <c:when test="${error eq 404}">
                    <spring:message code="error.code404"/>
                </c:when>
                <c:when test="${error eq 405}">
                    <spring:message code="error.code405"/>
                </c:when>
                <c:otherwise>
                    ${error} : ${message}
                </c:otherwise>
             </c:choose>
         </p>
    </body>
</html>
