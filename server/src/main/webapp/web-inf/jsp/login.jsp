<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<html>
    <head>
        <title><spring:message code="login.head.title"/></title>
    </head>
    <body>
        <form action="<c:url value="/login/submit"/>" method="post">
            <input type="text" name="username" value="${username}" maxlength="48" placeholder="<spring:message code="user.username"/>" autofocus="autofocus" required="required" pattern="^(([^ @#$]([ ]?[^ ])*)?){3,20}$"/>
            <input type="password" name="password" maxlength="32" required="required" placeholder="<spring:message code="user.password"/>"/>
            <input type="checkbox" name="_spring_security_remember_me">
            <c:if test="${not empty loginException}">
               <div class="alert alert-error login-error"><spring:message code="login.badCredentials"/></div>
               <% 
                  session.removeAttribute("loginException");
                  session.removeAttribute("username");
               %>
            </c:if>
            <button type="submit"><spring:message code="submit"/></button>
        </form>
    </body>
</html>
